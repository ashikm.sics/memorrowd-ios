//
//  MyFlowLayout.swift
//  Memorrowd
//
//  Created by Xavier Joseph on 13/01/22.
//  Copyright © 2022 Srishti Innovative. All rights reserved.
//

import UIKit

class MyFlowLayout: UICollectionViewFlowLayout {
    
    override init() {
        super.init()
        scrollDirection = .vertical
        let sideMargin: CGFloat = 0
        let spacing: CGFloat = 0
        let overlapppingNextPhoto: CGFloat = 3
        minimumLineSpacing = spacing
        minimumInteritemSpacing = spacing
        let screenWidth: CGFloat = UIScreen.main.bounds.width
        let size = screenWidth - (sideMargin + overlapppingNextPhoto)
        itemSize = CGSize(width: size, height: size)
        sectionInset = UIEdgeInsets(top: 0, left: sideMargin, bottom: 0, right: sideMargin)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    // This makes so that Scrolling the collection view always stops with a centered image.
    // This is heavily inpired form :
    // https://stackoverflow.com/questions/13492037/targetcontentoffsetforproposedcontentoffsetwithscrollingvelocity
    // -without-subcla
    override func targetContentOffset(forProposedContentOffset proposedContentOffset: CGPoint,
                                      withScrollingVelocity velocity: CGPoint) -> CGPoint {
        let spacing: CGFloat = 1
        let overlapppingNextPhoto: CGFloat = 1
        var offsetAdjustment = CGFloat.greatestFiniteMagnitude// MAXFLOAT
        let horizontalOffset = proposedContentOffset.x + spacing + overlapppingNextPhoto/2 // + 5
        
        guard let collectionView = collectionView else {
            return proposedContentOffset
        }
        let targetRect = CGRect(x: proposedContentOffset.x,
                                y: 0,
                                width: collectionView.bounds.size.width,
                                height: collectionView.bounds.size.height)
        guard let array = super.layoutAttributesForElements(in: targetRect) else {
            return proposedContentOffset
        }
        
        for layoutAttributes in array {
            let itemOffset = layoutAttributes.frame.origin.x
            if abs(itemOffset - horizontalOffset) < abs(offsetAdjustment) {
                offsetAdjustment = itemOffset - horizontalOffset
            }
        }
        return CGPoint(x: proposedContentOffset.x + offsetAdjustment, y: proposedContentOffset.y)
    }
}
