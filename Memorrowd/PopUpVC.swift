//
//  PopUpVC.swift
//  Memorrowd
//
//  Created by Srishti on 23/07/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//

import UIKit
protocol popUpDelegate {
    func continueSharingImage(indexPath:IndexPath)
    func continueSharingVideo(indexPath:IndexPath)
}
class PopUpVC: UIViewController {

    @IBOutlet weak var popView: UIView!
    @IBOutlet weak var popImageOuterView: UIView!
    @IBOutlet weak var doNotShowStackView: UIStackView!
    var type = ""
    var indexPath : IndexPath?
    var delegate : popUpDelegate?
    var justOnce: Bool = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        popView.layer.cornerRadius = 15
//        let image = UIImage(named: "popup")
//        let imageView = UIImageView(image: image)
//        imageView.frame = CGRect(x: 20.0, y: 50.0, width: self.view.frame.size.width - 40, height: 150.0)
//        popImageOuterView.addSubview(imageView)
//        guard let confettiImageView = UIImageView.fromGif(frame: popImageOuterView.frame, resourceName: "alertAnimation") else {
//            return }
//       // view.addSubview(confettiImageView)
//        confettiImageView.startAnimating()
//        confettiImageView.animationDuration = 3
//        confettiImageView.animationRepeatCount = 5
//        confettiImageView.frame = popImageOuterView.frame
//        confettiImageView.center = popImageOuterView.center
//        popImageOuterView.addSubview(confettiImageView)
        
    }
    @IBAction func doNotShowButtonTapped(_ sender: UIButton) {
        justOnce = !justOnce
        sender.isSelected = !sender.isSelected
        sender.setImage(UIImage(named: "checkbox"), for: .selected)
        
    }
    
    
    @IBAction func okButtonAction(_ sender: UIButton) {
        self.dismiss(animated: true)
        if justOnce == true {
            UserDefaults.standard.set(true, forKey: "popupVC")
            print(UserDefaults.standard.bool(forKey: "popupVC"))
        }
        /*{ [self] in
            print("Typo:\(type)")
            if self.type == "Image"{
                delegate!.continueSharingImage(indexPath:self.indexPath!)
            }else{
                delegate!.continueSharingVideo(indexPath:self.indexPath!)
            }
        }*/
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
