//
//  ServiceManager.swift
//  Pancha Niyamas
//
//  Created by SICS on 20/04/20.
//  Copyright © 2020 Visakh M P. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
var kBaseUrl = "https://www.memorrowd.com/api/"//"https://web.sicsglobal.com/memorrowds/api/"
 //   "https://web.sicsglobal.com/memorrowds/api/"


class ServiceManager: NSObject {
    
    static let sharedInstance = ServiceManager()
    
    func getBaseUrl() -> String {
        return kBaseUrl
    }
    
    func get(withServiceName serviceName: String!, andParameter parameters: Parameters?, withHud isHud: Bool, completion:@escaping (Bool?, AnyObject?, NSError?) -> Void) {
        if isHud {
            SVProgressHUD.show()
            
        }
         let header : HTTPHeaders = ["Content-Type":"application/x-www-form-urlencoded","X-Restli-Protocol-Version": "2.0.0"]
        request(getBaseUrl()+serviceName, method: .get, parameters: parameters, encoding: JSONEncoding.default, headers: header).responseJSON { (response) in
            if isHud {
               SVProgressHUD.dismiss()
            }
            if let result = response.result.value {
                completion(true, result as AnyObject, nil)
            }else {
                completion(false, nil, response.error! as NSError)
            }
        }
   
    }
    
    func post(withServiceName serviceName: String!, andParameters parameters: Parameters?,  withHud isHud: Bool,stopHud:Bool? = true, completion:@escaping (Bool?, AnyObject?, NSError?) -> Void) {
        if isHud {
            Loader.Show()
        }
        print("url: \(getBaseUrl()+serviceName!), params: \(parameters!)")
        request(getBaseUrl()+serviceName!, method: .post, parameters: parameters , encoding: URLEncoding.httpBody).responseString(completionHandler: { response in
            
            print("response String: \(response)")
            
        })
        request(getBaseUrl()+serviceName!, method: .post, parameters: parameters , encoding: URLEncoding.httpBody).responseJSON { (response) in
            if isHud {
                if stopHud!{
                    Loader.Stop()
                }
            }
            if let result = response.result.value {
                completion(true, result as AnyObject, nil)
            }else {
                completion(false, nil, response.error! as NSError)
            }
        }
    }
  
    func parseLinkUsingPostMethod(_ serviceName : String, with dictionary : NSDictionary,imageData :Data,isHud: Bool, completion : @escaping (Bool, AnyObject?, NSError?) -> Void) {
        if isHud {
            Loader.Show()
        }
        upload(multipartFormData: { (multipartFormData) in
          
            if imageData != nil {
               
                    multipartFormData.append( imageData, withName: "file", fileName: "\(String(describing: ServiceManager.makeFileName)).jpg", mimeType: "image/jpg")
                
            }
          
            
            for (key, value) in dictionary {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        }, to:"\(getBaseUrl())"+"\(serviceName)")
        { (result) in
            switch result {
            
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                })
                upload.responseJSON { response in
                    print(response)
                    if isHud {
                        
                            Loader.Stop()
                        
                    }
                    if response.error == nil{
                        completion(true, response.result.value as AnyObject , nil)
                    }else
                    {
                        completion(false, response.result.value as AnyObject, response.error! as NSError)
                    }
                }
            case .failure(let encodingError):
                completion(false, nil, encodingError as NSError)
            }
        }
    }
    func parseLinkUsingPostMethod(_ serviceName : String, with dictionary : NSDictionary,arrayOfImages:[Data],arrayOfVideos:[Data],withHud isHud: Bool, completion : @escaping (Bool, AnyObject?, NSError?) -> Void) {
       //ARSLineProgress.show()
        if isHud {
            Loader.Show()
        }
        upload(multipartFormData: { (multipartFormData) in
          
            if arrayOfImages.count > 0 {
                for data in arrayOfImages {
                    multipartFormData.append( data, withName: "files[]", fileName: "\(String(describing: ServiceManager.makeFileName)).jpg", mimeType: "image/jpg")
                }
            }
            if arrayOfVideos.count > 0 {
                 for data in arrayOfVideos {
                    multipartFormData.append(data, withName: "files[]", fileName: "\(String(describing: ServiceManager.makeFileName)).mp4", mimeType: "video/mp4")
                }
            }
            
            for (key, value) in dictionary {
                multipartFormData.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key as! String)
            }
        }, to:"\(getBaseUrl())"+"\(serviceName)")
        { (result) in
            //ARSLineProgress.hide()
         
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                })
                upload.responseJSON { response in
                    print(response)
                    if isHud {
                        
                            Loader.Stop()
                        
                    }
                    if response.error == nil{
                        completion(true, response.result.value as AnyObject , nil)
                    }else
                    {
                        completion(false, response.result.value as AnyObject, response.error! as NSError)
                    }
                }
            case .failure(let encodingError):
                completion(false, nil, encodingError as NSError)
            }
        }
    }

    func getData(from url: URL, completion: @escaping (Data?, URLResponse?, Error?) -> ()) {
        URLSession.shared.dataTask(with: url, completionHandler: completion).resume()
    }
    func downloadImage(from url: URL) {
        print("Download Started")
        getData(from: url) { data, response, error in
            guard let data = data, error == nil else { return }
            print(response?.suggestedFilename ?? url.lastPathComponent)
            print("Download Finished")
            DispatchQueue.main.async() {
                //self.imageView.image = UIImage(data: data)
            }
        }
    }
    //MARK:- Make File Name
    class func makeFileName(type:String) ->String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyMMddHHmmssSSS"
        let filename = dateFormatter.string(from: Date()) as String
        return "\(filename).jpg"
        
    }
    
    //**********************************LINKEDIN****************************************
    
      func requestForAccessToken(authorizationCode: String,completion : @escaping (Bool,NSDictionary?, NSError?)->Void) {
                let grantType = "authorization_code"
                
                let redirectURL = "https://www.srishtis.com/".addingPercentEncoding(withAllowedCharacters: .alphanumerics)
                let linkedInKey = "86jz0qe0hsg3u1"
                   
                   let linkedInSecret = "iadpgxVY9ngwsUoC"
                   
                   let authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
                   
                   let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
                // Set the POST parameters.
                var postParams = "grant_type=\(grantType)&"
                postParams += "code=\(authorizationCode)&"
                postParams += "redirect_uri=\(String(describing: redirectURL!))&"
                postParams += "client_id=\(linkedInKey)&"
                postParams += "client_secret=\(linkedInSecret)"
                
                // Convert the POST parameters into a NSData object.
             let postData = postParams.data(using: .utf8)
                
                
                // Initialize a mutable URL request object using the access token endpoint URL string.
                let request = NSMutableURLRequest(url: NSURL(string: accessTokenEndPoint)! as URL)
                
                // Indicate that we're about to make a POST request.
                request.httpMethod = "POST"
                
                // Set the HTTP body using the postData object created above.
                request.httpBody = postData
                
                // Add the required HTTP header field.
                request.addValue("application/x-www-form-urlencoded;", forHTTPHeaderField: "Content-Type")
                
                
                // Initialize a NSURLSession object.
                let session = URLSession(configuration: URLSessionConfiguration.default)
                
                // Make the request.
             let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                    // Get the HTTP status code of the request.
                let statusCode = response != nil ? (response as! HTTPURLResponse).statusCode : 0
                    
                    if statusCode == 200 {
                        // Convert the received JSON data into a dictionary.
                        do {
                           let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            completion(true,(dataDictionary as! NSDictionary),nil)
                        
                        }
                        catch {
                            completion(false,nil,error as NSError?)
                            print("Could not convert JSON data into a dictionary.")
                        }
                    }
                    else {
                    completion(false,nil,error as NSError?)
              }
                }
                
                task.resume()
            }
      // MARK: LinkedIn
     func getProfileInfo(completion : @escaping (Bool,NSDictionary?, NSError?)->Void) {
     
      if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
                 // Specify the URL string that we'll get the profile info from.
                 let targetURLString = "https://api.linkedin.com/v2/me"
                 
                 
                 // Initialize a mutable URL request object.
          let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)
                 
                 // Indicate that this is a GET request.
          request.httpMethod = "GET"
                 
                 // Add the access token as an HTTP header field.
          request.addValue("Bearer \(accessToken as! String)", forHTTPHeaderField: "Authorization")
                 
                 
                 // Initialize a NSURLSession object.
          let session = URLSession(configuration: URLSessionConfiguration.default)
                 
                 // Make the request.
          let task: URLSessionDataTask = session.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                     // Get the HTTP status code of the request.
              let statusCode = response != nil ? (response as! HTTPURLResponse).statusCode : 0
                     
                     if statusCode == 200 {
                         // Convert the received JSON data into a dictionary.
                         do {
                         let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                            completion(true,(dataDictionary as! NSDictionary),nil)
                         }
                         catch {
                         completion(false,nil,error as NSError?)
                             print("Could not convert JSON data into a dictionary.")
                         }
                     }
                     else {
                        completion(false,nil,error as NSError?)
              }
                 }
                 
                 task.resume()
             }
        else {
            SVProgressHUD.dismiss()
               }
         }
    
     func registerImageToLinkedIn(id:String,completion : @escaping (Bool,NSDictionary?, NSError?)->Void){
           if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
                  // Specify the URL string that we'll get the profile info from.
                  let targetURLString = "https://api.linkedin.com/v2/assets?action=registerUpload"
                  let payloadStr: String = "{\"registerUploadRequest\":{\"recipes\":[\"urn:li:digitalmediaRecipe:feedshare-image\"],\"owner\": \"urn:li:person:\(id)\",\"serviceRelationships\": [{\"relationshipType\": \"OWNER\",\"identifier\": \"urn:li:userGeneratedContent\"}]}}"
             
                  // Initialize a mutable URL request object.
                  let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)

                  // Indicate that this is a GET request.
                  request.httpMethod = "POST"
                  request.httpBody = payloadStr.data(using: String.Encoding.utf8)
                  // Add the access token as an HTTP header field.
                  request.addValue("Bearer \(accessToken as! String)", forHTTPHeaderField: "Authorization")
                  request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                  request.addValue("json", forHTTPHeaderField: "x-li-format")
                 request.addValue("2.0.0", forHTTPHeaderField: "X-Restli-Protocol-Version")
                  // Make the request.
                  let task: URLSessionDataTask = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                      // Get the HTTP status code of the request.
                      let statusCode = response != nil ? (response as! HTTPURLResponse).statusCode : 0

                      if statusCode == 200 {
                          // Convert the received JSON data into a dictionary.
                       do {
                           let dataDictionary = try JSONSerialization.jsonObject(with: data!, options: JSONSerialization.ReadingOptions.mutableContainers)
                              
                            print(dataDictionary)
                          
                        completion(true,(dataDictionary as! NSDictionary),nil)
                         
                          }
                          catch {
                           completion(false,nil,error as NSError?)
                              print("Could not convert JSON data into a dictionary.")
                          }
                          
                      }
                      else {
                       completion(false,nil,error as NSError?)
                   }
                  }
                  task.resume()

              }
        else {
                   SVProgressHUD.dismiss()
               }
          }
       
       func UploadImageBinaryFileToLinkedIn(assest:String,uploadUrl:String,id:String,completion : @escaping (Bool, NSError?)->Void){
              if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
               SVProgressHUD.show()
               
            
            
                     let url = URL(string: uploadUrl)
                     let boundary = generateBoundaryString()
                     var request = URLRequest(url: url!)
                /*let imageString = corgiData.base64EncodedString(options: [])

                var request: URLRequest? = nil
                do {
                    request = try client.urlRequest(withMethod: "POST" + URL, media, parameters: [
                        "media": imageString
                    ])
                } catch let requestError {
                }*/
               request.httpMethod = "POST"
               
                request.httpBody = createRequestBodyWith(image: UIImage(named: "img1")!, filePathKey: "upload-file", boundary: boundary) as Data
                    // createBody(with:  "upload-file", urls: [fileUrl], boundary: boundary)
            // request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")


              // request.addValue("data/binary", forHTTPHeaderField: "Content-Type")
                request.addValue("image/png", forHTTPHeaderField: "Content-Type")
                request.addValue("2.0.0", forHTTPHeaderField: "X-Restli-Protocol-Version")
                request.addValue("Bearer \(accessToken as! String)", forHTTPHeaderField: "Authorization")
                     let task = URLSession.shared.dataTask(with: request) { data, response, error in
                        let statusCode = response != nil ? (response as! HTTPURLResponse).statusCode : 0
                        if statusCode == 201 {
                             completion(true,nil)
                           
                       }
                        else{
                           completion(false,error as NSError?)
                       }
                    
                     }

                     task.resume()
             }
              else {
            SVProgressHUD.dismiss()
        }
       }
    func createMultipart(uploadUrl:String,fileData:Data, fileName:String , completion: @escaping (_ fileURL:String?, _ error:String?) -> Void) {

          if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
         print("FILENAME: \(fileName)")

           let boundary: String = "------VohpleBoundary4QuqLuM1cE5lMwCy"
           let contentType: String = "multipart/form-data; boundary=\(boundary)"
           let request = NSMutableURLRequest()
           request.url = URL(string: uploadUrl)
           request.httpShouldHandleCookies = false
           request.timeoutInterval = 60
           request.httpMethod = "PUT"
          // request.addValue("data/binary", forHTTPHeaderField: "Content-Type")
            request.addValue("Bearer \(accessToken as! String)", forHTTPHeaderField: "Authorization")

           let body = NSMutableData()
           body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
           body.append("Content-Disposition: form-data; name=\"fileName\"\r\n\r\n".data(using: String.Encoding.utf8)!)
           body.append("\(fileName)\r\n".data(using: String.Encoding.utf8)!)

           body.append("--\(boundary)\r\n".data(using: String.Encoding.utf8)!)
           body.append("Content-Disposition: form-data; name=\"file\"; filename=\"file\"\r\n".data(using: String.Encoding.utf8)!)

           // File is an image
           if fileName.hasSuffix(".png") {
               body.append("Content-Type:image/jpg\r\n\r\n".data(using: String.Encoding.utf8)!)
           // File is a video
           } else if fileName.hasSuffix(".mp4") {
               body.append("Content-Type:video/mp4\r\n\r\n".data(using: String.Encoding.utf8)!)
           }

           body.append(fileData)
           body.append("\r\n".data(using: String.Encoding.utf8)!)


           body.append("--\(boundary)--\r\n".data(using: String.Encoding.utf8)!)
           request.httpBody = body as Data
           let session = URLSession.shared
           let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
               guard let _:Data = data as Data?, let _:URLResponse = response, error == nil else {
                   DispatchQueue.main.async { completion(nil, error!.localizedDescription) }
                   return
               }
               if let response = String(data: data!, encoding: String.Encoding(rawValue: String.Encoding.utf8.rawValue)) {
                   DispatchQueue.main.async { completion(response, nil) }

               // NO response
               } else { DispatchQueue.main.async { completion(nil, error?.localizedDescription) } }// ./ If response
           }; task.resume()
     
     

        }
       }

    
       func shareFileToLinkedIn(assest:String,id:String,completion : @escaping (Bool, NSError?)->Void){
          
           
               if let accessToken = UserDefaults.standard.object(forKey: "LIAccessToken") {
                      // Specify the URL string that we'll get the profile info from.
                      let targetURLString = "https://api.linkedin.com/v2/ugcPosts"
            

               let payloadStr: String = "{\"author\": \"urn:li:person:\(id)\",\"lifecycleState\": \"PUBLISHED\",\"specificContent\": {\"com.linkedin.ugc.ShareContent\": {\"shareCommentary\": {\"text\": \"Feeling inspired after meeting so many talented individuals at this year's conference. #talentconnect\"},\"shareMediaCategory\":\"IMAGE\",\"media\": [{\"status\": \"READY\",\"description\": {\"text\": \"Center stage!\"},\"media\": \"\(assest)\",\"title\": {\"text\": \"LinkedIn Talent Connect 2018\"}}]}},\"visibility\": {\"com.linkedin.ugc.MemberNetworkVisibility\": \"PUBLIC\"}}"
                 
                      // Initialize a mutable URL request object.
                      let request = NSMutableURLRequest(url: NSURL(string: targetURLString)! as URL)
                      // Indicate that this is a GET request.
                      request.httpMethod = "POST"
                
                      request.httpBody = payloadStr.data(using: String.Encoding.utf8)
                      // Add the access token as an HTTP header field.
                      request.addValue("Bearer \(accessToken as! String)", forHTTPHeaderField: "Authorization")
                      request.addValue("application/json", forHTTPHeaderField: "Content-Type")
                      request.addValue("json", forHTTPHeaderField: "x-li-format")
                      request.addValue("2.0.0", forHTTPHeaderField: "X-Restli-Protocol-Version")
              
                      // Make the request.
                let task: URLSessionDataTask = URLSession.shared.dataTask(with: request as URLRequest) { (data, response, error) -> Void in
                    
                          // Get the HTTP status code of the request.
                          let statusCode = response != nil ? (response as! HTTPURLResponse).statusCode : 0

                          if statusCode == 201 {
                              // Convert the received JSON data into a dictionary.
                             completion(true,nil)
                          // self.navigationController?.popViewController(animated: true)
                             
                          }
                          else {
                            completion(false,error as NSError?)
                   }
                      }
                      task.resume()

                  }
        else {
                   SVProgressHUD.dismiss()
               }
           
              }
    func createRequestBodyWith(image:UIImage, filePathKey:String, boundary:String) -> NSData{

          let body = NSMutableData()

        
          body.appendString(string: "--\(boundary)\r\n")

        let mimetype = "image/jpg"

          let defFileName = "yourImageName.jpg"

        let imageData = image.jpegData(compressionQuality: 0.1)

        body.appendString(string: "Content-Disposition: form-data; name=\"\(filePathKey)\"; filename=\"\(defFileName)\"\r\n")
          body.appendString(string: "Content-Type: \(mimetype)\r\n\r\n")
          body.append(imageData!)
          body.appendString(string: "\r\n")

          body.appendString(string: "--\(boundary)--\r\n")

          return body
      }



      func generateBoundaryString() -> String {
          return "Boundary-\(NSUUID().uuidString)"
      }


}

