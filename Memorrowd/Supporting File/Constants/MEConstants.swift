//
//  MEConstants.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 07/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import Foundation
import UIKit
let kAppThemeBackground = UIColor.init(red: 255.0/255.0, green: 79.0/255.0, blue: 0, alpha: 1.0)
let kAppThemeLightGreen = UIColor.init(red: 180.0/255.0, green: 255.0/255.0, blue: 30.0/255.0, alpha: 1.0)
let kStoryboard         = UIStoryboard.init(name: "Main", bundle: nil)
let kAppDelegate        = (UIApplication.shared.delegate) as! AppDelegate
let kScreenWidth        = UIScreen.main.bounds.width
let kScreenHeight       = UIScreen.main.bounds.height

let kAppThemeVioletBottom = UIColor.init(red: 65.0/255.0, green: 0.0/255.0, blue: 54.0/255.0, alpha: 1.0)
let kAppThemeYellowTop      = UIColor.init(red: 246/255.0, green: 143/255.0, blue: 25/255.0, alpha: 1.0)

let kAppFontRegular         = "Nunito-Regular"
let kAppFontSemiBold        = "Nunito-SemiBold"
var kDeviceToken = ""
let kplaceholderImage : UIImage = #imageLiteral(resourceName: "placeholder")
struct Array {
  
    static let kPlaceHolderArrayForSginIn = ["Username", "Phone Number","Email","Password"]
    static let kPlaceHolderArrayLogin = ["Email / Username", "Password"]
  
}
struct API {
   // static let kLogin               = "Auth/login"
   
}
