//
//  ButtonDesign.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 29/4/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//

import UIKit


class ButtonDesign : UIButton {
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setRadiusAndShadow()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setRadiusAndShadow()
    }
    
    func setRadiusAndShadow() {
        layer.cornerRadius = frame.height / 12
        clipsToBounds = true
        layer.masksToBounds = false
        layer.shadowRadius = 5
        layer.shadowOpacity = 1.0
        layer.shadowOffset = CGSize(width: 3, height: 3)
        layer.shadowColor = UIColor.lightGray.cgColor
    }
    
}
