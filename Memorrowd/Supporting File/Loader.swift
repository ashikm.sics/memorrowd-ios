//
//  Loader.swift
//  Time Capsule
//
//  Created by Srishti on 14/05/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//
//
//import Foundation
//import UIKit
//import NVActivityIndicatorView
//
//class Loader:NVActivityIndicatorPresenter{
//
//    let activityData = ActivityData()
//
//    init() {
//        NVActivityIndicatorView.DEFAULT_COLOR = UIColor.white
//        NVActivityIndicatorView.DEFAULT_BLOCKER_BACKGROUND_COLOR = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
//    }
//
//    static func show(){
//        NVActivityIndicatorPresenter.sharedInstance.startAnimating(activityData)
//    }
//    static func stop(){
//        //NVActivityIndicatorPresenter.sharedInstance.stopAnima
//    }
//}
import NVActivityIndicatorView
import UIKit


final class Loader{
    
    static let activityIndicator =  NVActivityIndicatorView(frame:CGRect(
        origin: .zero,
        size: NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE
    ) , type: .ballRotateChase, color: .gray, padding: .zero)
    
    var current = Loader()
    static func Show(){
        (UIApplication.shared.delegate as! AppDelegate).window?.startBlockingActivityIndicator()
    }
    static func Stop(){
        (UIApplication.shared.delegate as! AppDelegate).window?.stopBlockingActivityIndicator()
    }
}

final class BlockingActivityIndicator: UIView {
  private let activityIndicator: NVActivityIndicatorView

    override init(frame: CGRect) {
    self.activityIndicator = NVActivityIndicatorView(
      frame: CGRect(
        origin: .zero,
        size: NVActivityIndicatorView.DEFAULT_BLOCKER_SIZE
      ),type: .ballRotateChase,color:.gray
    )
    activityIndicator.startAnimating()
    activityIndicator.translatesAutoresizingMaskIntoConstraints = false
    super.init(frame: frame)
      
            if #available(iOS 13.0, *) {
                backgroundColor = UIColor.systemBackground.withAlphaComponent(0.3)
            } else {
                // Fallback on earlier versions
            }
        
    addSubview(activityIndicator)
    NSLayoutConstraint.activate([
      activityIndicator.centerXAnchor.constraint(equalTo: safeAreaLayoutGuide.centerXAnchor),
      activityIndicator.centerYAnchor.constraint(equalTo: safeAreaLayoutGuide.centerYAnchor),
    ])
  }

  required init?(coder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }
}

extension UIWindow {
   
  func startBlockingActivityIndicator() {
    guard !subviews.contains(where: { $0 is BlockingActivityIndicator }) else {
      return
    }
    let activityIndicator = BlockingActivityIndicator()
    
    activityIndicator.autoresizingMask = [.flexibleWidth, .flexibleHeight]
    activityIndicator.frame = bounds

    UIView.transition(
      with: self,
      duration: 0.3,
      options: .transitionCrossDissolve,
      animations: {
        self.addSubview(activityIndicator)
      }
    )
  }
    
    func stopBlockingActivityIndicator(){
        
        for subview in subviews{
            if (subview is BlockingActivityIndicator){
                UIView.transition(
                  with: self,
                  duration: 0.3,
                  options: .transitionCrossDissolve,
                  animations: {
                    subview.removeFromSuperview()
                  }
                )
                
            }
        }
        
    }
}

