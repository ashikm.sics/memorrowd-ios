//
//  AppDelegate.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 07/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//






import UIKit
import CoreData
import GoogleSignIn
import SwiftyDropbox
import FBSDKCoreKit
import TwitterKit
import SCSDKLoginKit
import TikTokOpenSDK
import HSGoogleDrivePicker
import UserNotifications
import Stripe

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var isPushNotificationAction = false
    var fileId = 0
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        self.checkSubscription()
        self.checkAutoLogin()
        
        GIDSignIn.sharedInstance().clientID = "59368663056-1gvcq3ns1h3in4ni6vle9p8bl2l71e8s.apps.googleusercontent.com"
        DropboxClientsManager.setupWithAppKey("d578h6sozhcwyye")
        ApplicationDelegate.shared.application(
            application,
            didFinishLaunchingWithOptions: launchOptions
        )
        /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
         API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
         Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
        
        TWTRTwitter.sharedInstance().start(withConsumerKey:"Yrbr1QkdEcUjUQ88eZwIdtixx", consumerSecret:"HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw")
        TikTokOpenSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        application.registerUserNotificationSettings(UIUserNotificationSettings(types: [UIUserNotificationType.alert, UIUserNotificationType.badge, UIUserNotificationType.sound], categories: nil))
        application.registerForRemoteNotifications()
        if let notification = launchOptions?[UIApplication.LaunchOptionsKey.remoteNotification] as? [String: AnyObject] {
            application.applicationIconBadgeNumber = 0
            isPushNotificationAction = true
            
            self.fileId = ((notification["aps"] as! NSDictionary)["notificationid"] as! Int)
            
        }else {
            isPushNotificationAction = false
        }
        application.applicationIconBadgeNumber = 0
        // Override point for customization after application launch.
        
        //StripeAPI.defaultPublishableKey = "pk_live_9HID5pfiXfsvz3WPiDuIfJKo00JhNVnHlI"
        
       StripeAPI.defaultPublishableKey = "pk_test_BY01x7pcnquPdkZw3HgRJIlM00TJi185Lt"
//        STRIPE_SECRET ="sk_test_lSqS1KT7DzV0FlPyRQzrRE2k00CzcOg5lp"
//
//        STRIPE_KEY = "pk_live_9HID5pfiXfsvz3WPiDuIfJKo00JhNVnHlI"
//        STRIPE_SECRET = "sk_live_svdyQ02tCGnT4rl6UkGJ63RH00fXn7wnAa"

        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        
        if TikTokOpenSDKApplicationDelegate.sharedInstance().application(app, open: url, sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String, annotation:  options[UIApplication.OpenURLOptionsKey.annotation] as Any) {
            return true
        }
        
        if TWTRTwitter.sharedInstance().application(app, open: url, options: options){
            return true
        }
        SCSDKLoginClient.application(app, open: url, options: options)
        ApplicationDelegate.shared.application(
            app,
            open: url,
            sourceApplication: options[UIApplication.OpenURLOptionsKey.sourceApplication] as? String,
            annotation: options[UIApplication.OpenURLOptionsKey.annotation]
        )
        /* _ = DropboxClientsManager.handleRedirectURL(url, completion: { (authResult) in
         switch authResult {
         case .success:
         print("Success! User is logged into Dropbox.")
         case .cancel:
         print("Authorization flow was manually canceled by user!")
         case .error(_, let description):
         print("Error: \(description ?? "")")
         case .none:
         print("none")
         }
         })*/
        
        // real one
        let oauthCompletion: DropboxOAuthCompletion = {
            if let authResult = $0 {
                switch authResult {
                case .success:
                    print("Success! User is logged into DropboxClientsManager.")
                case .cancel:
                    print("Authorization flow was manually canceled by user!")
                case .error(_, let description):
                    print("Error: \(String(describing: description))")
                }
            }
        }
        let canHandleUrl = DropboxClientsManager.handleRedirectURL(url, completion: oauthCompletion)
            return canHandleUrl
        
        if HSDrivePicker.handle(url) {
            return true
        }
        
        return true
    }
    //MARK:- PUSHNOTICATION IN ACTIVE STATE
    func showViewControllerWithJobId(_ stringJobId: Int) {
        isPushNotificationAction = true
        self.fileId = stringJobId
        self.checkAutoLogin()
    }
    //MARK:- CHECK AUTO LOGIN
    fileprivate func checkAutoLogin() {
        if UserDefaults.standard.object(forKey: "userDetails") != nil {
            let tuUser = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userDetails") as! Data) as? NSDictionary
            MEUserDetails.currentUser.initWithDict(dict: tuUser!)
            
//            ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { success, result, error in
//                if success == true {
//                    let planDetailsDict = result!["packages"]! as! NSDictionary
//                    let registerPlanNumber = planDetailsDict["register_plan"] as AnyObject
//                    print("RegisterPlanNumber is : \(registerPlanNumber)")
//                    if registerPlanNumber as! Int == 0 {
//                        let viewController = kStoryboard.instantiateViewController(withIdentifier: "MEChoosePlanVC") as! MEChoosePlanViewController
//                        self.window!.rootViewController = viewController
//                    } else {
                        
                        if self.isPushNotificationAction {
                            let viewController = kStoryboard.instantiateViewController(withIdentifier: "MENotificationSetTVC") as! MENotificationSetTVC
                            viewController.fileId = self.fileId
                            viewController.from = "appDelegate"
                            self.isPushNotificationAction = false
                            //  window!.rootViewController = UINavigationController(rootViewController: viewController)
                            self.window!.rootViewController = viewController
                        }
                        else {
                            let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                            self.isPushNotificationAction = false
                            self.window!.rootViewController = viewController
                        }
                    }
                    
//                } else {
//                    self.showAlertWithOkButton(message: "Please try again")
//                }
            }
//        }
//    }
    
    
    
    //MARK:- REMOTE NOTIFICATION METHODS
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        /*  var tokenStr = deviceToken.description
         tokenStr = tokenStr.replacingOccurrences(of: "<", with: "", options: [], range: nil)
         tokenStr = tokenStr.replacingOccurrences(of: ">", with: "", options: [], range: nil)
         tokenStr = tokenStr.replacingOccurrences(of: " ", with: "", options: [], range: nil)*/
        let deviceTokenString = deviceToken.hexString
        print("Device Token in AppDelegate: \(deviceTokenString)")
        if deviceTokenString == ""{
            kDeviceToken = "123456sim"
        }else{
            kDeviceToken = deviceTokenString
        }
       
        UserDefaults.standard.setValue(deviceTokenString, forKey: "kDeviceToken")
        UserDefaults.standard.synchronize()
    }
    
    func application(_ application: UIApplication, didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print(error)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any])
    {
        //NSLog("***********Notification Received***********\n%@", userInfo)
        application.applicationIconBadgeNumber = 0
        if UIApplication.shared.applicationState == .inactive {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil, userInfo: userInfo)
            
            
        }
        if UIApplication.shared.applicationState == .background {
            self.isPushNotificationAction = true
            self.fileId = ((userInfo["aps"] as! NSDictionary)["notificationid"] as! Int)
            
            self.checkAutoLogin()
        }
        if UIApplication.shared.applicationState == .active {
            
            NotificationCenter.default.post(name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil, userInfo: userInfo)
        }
    }
    
    //MARK: - To get notification when the app is in Active stage (foreground)
    func userNotificationCenter(_ center: UNUserNotificationCenter, willPresent notification: UNNotification, withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void)
        {
            let userInfo = notification.request.content.userInfo as? NSDictionary
            let aps = userInfo!["aps"] as? NSDictionary
        NotificationCenter.default.post(name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil, userInfo: userInfo as? [AnyHashable : Any])
            completionHandler([.alert, .badge, .sound])
    }
    
    //MARK: - Check if the user selected any subscription plan
        func checkSubscription() {
            if UserDefaults.standard.object(forKey: "userDetails") != nil {
                let tuUser = NSKeyedUnarchiver.unarchiveObject(with: UserDefaults.standard.object(forKey: "userDetails") as! Data) as? NSDictionary
                 MEUserDetails.currentUser.initWithDict(dict: tuUser!)
            ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { success, result, error in
            if success == true {
                let planDetailsDict = result!["packages"]! as! NSDictionary
                let registerPlanNumber = planDetailsDict["register_plan"] as AnyObject
                if let expiryDate = planDetailsDict["expiry_date"] as? String{
                    self.checkIfTrialExpired(dateString: expiryDate)//"02 July 2021 12:00 AM")
                }
                print("RegisterPlanNumber is : \(registerPlanNumber)")
                if registerPlanNumber as! Int == 0 {
                    let mechoose = kStoryboard.instantiateViewController(withIdentifier: "MEChoosePlanVC") as! MEChoosePlanViewController
                    mechoose.isFromSignUp = true
                    let navController = UINavigationController(rootViewController:mechoose)//kStoryboard.instantiateViewController(withIdentifier: "NavigationId") as! UINavigationController
                    
                    self.window!.rootViewController = navController
                } else {
                let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                    self.isPushNotificationAction = false
                    self.window!.rootViewController = viewController
                }
            } else {
                self.showAlertWithOkButton(message: "Please try again")
            }
            }
            }
        }
    func checkIfTrialExpired(dateString:String){
        let currentDate = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd MMMM yyyy h:mm a"//02 August 2021 12:00 AM
        if let date = dateFormatter.date(from: dateString){
            if currentDate > date{
                MEUserDetails.currentUser.isPackageExpired = true
            }else{
                MEUserDetails.currentUser.isPackageExpired = false
            }
        }else{
            //
        }
    }
    //MARK:- Show Alert With Ok Button
    func showAlertWithOkButton(message:String){
        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
            
        }
    }
    
    
    // MARK: UISceneSession Lifecycle
    
    /*    @available(iOS 13.0, *)
     func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
     // Called when a new scene session is being created.
     // Use this method to select a configuration to create the new scene with.
     return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
     }
     
     @available(iOS 13.0, *)
     func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
     // Called when the user discards a scene session.
     // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
     // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
     }
     */
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "Memorrowd")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    
    func saveContext () {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
}
extension Data {
    var hexString: String {
        let hexString = map { String(format: "%02.2hhx", $0) }.joined()
        return hexString
    }
}

