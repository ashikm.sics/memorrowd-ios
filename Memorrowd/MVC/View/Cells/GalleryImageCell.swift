//
//  galleryImageCell.swift
//  Memorrowd
//
//  Created by Xavier Joseph on 11/01/22.
//  Copyright © 2022 Srishti Innovative. All rights reserved.
//

import UIKit

class GalleryImageCell: UICollectionViewCell {
    @IBOutlet weak var galleryImageView: UIImageView!
    
}
