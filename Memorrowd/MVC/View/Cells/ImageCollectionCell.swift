//
//  ImageCollectionCell.swift
//  Memorrowd
//
//  Created by John Srishti on 14/01/22.
//  Copyright © 2022 Srishti Innovative. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    @IBOutlet weak var imageView: UIImageView!
    
}
