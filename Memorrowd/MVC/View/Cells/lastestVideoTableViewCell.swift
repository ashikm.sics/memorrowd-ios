//
//  lastestVideoTableViewCell.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 08/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class lastestVideoTableViewCell: UITableViewCell {
      @IBOutlet var labelVideosTime                  : UILabel!
      @IBOutlet var imageViewVideos                  : UIImageView!
      @IBOutlet var btnShare                         : UIButton!
      @IBOutlet var btnPlay                            : UIButton!
      @IBOutlet var btnthreeDot                         : UIButton!
    @IBOutlet weak var noMediaLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageViewVideos.contentMode = .scaleAspectFill
        imageViewVideos.layer.masksToBounds = true

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    override func prepareForReuse() {
        if noMediaLabel != nil{
            noMediaLabel.isHidden = true
        }
        if btnShare != nil{
            btnShare.isHidden = false
        }
        labelVideosTime.isHidden = false
        imageViewVideos.isHidden = false
       
        btnPlay.isHidden = false
        if btnthreeDot != nil{
        btnthreeDot.isHidden = false
        }
        
    }

}
class sharedVideoTableViewCell: UITableViewCell {
    
      @IBOutlet var imageViewVideos                  : UIImageView!
      @IBOutlet var imageViewPlay                    : UIImageView!
      @IBOutlet var labelVideosName                  : UILabel!
      @IBOutlet var labelVideosTime                  : UILabel!
      @IBOutlet var labelVideosDate                 : UILabel!

      @IBOutlet var labelVideosSharedVia             : UILabel!
      @IBOutlet var viewBackground                   : UIView!
      @IBOutlet var btnDelete                         : UIButton!
      @IBOutlet var btnEdit                         : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class videoEditTableViewCell: UITableViewCell {
           
                     @IBOutlet var txtVideoTitle             : UITextField!
                     @IBOutlet var txtVideoCategory          : UITextField!
                     @IBOutlet var txtViewDescription        : UITextView!
                     @IBOutlet var btnSubmitFile             : UIButton!
                     @IBOutlet var btnDeleteFile             : UIButton!
                     @IBOutlet var labelTitle                : UILabel!
                     @IBOutlet var labelAlbum                : UILabel!
                     @IBOutlet var txtFields                 : [UITextField]!

    override func awakeFromNib() {
        super.awakeFromNib()
        txtFields.forEach { (txtField) in
                         txtField.setBorderAndCornerRadius(borderColor: UIColor.gray, borderWidth: 1.0, cornerRadius: 4.0)
                         
                        
                      
                     }
         self.txtViewDescription.setBorderAndCornerRadius(borderColor: UIColor.gray, borderWidth: 1.0, cornerRadius: 4.0)
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
class videoNotificationTableViewCell: UITableViewCell {
           
       @IBOutlet var txtSelectDate             : UITextField!
       @IBOutlet var txtSelectTime             : UITextField!
       @IBOutlet var txtFields                 : [UITextField]!
       @IBOutlet var collectionViewSet         : UICollectionView!
       @IBOutlet var btnSubmit                 : UIButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        txtFields.forEach { (txtField) in
                         txtField.setBorderAndCornerRadius(borderColor: UIColor.gray, borderWidth: 1.0, cornerRadius: 4.0)
                      
                      
                     }
       
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
