//
//  MELoginTableViewController.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 07/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class MELoginTableViewController: UITableViewController,UITextFieldDelegate {
          @IBOutlet var txtUserName               : UITextField!
          @IBOutlet var txtPasswrd                : UITextField!

          @IBOutlet var btnSignup                 : UIButton!
          @IBOutlet var btnSignIn                 : UIButton!
          @IBOutlet var txtFields                 : [UITextField]!
    override func viewDidLoad() {
        super.viewDidLoad()
       
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.setUpNavigationBar()
        self.customUI()
    }
    //MARK:- Set Up NavigationBar
      func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
                   let navBarAppearance = UINavigationBarAppearance()
                   navBarAppearance.configureWithOpaqueBackground()
                   navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.backgroundColor = kAppThemeYellowTop
                   self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                   self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
               }
          self.navigationController?.navigationBar.isHidden = true
          self.navigationItem.setHidesBackButton(true, animated: true)
          //self.navigationItem.title = "Add To Cart"
          self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
          self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
          //self.navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!]
          //let logoButton = UIBarButtonItem(image: Images.kLogoSmallWhite, style: .plain, target: self, action: #selector(logoTouched))
         // self.navigationItem.leftBarButtonItem = logoButton
         // let rightMenuButton = UIBarButtonItem(image: Images.kMenuIcon, style: .plain, target: self, action: #selector(rightMenuTouched))
          //self.navigationItem.rightBarButtonItem = rightMenuButton
      }
      
      //MARK:- CustomUI
         func customUI() {
          self.tableView.backgroundView =  UIImageView(image: UIImage(named: "login-bg"))
             /* Add done button to keyboard */
//            self.txtUserName.text = "angela@gmail.com"
//                self.txtPasswrd.text = "qwerty"
             let toolbarDone = UIToolbar.init()
             toolbarDone.sizeToFit()
             let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done,target: self, action: #selector(self.doneButtonClicked))
             barBtnDone.tintColor = kAppThemeYellowTop
             let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
             toolbarDone.items = [flexSpace,barBtnDone]
             txtFields.forEach { (txtField) in
                 txtField.setBorderAndCornerRadius(borderColor: UIColor.clear, borderWidth: 1.0, cornerRadius: 5.0)
                 
                 txtField.inputAccessoryView = toolbarDone
              
             }
           
             for (index,txtField) in txtFields.enumerated() {
                 txtField.setPlaceHolder(placeHolderText: Array.kPlaceHolderArrayLogin[index], color: .white)
             }
             
             self.btnSignIn.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
             hideKeyBoardWhenTapAround()
             
         }
      
       //MARK:- Button Actions
  
    @IBAction func buttonSignUpAction(_ sender: UIButton) {
        let signUpVC = kStoryboard.instantiateViewController(withIdentifier: "MESignupTableViewController") as! MESignupTableViewController
        self.navigationController?.pushViewController(signUpVC, animated: true)
       }
    
        @IBAction func buttonSignInAction(_ sender: UIButton) {
            if !self.txtUserName.text!.isEmpty && !self.txtPasswrd.text!.isEmpty {
                var deviceToekn = ""
                if  UserDefaults.standard.value(forKey: "kDeviceToken")  != nil {
                   deviceToekn =  UserDefaults.standard.value(forKey: "kDeviceToken") as! String
                    print("Device Token = \(deviceToekn)")
                }
                if kDeviceToken != ""{
                ServiceManager.sharedInstance.post(withServiceName: "login", andParameters: ["name":self.txtUserName.text!,"password":self.txtPasswrd.text!,"device_token":deviceToekn,"device_type":"ios"], withHud: true) { (success, result, error) in
                if success == true {
                    if (result as! NSDictionary)["message"] as! String == "Successfully Logged In" {
                        let userDetailsDict = result!["user_details"]! as! NSDictionary
                         MEUserDetails.currentUser.initWithDict(dict: userDetailsDict)
                        
                        let data = NSKeyedArchiver.archivedData(withRootObject: userDetailsDict)
                          UserDefaults.standard.set(data, forKey: "userDetails")
                        let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                                /* let keyWindow = UIApplication.shared.connectedScenes
                                 .filter({$0.activationState == .foregroundActive})
                                 .map({$0 as? UIWindowScene})
                                 .compactMap({$0})
                                 .first?.windows
                                 .filter({$0.isKeyWindow}).first*/
                         UIApplication.shared.keyWindow!.rootViewController = viewController
                    }
                    else {
                          self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                    }
                }
                else {
                    print(error?.localizedDescription)
                      self.showAlertWithOkButton(message: "Please try again")
                }
                }}else{
                    
                }
            }
                
            else {
                self.showAlertWithOkButton(message: "Please fill the fields")
            }
            
                   
        }
  
        @IBAction func buttonShowPwdAction(_ sender: UIButton) {
            self.txtPasswrd.isSecureTextEntry = !self.txtPasswrd.isSecureTextEntry
                      
        }
        @IBAction func buttonFrgtPwdAction(_ sender: UIButton) {
        let signUpVC = kStoryboard.instantiateViewController(withIdentifier: "MEForgotPasswordTVC") as! MEForgotPasswordTVC
                        
                          
        self.navigationController?.pushViewController(signUpVC, animated: true)
                        
        }
  /*  // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    
    
      //MARK:- Done button action keyboard
         @objc func doneButtonClicked() {
             self.view.endEditing(true)
         }
      
         //MARK:- Show Alert With Ok Button
            func showAlertWithOkButton(message:String){
                AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                    
                }
            }
      
      
      
      //MARK:- TextField Delgate
            func textFieldDidBeginEditing(_ textField: UITextField) {
              
                  textField.becomeFirstResponder()
                  
            }
         func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
            
             return true
         }
            func textFieldDidEndEditing(_ textField: UITextField) {
             
                textField.resignFirstResponder()
            }
      func textFieldShouldReturn(_ textField: UITextField) -> Bool {
           textField.resignFirstResponder()
          return true
      }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
