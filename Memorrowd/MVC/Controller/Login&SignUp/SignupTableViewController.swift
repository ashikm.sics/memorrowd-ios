//
//  SignupTableViewController.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 07/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//
protocol delegateDidSignUpPressed {
    func didSignUpPressed(dictOfSign:[String:Any],phoneNumber:String,type:String,dailCode:String,CountryName:String,CountryFlag:UIImage?)
}

import UIKit
import ADCountryPicker
import CoreLocation

class MESignupTableViewController: UITableViewController,UITextFieldDelegate,ADCountryPickerDelegate {
       @IBOutlet var txtUserName               : UITextField!
       @IBOutlet var txtPhoneNbr               : UITextField!
       @IBOutlet var txtEmail                  : UITextField!
       @IBOutlet var txtPasswrd                : UITextField!

       @IBOutlet var btnSignup                 : UIButton!
       @IBOutlet var btnSignIn                 : UIButton!
       @IBOutlet var txtFields                 : [UITextField]!
        @IBOutlet var buttonCountryCode          : UIButton!

    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    let picker = ADCountryPicker()
    var dialCode = ""
       var countryName  = ""
       var countryFlag = UIImage()
    var countryCodeArray = [MECountryList]()
    var countryCodePicked = "IN"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.customUI()
        showCountryList()
        determineMyCurrentLocation()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
                   let navBarAppearance = UINavigationBarAppearance()
                   navBarAppearance.configureWithOpaqueBackground()
                   navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.backgroundColor = kAppThemeYellowTop
                   self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                   self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
               }
        self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.setHidesBackButton(true, animated: true)
        //self.navigationItem.title = "Add To Cart"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
       
        //let logoButton = UIBarButtonItem(image: Images.kLogoSmallWhite, style: .plain, target: self, action: #selector(logoTouched))
       // self.navigationItem.leftBarButtonItem = logoButton
       // let rightMenuButton = UIBarButtonItem(image: Images.kMenuIcon, style: .plain, target: self, action: #selector(rightMenuTouched))
        //self.navigationItem.rightBarButtonItem = rightMenuButton
    }
    func determineMyCurrentLocation() {
        
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
    }
    func setCurrentCountryCode(){
       
            
            var lat = ""
            var long = ""
            
            if
               CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
               CLLocationManager.authorizationStatus() ==  .authorizedAlways
            {
                currentLocation = locManager.location
                 lat = "\(currentLocation.coordinate.latitude)"
                 long = "\(currentLocation.coordinate.longitude)"
            }
            
            let geoCoder = CLGeocoder()
            var placeMark: CLPlacemark!
            let coordinations = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude,longitude: currentLocation.coordinate.longitude)
             let location = CLLocation(latitude: coordinations.latitude, longitude: coordinations.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler: { [self] (placemarks, error) -> Void in
                            
                            if placemarks != nil{
                                
                                placeMark = placemarks?[0]
                                
                                if let city = placeMark.locality,let country = placeMark.country{
                                    if let countryCode = placeMark.isoCountryCode{
                                     picker.getDialCode(countryCode: countryCode, completion: { phoneCode in
                                                            if phoneCode != nil{
                                                                buttonCountryCode.setTitle(phoneCode!, for: .normal)
                                                            }
                                       
                                    })}
                                }
                                        
                                    }
                                })
                            }
    //MARK:- CustomUI
       func customUI() {
        self.tableView.backgroundView =  UIImageView(image: UIImage(named: "login-bg"))
           /* Add done button to keyboard */
        picker.delegate = self
                               /// Optionally, set this to display the country calling codes after the names
        picker.showCallingCodes = true
         /// The nav bar title to show on picker view
         picker.pickerTitle = "Select a Country"
         let currentLocale = NSLocale.current as NSLocale
          let countryCode = currentLocale.object(forKey: NSLocale.Key.countryCode) as? String
                             /// - Returns: the dial code for given country code if it exists
           if countryCode != nil {
             picker.getDialCode(countryCode: countryCode!, completion: {dialCode in
                self.buttonCountryCode.setTitle("\(dialCode ?? "")", for: .normal)
             })
           }
           let toolbarDone = UIToolbar.init()
           toolbarDone.sizeToFit()
           let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done,target: self, action: #selector(self.doneButtonClicked))
           barBtnDone.tintColor = kAppThemeYellowTop
           let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
           toolbarDone.items = [flexSpace,barBtnDone]
           txtFields.forEach { (txtField) in
               txtField.setBorderAndCornerRadius(borderColor: UIColor.clear, borderWidth: 1.0, cornerRadius: 5.0)
               
               txtField.inputAccessoryView = toolbarDone
            
           }
         
           for (index,txtField) in txtFields.enumerated() {
               txtField.setPlaceHolder(placeHolderText: Array.kPlaceHolderArrayForSginIn[index], color: .white)
           }
           
           self.btnSignup.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
           hideKeyBoardWhenTapAround()
          
       }
    
    
    
    //MARK:- Button Actions
    @IBAction func buttonSignUpAction(_ sender: UIButton) {
        if !self.txtPhoneNbr.text!.isEmpty && !self.txtEmail.text!.isEmpty && !self.txtPasswrd.text!.isEmpty && !self.txtUserName.text!.isEmpty {
        if self.txtEmail.text!.isValidEmail {
            if self.txtPhoneNbr.text!.count > 9{
            var deviceToekn = ""
            if  UserDefaults.standard.value(forKey: "kDeviceToken")  != nil {
               deviceToekn =  UserDefaults.standard.value(forKey: "kDeviceToken") as! String
            }
            guard isValidPhoneNumber else { return }
              print("country code \(dialCode)")
                if kDeviceToken != ""{
            ServiceManager.sharedInstance.post(withServiceName: "register", andParameters: ["number":self.txtPhoneNbr.text!,"email":self.txtEmail.text!,"password":self.txtPasswrd.text!,"name":self.txtUserName.text!,"device_token":kDeviceToken,"device_type":"ios","country_code": dialCode], withHud: true) { (success, result, error) in
                    if success == true {
                        if (result as! NSDictionary)["message"] as! String == "Successfully Registered" {
                            let userDetailsDict = result!["user_details"]! as! NSDictionary
                             MEUserDetails.currentUser.initWithDict(dict: userDetailsDict)
                            let data = NSKeyedArchiver.archivedData(withRootObject: userDetailsDict)
                            UserDefaults.standard.set(data, forKey: "userDetails")
                              let viewController = kStoryboard.instantiateViewController(withIdentifier: "MEChoosePlanVC") as! MEChoosePlanViewController
                            viewController.isFromSignUp = true
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                        else {
                              self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                        }
                    }
                    else {
                        print(error)
                          self.showAlertWithOkButton(message: "Please try again")
                    }
            }
                    
                }else{
                    
                }
            }else{
                self.showAlertWithOkButton(message: "Please fill a vaild Phone Number")
            }}else {
                    self.showAlertWithOkButton(message: "Please fill a vaild Email Id")
                }
        }
      else {
         self.showAlertWithOkButton(message: "Please fill the fields")
        }
          
               
    }
   
     @IBAction func buttonSignInAction(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
                
     }
    @IBAction func buttonPickerAction(_ sender: UIButton) {
          self.present(picker, animated: true, completion: nil)
       }
    
    
    
    
    // MARK: - Table view data source

  /*  override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 7
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        <#code#>
    }*/
    
    
    //MARK:- Done button action keyboard
       @objc func doneButtonClicked() {
           self.view.endEditing(true)
       }
    // MARK: - ADCountryPicker Delegate
       func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
           
           self.dialCode = dialCode
           if let i = countryCodeArray.firstIndex(where: { $0.phone_code == dialCode }) {
               countryCodePicked = countryCodeArray[i].phone_code ?? "IN"
           }
      //  self.countryName =  picker.getCountryName(countryCode: code) != nil ?  picker.getCountryName(countryCode: code)! : ""
         //  self.countryFlag = (picker.getFlag(countryCode: code) == nil ? UIImage(named: "img1"): picker.getFlag(countryCode: code)!)!
           self.buttonCountryCode.setTitle("\(dialCode)", for: .normal)
           self.dismiss(animated: true, completion: nil)
       }
       //MARK:- Show Alert With Ok Button
          func showAlertWithOkButton(message:String){
              AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                  
              }
          }
    
    
    
    //MARK:- TextField Delgate
          func textFieldDidBeginEditing(_ textField: UITextField) {
            
                textField.becomeFirstResponder()
                
          }
       func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
          
           return true
       }
          func textFieldDidEndEditing(_ textField: UITextField) {
           
              textField.resignFirstResponder()
          }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
         textField.resignFirstResponder()
        return true
    }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MESignupTableViewController {
    var isValidPhoneNumber: Bool {
        let phoneNumber = txtPhoneNbr.text ?? ""
        if !limitValidation(string: phoneNumber, minLength: 10, maxLength: 14) {
            showDefaultAlert(viewController: self, title: "", msg: "Please enter a valid phone number")
            return false
        }
        else {
            return true
        }
    }
}

extension MESignupTableViewController {
    func limitValidation(string: String, minLength: Int, maxLength: Int) -> Bool {
        if maxLength > 0{
            return (string.count >= minLength && string.count <= maxLength)
        } else {
            return (string.count >= minLength)
        }
        
    }
}
extension MESignupTableViewController {
    func showDefaultAlert(viewController: UIViewController, title: String, msg: String) {
        let alert = UIAlertController(title: title, message: msg, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { (action) in
        }))
        viewController.present(alert, animated: true, completion: nil)
    }
}

extension MESignupTableViewController {
    func showCountryList() {
        ServiceManager.sharedInstance.post(withServiceName: "countries", andParameters: [:], withHud: true) { (success, result, error) in
                    if success == true {
                        let listCountryDict = result as! NSDictionary
                        for items in listCountryDict.value(forKey: "packages") as! NSArray {
                            let obj = MECountryList().initWithDictionary(userDictionary: items as! NSDictionary)
                            self.countryCodeArray.append(obj)
                        }
                    }
                    else {
                        print(error)
                          self.showAlertWithOkButton(message: "Please try again")
                    }
                }
    }
    
}

extension MESignupTableViewController : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.setCurrentCountryCode()
    }}
