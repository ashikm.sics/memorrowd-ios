//
//  MEForgotPasswordTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 23/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class MEForgotPasswordTVC: UITableViewController {
    @IBOutlet var txtEmail                  : UITextField!
    @IBOutlet var btnReset                  : UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.customUI()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    //MARK:- Set Up NavigationBar
       func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
                   let navBarAppearance = UINavigationBarAppearance()
                   navBarAppearance.configureWithOpaqueBackground()
                   navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.backgroundColor = kAppThemeYellowTop
                   self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                   self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
               }
           self.navigationController?.navigationBar.isHidden = false
           self.navigationItem.setHidesBackButton(true, animated: true)
           //self.navigationItem.title = "Add To Cart"
           self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
           self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
          
           self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
           let sideButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backAction))
           self.navigationItem.leftBarButtonItem = sideButtonButton
       }
       
       //MARK:- CustomUI
          func customUI() {
           self.tableView.backgroundView =  UIImageView(image: UIImage(named: "login-bg"))
              /* Add done button to keyboard */
              let toolbarDone = UIToolbar.init()
              toolbarDone.sizeToFit()
              let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done,target: self, action: #selector(self.doneButtonClicked))
              barBtnDone.tintColor = kAppThemeYellowTop
              let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
              toolbarDone.items = [flexSpace,barBtnDone]
              txtEmail.setBorderAndCornerRadius(borderColor: UIColor.clear, borderWidth: 1.0, cornerRadius: 5.0)
              txtEmail.inputAccessoryView = toolbarDone
              txtEmail.setPlaceHolder(placeHolderText: "Email", color: .white)
             
              
              self.btnReset.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
              hideKeyBoardWhenTapAround()
              
          }
       @objc func backAction() {
                 self.navigationController?.popViewController(animated: true)
                
                 }
    //MARK:- Button Actions
    @IBAction func buttonResetAction(_ sender: UIButton) {
    if !self.txtEmail.text!.isEmpty {
           if self.txtEmail.text!.isValidEmail {
           ServiceManager.sharedInstance.post(withServiceName: "forgotPassword", andParameters: ["email":self.txtEmail.text!], withHud: true) { (success, result, error) in
                       if success == true {
                           if (result as! NSDictionary)["message"] as! String == "Logged In" {
                             // "message": "Please check your email to reset your password",
                            let alert = UIAlertController(title: "oops", message: (result as! NSDictionary)["message"] as! String, preferredStyle: .alert)
                             let actionOk = UIAlertAction(title:"OK", style: .default) { (action) in
                                
                                   self.navigationController?.popViewController(animated: true)
                                                          }
                                                          alert.addAction(actionOk)
                                                          self.present(alert, animated: true){
                                                              
                                                          }
                           }
                           else {
                                 self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                           }
                       }
                       else {
                             self.showAlertWithOkButton(message: "Please try again")
                       }
                   }
                   }
                   else {
                       self.showAlertWithOkButton(message: "Please fill vaild Email Id")
                   }
           }
         else {
            self.showAlertWithOkButton(message: "Please fill the fields")
           }
    }
    //MARK:- Done button action keyboard
          @objc func doneButtonClicked() {
              self.view.endEditing(true)
          }
       
          //MARK:- Show Alert With Ok Button
             func showAlertWithOkButton(message:String){
                 AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                      
                 }
             }
       
       
       
       //MARK:- TextField Delgate
             func textFieldDidBeginEditing(_ textField: UITextField) {
               
                   textField.becomeFirstResponder()
                   
             }
          func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
             
              return true
          }
             func textFieldDidEndEditing(_ textField: UITextField) {
              
                 textField.resignFirstResponder()
             }
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
           return true
       }
   /* // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
