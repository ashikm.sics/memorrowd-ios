//
//  MENotificationsTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 14/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class MENotificationsTVC: UITableViewController {
   var arrayOfNotification = [NSDictionary]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.customUI()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
         
         super.viewWillAppear(true)
          self.getLastestList()
     
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
         
       }
    //MARK:- Set Up NavigationBar
          func setUpNavigationBar() {
            if #available(iOS 13.0, *) {
                       let navBarAppearance = UINavigationBarAppearance()
                       navBarAppearance.configureWithOpaqueBackground()
                       navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                       navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                       navBarAppearance.backgroundColor = kAppThemeYellowTop
                       self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                       self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
                   }
              self.navigationController?.navigationBar.isHidden = false
              self.navigationItem.setHidesBackButton(true, animated: true)
              self.navigationItem.title = "Notifications"
                    self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
            
              self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
          
          }
             //MARK:- CustomUI
                func customUI() {
                   self.tableView.reloadData()
                }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
         @objc func actionPushNotificationInActiveState(_ notification: Notification) {
    
          let message = "Its time to post your post."
               
                 let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
                 let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
                    let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                      listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                     self.navigationController?.present(listVC, animated: true, completion: {
                        
                     })
                  
                 })
                 let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                     self.dismiss(animated: true, completion: nil)
                 })
                 alertController.addAction(buttonView)
                 alertController.addAction(buttonCancel)
                 self.present(alertController, animated: true, completion: nil)
                 
             
         }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
              let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                              listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                             self.navigationController?.present(listVC, animated: true, completion: {
                                
                             })
      }
      // MARK: - Table view data source
     override func numberOfSections(in tableView: UITableView) -> Int {
         // #warning Incomplete implementation, return the 8 of sections
         return 1
     }

     override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         // #warning Incomplete implementation, return the number of rows
        return self.arrayOfNotification.count
     }
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
              let cell  = tableView.dequeueReusableCell(withIdentifier: "cell") as! sharedVideoTableViewCell
        cell.labelVideosName.text = ((self.arrayOfNotification[indexPath.row] as! NSDictionary).value(forKey: "message") as! String)
              return cell
          }
     override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
         return 88
     }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
          let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
        listVC.fileId  =  ((self.arrayOfNotification[indexPath.row] ).value(forKey: "notid") as! Int)
                           self.navigationController?.present(listVC, animated: true, completion: {
                              
                           })
    }
    func getLastestList() {
        ServiceManager.sharedInstance.post(withServiceName: "notificationlist", andParameters: ["user_id":MEUserDetails.currentUser.id!], withHud: true) { (success, result, error) in
                 
                 if success == true {
                     if (result as! NSDictionary)["message"] as! String == "Success" {
                         self.arrayOfNotification.removeAll()
                         let listDetailsDict = result as! NSDictionary
                       
                         for items in listDetailsDict.value(forKey: "notifications") as! NSArray {
                            
                            self.arrayOfNotification.append(items as! NSDictionary)
                         }
                       self.tableView.reloadData()
 
                     }
                     else {
                          // self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                     }
                 }
                 else {
                       self.showAlertWithOkButton(message: "Please try again")
                 }
             }}
       //MARK:- Show Alert With Ok Button
                   func showAlertWithOkButton(message:String){
                       AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                           
                       }
                   }
             
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
