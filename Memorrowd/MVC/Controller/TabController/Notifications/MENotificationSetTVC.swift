//
//  MENotificationSetTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 15/10/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import FBSDKShareKit
import TwitterKit
import SCSDKCreativeKit
import SCSDKStoryKit
import MessageUI
import Social
import TikTokOpenSDK
import AVFoundation
import MediaPlayer
import AVKit
import SVProgressHUD

class MENotificationSetTVC: UITableViewController,UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate,TWTRComposerViewControllerDelegate,SharingDelegate {
    var fileId = 0
    var arrayOfImages = ["fb_Square","twitter_square","insta_Square","snapchat_Square","linkedin_Square","whatsapp_square","email_square","plus"]
       
         
      var arrayOfImagesShareName = ["Facebook","Twitter","Instagram","Snapchat","Linkedin","Whatsapp","Email","More"]
    var arrayOfMediaSelected = [String]()
    var playerLayer                      = AVPlayerLayer()
          let playerController                 = AVPlayerViewController()
      var destinationUrl = ""
    var type = ""
    var filPath = ""
    var from = ""
    @IBOutlet var viewPlayVideo                    : UIView!
    @IBOutlet var imageViewMedia                   : UIImageView!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView()
        self.getFileDeatils()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    //MARK:- Play Video
         func playVideo()  {
             guard let path = Bundle.main.path(forResource: "YogaTutorial", ofType:"mp4") else {
                 debugPrint("Dummy Video not found")
                 return
             }
            /* if dictOfVideo.video! != "" {
             let player = AVPlayer(url: URL(string: dictOfVideo.video!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!)!)
             playerController.player = player
             }*/
            let player = AVPlayer(url: URL(string: self.filPath)!)
             playerController.player = player
             self.viewPlayVideo.addSubview(playerController.view)
             playerController.view.frame = CGRect(x: 0, y: 0, width: self.viewPlayVideo.frame.width, height: self.viewPlayVideo.frame.height)
             playerController.view.tintColor = kAppThemeYellowTop
             playerController.view.setBorderAndCornerRadius(borderColor: .clear, borderWidth: 0.0, cornerRadius: 5.0)
             playerController.player?.play()
          
         }
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
                   let navBarAppearance = UINavigationBarAppearance()
                   navBarAppearance.configureWithOpaqueBackground()
                   navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.backgroundColor = kAppThemeYellowTop
                   self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                   self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
               }
        self.navigationController?.navigationBar.isHidden = true
        self.navigationItem.setHidesBackButton(true, animated: true)
        //self.navigationItem.title = "Add To Cart"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
        let sideButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backAction))
                        self.navigationItem.leftBarButtonItem = sideButtonButton
            }
    @objc func backAction() {
        if self.from ==  "appDelegate" {
         let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
         
           UIApplication.shared.keyWindow!.rootViewController = viewController
        }
        else {
            self.dismiss(animated: true) {
              
            }
        }
                 
            }
    
    //MARK:- Button Actions
    @IBAction func buttonCloseAction(_ sender: UIButton) {
             if self.from ==  "appDelegate" {
       let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
       
         UIApplication.shared.keyWindow!.rootViewController = viewController
      }
      else {
          self.dismiss(animated: true) {
            
          }
      }
    }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.arrayOfImages.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath)
        let labelName = cell.viewWithTag(1) as! UILabel
        let imageView = cell.viewWithTag(2) as! UIImageView
        labelName.text = self.arrayOfImagesShareName[indexPath.row]
        imageView.image = UIImage(named: self.arrayOfImages[indexPath.row])
        // Configure the cell...

        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.arrayOfMediaSelected.contains(self.arrayOfImagesShareName[indexPath.row]){
        return 82
    }
    else {
    return 0
    }
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
              
        if self.type == "Image" {
                          
                         if indexPath.row == 0 {
                             //Facebook
                          
                           let sharePhoto = SharePhoto()
                           sharePhoto.image = self.imageViewMedia.image!
                           sharePhoto.isUserGenerated = true
                           sharePhoto.caption = ""
                           let photoContent = SharePhotoContent()
                           photoContent.photos = [sharePhoto]
                           let showDialog = ShareDialog.init(fromViewController: self, content: photoContent, delegate: self)
                           if (showDialog.canShow) {
                               showDialog.show()
                           } else {
                              print("It looks like you don't have the Facebook mobile app on your phone.")
                           }
                         }
                         else if indexPath.row == 1 {
                           //Twitter
                         /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
                             API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
                             Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
                             // Swift
                            if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                                       // App must have at least one logged-in user to compose a Tweet
                                       let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.imageViewMedia.image!, videoURL: nil)
                                       present(composer, animated: true, completion: nil)
                                   } else {
                                       // Log in, and then check again
                                   
                                       TWTRTwitter.sharedInstance().logIn { session, error in
                                           if session != nil { // Log in succeeded
                                               let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.imageViewMedia.image!, videoURL: nil)
                                               self.present(composer, animated: true, completion: nil)
                                           } else {
                                               self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                              
                                           }
                                       }
                                   }
                             
                         }
                         else if indexPath.row == 2 {
                             //Instagram
                           PHPhotoLibrary.shared().performChanges({
                               PHAssetChangeRequest.creationRequestForAsset(from: self.imageViewMedia.image!)
                           }, completionHandler: { [weak self] success, error in
                               if success {
                                   let fetchOptions = PHFetchOptions()
                                   fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                                   let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                                   if let lastAsset = fetchResult.firstObject {
                                       let localIdentifier = lastAsset.localIdentifier
                                       
                                       let urlFeed = "instagram://library?LocalIdentifier=" + localIdentifier
                                       guard let url = URL(string: urlFeed) else {
                                           print("Could not open url")
                                           return
                                       }
                                       DispatchQueue.main.async {
                                           if UIApplication.shared.canOpenURL(url) {
                                               if #available(iOS 10.0, *) {
                                                   UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                                                       print("self?.delegate?.success")
                                                   })
                                               } else {
                                                   UIApplication.shared.openURL(url)
                                                   print("self?.delegate?.success")
                                                   
                                               }
                                           } else {
                                               print("Instagram not found")
                                           }
                                       }
                                   }
                               } else if let error = error {
                                   print(error.localizedDescription)
                               }
                               else {
                                   print("Could not save the photo")
                               }
                           })
                         }
                       
                         else if indexPath.row == 3 {
                             //SnapChat
                             // Copied from Snapchat Documentation

                           let snapPhoto = SCSDKSnapPhoto(image: self.imageViewMedia.image!)
                                   let snap = SCSDKPhotoSnapContent(snapPhoto: snapPhoto)
                                   // snap.sticker = /* Optional, add a sticker to the Snap */
                                   // snap.caption = /* Optional, add a caption to the Snap */
                                   // snap.attachmentUrl = /* Optional, add a link to the Snap */
                                     SVProgressHUD.show()
                                  
                                   SCSDKSnapAPI(content: snap).startSnapping() { (error: Error?) in
                                        SVProgressHUD.dismiss()
                                       if let error = error {
                                           print(error.localizedDescription)
                                       } else {
                                            self.addshareDetails(app: "snapchat")
                                           // Successfully shared content to Snapchat!
                                       }
                                   }
                                  
                                  
                          
                         
                         }
                         else if indexPath.row == 4 {
                            // LinkedIn
                         let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                                                                        
                           self.navigationController?.pushViewController(webVC, animated: true)
                            // self.sendToLinkedIn(image: UIImage(named: "img1")!)
                         }
                         else if indexPath.row == 5 {
                             let items = [ self.imageViewMedia.image!]
                               let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                                              present(ac, animated: true)
                           
                           ac.completionWithItemsHandler = { activity, completed, items, error in
                                                        if !completed {
                                                            // handle task not completed
                                                            return
                                                        }
                                                        else {
                                                             self.addshareDetails(app: "more")
                                                        }
                                                      
                                                    }
                             //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
                         }
                         else if indexPath.row == 6 {
                             if MFMailComposeViewController.canSendMail() {
                                    let mail = MFMailComposeViewController()
                                    mail.mailComposeDelegate = self;
                                    mail.setCcRecipients(["yyyy@xxx.com"])
                                    mail.setSubject("Your message")
                                    mail.setMessageBody("Message body", isHTML: false)
                               let imageData: NSData = self.imageViewMedia.image!.pngData()! as NSData
                                    mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                                    self.present(mail, animated: true, completion: nil)
                                  }
                         }
                         else {
                             let items = [self.imageViewMedia.image!]
                                   let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                                   present(ac, animated: true)
                           
                           ac.completionWithItemsHandler = { activity, completed, items, error in
                                                        if !completed {
                                                            // handle task not completed
                                                            return
                                                        }
                                                        else {
                                                             self.addshareDetails(app: "more")
                                                        }
                                                      
                                                    }
                         }
                       }
                       else {
                                  if indexPath.row == 0 {
                                                
                                            let content: ShareVideoContent = ShareVideoContent()
                                 
                                          DispatchQueue.main.async {
                                                                   let video = ShareVideo()
                                              print(self.destinationUrl)
                                              video.videoURL =  URL(string:self.destinationUrl)
                                                                   content.video = video
                                                                   
                                                                   let shareDialog = ShareDialog()
                                                                   shareDialog.shareContent = content
                                                                   shareDialog.mode = .native
                                                                   shareDialog.delegate = self
                                                                   shareDialog.show()
                                                               }
                                    
                                               
                                                
                                          
                                  }
                                  else if indexPath.row == 1 {
                                      //Twitter
                                  /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
                                      API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
                                      Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
                                      // Swift
                                     if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                                                // App must have at least one logged-in user to compose a Tweet
                                                let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.filPath))
                                                present(composer, animated: true, completion: nil)
                                            } else {
                                                // Log in, and then check again
                                            
                                                TWTRTwitter.sharedInstance().logIn { session, error in
                                                    if session != nil { // Log in succeeded
                                                       let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.filPath))
                                                        self.present(composer, animated: true, completion: nil)
                                                    } else {
                                                        self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                                       
                                                    }
                                                }
                                            }
                                      
                                  }
                                  else if indexPath.row == 2 {
                                      //instagram
                                             DispatchQueue.main.async {
                                              let urlFeed = "instagram://library?LocalIdentifier=" + self.destinationUrl
                                                       guard let url = URL(string: urlFeed) else {
                                                           print("Could not open url")
                                                           return
                                                       }
                                                       DispatchQueue.main.async {
                                                           if UIApplication.shared.canOpenURL(url) {
                                                               if #available(iOS 10.0, *) {
                                                                   UIApplication.shared.open(url, options: [:]
                                       , completionHandler: { (success) in
                                                                       print("self?.delegate?.success")
                                                                   })
                                                               } else {
                                                                   UIApplication.shared.openURL(url)
                                                                   print("self?.delegate?.success")
                                                               }
                                                           } else {
                                                               print("Instagram not found")
                                                           }
                                                       }
                                                   }
                                  }
                                      
                           
                                  else if indexPath.row == 3 {
                                      //SnapChat
                                      // Copied from Snapchat Documentation
                                       
                                      let video = SCSDKSnapVideo(videoUrl: URL(string:self.filPath)!)
                                      let videoContent = SCSDKVideoSnapContent(snapVideo: video)
                                         
                                    
                                            SVProgressHUD.show()
                                           
                                            SCSDKSnapAPI(content: videoContent).startSnapping() { (error: Error?) in
                                                SVProgressHUD.dismiss()
                                                if let error = error {
                                                    print(error.localizedDescription)
                                                } else {
                                                    self.addshareDetails(app: "snapchat")
                                                    // Successfully shared content to Snapchat!
                                                }
                                            }
                                           
                                     
                                  
                                  }
                                  else if indexPath.row == 4 {
                                      //LinkedI
                                    let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                                                                                
                                              self.navigationController?.pushViewController(webVC, animated: true)
                                     // self.sendToLinkedIn(image: UIImage(named: "img1")!)
                                  }
                                  else if indexPath.row == 5 {
                                 let urlData = NSData(contentsOf: NSURL(string:self.filPath)! as URL)
                            SVProgressHUD.show()
                                            if ((urlData) != nil){

                                                print(urlData)


                                               let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                                                let docDirectory = paths[0]
                                                let filePath = "\(docDirectory)/tmpVideo.mp4"
                                               urlData?.write(toFile: filePath, atomically: true)
                                                // file saved

                                                let videoLink = NSURL(fileURLWithPath: filePath)


                                                let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
                                                let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                                                activityVC.setValue("Video", forKey: "subject")


                                                SVProgressHUD.dismiss()

                                               self.present(activityVC, animated: true, completion: nil)
                                               activityVC.completionWithItemsHandler = { activity, completed, items, error in
                                                                            if !completed {
                                                                                // handle task not completed
                                                                                return
                                                                            }
                                                                            else {
                                                                                 self.addshareDetails(app: "more")
                                                                            }
                                                                          
                                                                        }
                                            }
                                      //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
                                  }
                                  else if indexPath.row == 6 {
                                     if MFMailComposeViewController.canSendMail() {
                                             let mail = MFMailComposeViewController()
                                             mail.mailComposeDelegate = self;
                                             mail.setCcRecipients(["yyyy@xxx.com"])
                                             mail.setSubject("Your message")
                                             mail.setMessageBody("Message body", isHTML: false)
                                         do {
                                          let fileData: NSData = try Data(contentsOf: URL(string: self.filPath)!) as NSData
                                          mail.addAttachmentData(fileData as Data, mimeType: "mp4", fileName: "myfile.mp4")

                                             self.present(mail, animated: true, completion: nil)
                                           
                                      }
                                      catch {
                                          print("error")
                                      }
                                      }
                                  }
                                  else {
                                      let urlData = NSData(contentsOf: NSURL(string:self.filPath)! as URL)

                                   if ((urlData) != nil){
                            SVProgressHUD.show()
                                       print(urlData)


                                      let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                                       let docDirectory = paths[0]
                                       let filePath = "\(docDirectory)/tmpVideo.mp4"
                                      urlData?.write(toFile: filePath, atomically: true)
                                       // file saved

                                       let videoLink = NSURL(fileURLWithPath: filePath)


                                       let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
                                       let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                                       activityVC.setValue("Video", forKey: "subject")

                            SVProgressHUD.dismiss()
                                     

                                      self.present(activityVC, animated: true, completion: nil)
                                       activityVC.completionWithItemsHandler = { activity, completed, items, error in
                                                                                             if !completed {
                                                                                                 // handle task not completed
                                                                                                 return
                                                                                             }
                                                                                             else {
                                                                                                  self.addshareDetails(app: "more")
                                                                                             }
                                                                                           
                                                                                         }
                                   }
                                
                                  }
                       }
    }
    
       func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
           self.addshareDetails(app: "mail")
           controller.dismiss(animated: true, completion: nil)
       }
       
       // MARK: WhatsApp
       func sendVideoToWhtsApp() {

           let path = Bundle.main.path(forResource: "YogaTutorial", ofType:"mp4")!
           let fileUrl = NSURL(fileURLWithPath: path)

           let docController = UIDocumentInteractionController(url: fileUrl as URL)
           docController.uti = "net.whatsapp.movie"
           docController.presentPreview(animated: true)

       }
     
      
       func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
           self.addshareDetails(app: "twitter")
           print("success")
       }
       func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
            print("Fail")
       }
       
       func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
           self.addshareDetails(app: "facebook")
           print("")
       }
       
       func sharer(_ sharer: Sharing, didFailWithError error: Error) {
            print("")
       }
       
       func sharerDidCancel(_ sharer: Sharing) {
           print("")
       }
    func getFileDeatils(){
        //MEUserDetails.currentUser.id!
        ServiceManager.sharedInstance.post(withServiceName: "socialsharepost", andParameters: ["notid":self.fileId,"id":MEUserDetails.currentUser.id!], withHud: true) { (success, result, error) in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    self.arrayOfMediaSelected.removeAll()
                    let details = ((result as! NSDictionary)["details"] as! NSDictionary) ["socialmedia"] as! String
                    for items in details.components(separatedBy: ",") {
                        self.arrayOfMediaSelected.append(items)
                    }
                    self.type = ((result as! NSDictionary)["details"] as! NSDictionary) ["file_type"] as! String
                    self.filPath = ((result as! NSDictionary)["details"] as! NSDictionary) ["file"] as! String
                   if self.type ==  "Image" {
                     self.imageViewMedia.isHidden = false
                    self.imageViewMedia.downloaded(from: self.filPath)
                   }
                   else {
                     self.imageViewMedia.isHidden = true
                    self.playVideo()
                     self.downloadVideoLinkAndCreateAsset() { (url) in
                     self.destinationUrl = url
                        }
                    }
                    self.tableView.reloadData()

                }
                else {
                  self.tableView.reloadData()
                }
            }
            else {
                  self.showAlertWithOkButton(message: "Please try again")
            }
        }
        
    }
    
    func addshareDetails(app:String){
             
        ServiceManager.sharedInstance.post(withServiceName: "addshare", andParameters: ["user_id":MEUserDetails.currentUser.id!,"fileid":"\(self.fileId)","socialapp":app], withHud: true) { (success, result, error) in
                              if success == true {
                                  if (result as! NSDictionary)["message"] as! String == "Success" {
                                     
                                  }
                                  else {
                                       // self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                                  }
                              }
                              else {
                                   // self.showAlertWithOkButton(message: "Please try again")
                              }
                          }
               
                    
            }
   //MARK:- Show Alert With Ok Button
         func showAlertWithOkButton(message:String){
             AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                 
             }
         }

   func downloadVideoLinkAndCreateAsset(completion: @escaping (String) -> Void) {

       // use guard to make sure you have a valid url
     
        SVProgressHUD.show()

      DispatchQueue.global(qos: .background).async {
          if let url = URL(string:self.filPath),
              let urlData = NSData(contentsOf: url) {
              let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
              let filePath="\(documentsPath)/tempFile.mp4"
              DispatchQueue.main.async {
                  urlData.write(toFile: filePath, atomically: true)
                var videoAssetPlaceholder:PHObjectPlaceholder!
                  PHPhotoLibrary.shared().performChanges({
                      let request =  PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                    videoAssetPlaceholder = request!.placeholderForCreatedAsset
                  }) { completed, error in
                     SVProgressHUD.dismiss()
                      if completed {
                          print("Video is saved!")
                       let localID = NSString(string: videoAssetPlaceholder.localIdentifier)
                       let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: NSString.CompareOptions.regularExpression, range: NSRange())
                       let ext = "mp4"
                       let assetURLStr =
                       "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                       
                       completion(assetURLStr)
                      }
                  }
              }
          }
      }
   }
   func createAssetURL(url: URL, completion: @escaping (String) -> Void) {
          let photoLibrary = PHPhotoLibrary.shared()
          var videoAssetPlaceholder:PHObjectPlaceholder!
          photoLibrary.performChanges({
              let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
              videoAssetPlaceholder = request!.placeholderForCreatedAsset
          },completionHandler: { success, error in
              if success {
                  let localID = NSString(string: videoAssetPlaceholder.localIdentifier)
                  let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: NSString.CompareOptions.regularExpression, range: NSRange())
                  let ext = "mp4"
                  let assetURLStr =
                  "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                  
                  completion(assetURLStr)
              }
          })
      }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
