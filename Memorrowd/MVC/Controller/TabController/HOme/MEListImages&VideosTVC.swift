//
//  MEListImages&VideosTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 08/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import UIKit
import AVFoundation
import MediaPlayer
import AVKit
import FBSDKShareKit
import TwitterKit
import SCSDKCreativeKit
import SCSDKStoryKit
import MessageUI
import Social
import TikTokOpenSDK
import SVProgressHUD

class MEListImagesVideosTVC: UITableViewController,UICollectionViewDelegate,UICollectionViewDataSource,UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate,TWTRComposerViewControllerDelegate,SharingDelegate, popUpDelegate {
    
    @IBOutlet var btnImages                        : UIButton!
    @IBOutlet var btnVideos                        : UIButton!
    @IBOutlet var viewImages                       : UIView!
    @IBOutlet var viewVideos                       : UIView!
    @IBOutlet var collectionViewShare              : UICollectionView!
    @IBOutlet var viewOfShare                      : UIView!
    var from                           = "image"
    
    var index                           = 0
    var isLoadMoreData                  = Bool()
    var isAvailableData                 = Bool()
    var refreshContrl = UIRefreshControl()
    
    var arrayOfImagesShare = ["fb_round","twitter_round","insta_round","snapchat_round","linkedin_round","whatsapp_round","email_round","more"]
    var arrayOfImagesShareName = ["Facebook","Twitter","Instagram","Snapchat","Linkedin","Whatsapp","Email","More"]
    var selected = 1
    var arrayOfimagesOrVideo = [listOfFiles]()
    var videoUrlArray = [String]()
    var selectedIndex = 0
    var dictOfDetails = listOfFiles()
    var selectedImage = UIImageView()
    var destinationUrl = ""
    var noOfVideo = ""
    var noOfimages = ""
    var videoThumbDict = [String: UIImage]()
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.tableFooterView = UIView()
        self.setUpNavigationBar()
        self.customUI()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false
        
        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.isLoadMoreData              = false
        self.isAvailableData             = false
        self.index                       = 0
        
        if  from != "video"{
            self.selected = 1
        }
        else {
            
            self.selected = 0
        }
        self.btnImages.setTitle("Images(\(noOfimages))", for: .normal)
        self.btnVideos.setTitle("Videos(\(noOfVideo))", for: .normal)
        if self.selected == 1 && from != "video" {
            btnImages.setTitleColor(kAppThemeYellowTop, for: .normal)
            self.btnVideos.setTitleColor(UIColor.darkGray, for: .normal)
            
            
            self.viewImages.backgroundColor = kAppThemeYellowTop
            self.viewVideos.backgroundColor = UIColor.darkGray
            self.navigationItem.title = "Images"
            self.arrayOfimagesOrVideo.removeAll()
            self.tableView.reloadData()
            self.getLastestList(type: "Image")
        }
        else{
            btnVideos.setTitleColor(kAppThemeYellowTop, for: .normal)
            self.btnImages.setTitleColor(UIColor.darkGray, for: .normal)
            self.viewImages.backgroundColor = UIColor.darkGray
            self.viewVideos.backgroundColor = kAppThemeYellowTop
            self.arrayOfimagesOrVideo.removeAll()
            self.navigationItem.title = "Videos"
            self.tableView.reloadData()
            self.getVideoList()
            self.getLastestList(type: "Video")
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
        self.dismissPopUpView()
    }
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = kAppThemeYellowTop
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.title = "Videos"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
        
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
        let sideButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = sideButtonButton
        
    }
    func getCell(with text:String)->UITableViewCell{
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! lastestVideoTableViewCell
        cell.btnPlay.isHidden = true
        cell.btnShare.isHidden = true
        cell.btnthreeDot.isHidden = true
        cell.imageViewVideos.isHidden = true
        cell.labelVideosTime.isHidden = true
        cell.noMediaLabel.text = text
        cell.noMediaLabel.isHidden = false
        return cell
    }
    //MARK:- CustomUI
    func customUI() {
        
        let width = (kScreenWidth - 60) / 3
        let columnLayout = ColumnFlowLayout.init(cellsPerRow: 3, minimumInteritemSpacing: 5.0, minimumLineSpacing: 10.0, sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), cellHeight: 100, cellWidth: width,scrollDirec: .vertical)
        
        collectionViewShare?.collectionViewLayout = columnLayout
        collectionViewShare.reloadData()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action:#selector(tapBlurButton))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        refreshContrl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshContrl.addTarget(self, action: #selector(MEListImagesVideosTVC.refresh(sender:)), for: UIControl.Event.valueChanged)
        tableView.addSubview(refreshContrl) //
        collectionViewShare.reloadData()
        
    }
    @objc func backAction() {
        self.navigationController?.popViewController(animated: true)
        
    }
    
    
    //MARK:- Button Actions
    @IBAction func buttonImagesAction(_ sender: UIButton) {
        selected = 1
        sender.setTitleColor(kAppThemeYellowTop, for: .normal)
        self.btnVideos.setTitleColor(UIColor.darkGray, for: .normal)
        self.viewImages.backgroundColor = kAppThemeYellowTop
        self.viewVideos.backgroundColor = UIColor.darkGray
        self.navigationItem.title = "Images"
        self.getLastestList(type:"Image")
    }
    
    @IBAction func buttonVideosAction(_ sender: UIButton) {
        selected = 0
        sender.setTitleColor(kAppThemeYellowTop, for: .normal)
        self.btnImages.setTitleColor(UIColor.darkGray, for: .normal)
        self.viewImages.backgroundColor = UIColor.darkGray
        self.viewVideos.backgroundColor = kAppThemeYellowTop
        self.navigationItem.title = "Videos"
        self.getLastestList(type:"Video")
        self.getVideoList()
        
    }
    @IBAction func buttonShareAction(_ sender: UIButton) {
        
        
        selectedIndex = sender.tag
        if selected ==  0 {
            SVProgressHUD.show()
            
            self.downloadVideoLinkAndCreateAsset(path: self.arrayOfimagesOrVideo[selectedIndex].path!) { (url) in
                SVProgressHUD.dismiss()
                
                self.destinationUrl = url
                self.showSharePopup()
            }
        }
        else {
            guard let cell = sender.superview?.superview as? lastestVideoTableViewCell else {
                return // or fatalError() or whatever
            }
            
            let indexPath = self.tableView.indexPath(for: cell)
            if  cell.imageViewVideos.image != nil {
                selectedImage.image = cell.imageViewVideos.image!
                self.showSharePopup()
            }
            
        }
        
    }
    //     @IBAction func buttonDetialAction(_ sender: UIButton) {
    //        let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEImageDetailViewController") as! MEImageDetailViewController
    //        listVC.dictOfDetails = self.arrayOfimagesOrVedio[sender.tag]
    //        self.navigationController?.pushViewController(listVC, animated: true)
    //    }
    @IBAction func buttoPlayAction(_ sender: UIButton) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEDeatilsPageTVC") as! MEDeatilsPageTVC
        listVC.dictOfDetails = self.arrayOfimagesOrVideo.reversed()[sender.tag]
        listVC.videoUrlArray = self.videoUrlArray
        listVC.videoThumbDict = self.videoThumbDict
        listVC.type = "video"
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    @IBAction func buttonCloseAction(_ sender: UIButton) {
        self.dismissPopUpView()
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if self.selected == 1 {
            if arrayOfimagesOrVideo.count == 0{
                return 1
            }
            return  self.arrayOfimagesOrVideo.count
        }
        else {
            if videoUrlArray.count == 0{
                return 1
            }
            return self.videoUrlArray.count
        }
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 168
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.row == 0{
            if selected == 1{
                if arrayOfimagesOrVideo.count == 0{
                  
                    return getCell(with: "Nothing to show, Please upload files")
                }
            }else{
                if videoUrlArray.count == 0{
                    return getCell(with: "Nothing to show, Please upload files")

                }
            }
        }
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell") as! lastestVideoTableViewCell
        cell.imageViewVideos.image = UIImage(named: "placeholder")
        cell.btnShare.tag = indexPath.row
        cell.btnthreeDot.tag = indexPath.row
        cell.btnPlay.tag = indexPath.row
        cell.labelVideosTime.text = ""
        cell.imageViewVideos.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
        cell.imageViewVideos.contentMode = .scaleAspectFill
        if self.selected == 1 {
            cell.btnthreeDot.isHidden = true
            cell.btnPlay.isHidden = true
            cell.imageViewVideos.downloaded(from:  self.arrayOfimagesOrVideo[indexPath.row].path!)
        }
        else {
            cell.btnthreeDot.isHidden = true
            
            cell.btnPlay.isHidden = false
            //         cell.imageViewVideos.image =   UIImage(named: "img1")
            //                let videoUrl = URL(fileURLWithPath: videoUrlArray[indexPath.row])
            //                cell.imageViewVideos.image = thumbnailForVideoAtURL(url: videoUrl as NSURL)
            cell.imageViewVideos.image = videoThumbDict[videoUrlArray[indexPath.row]]
            
        }
        return cell
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if selected == 0 {
            guard videoUrlArray.count < 0 else{ return}
            let videoUrl = videoUrlArray[indexPath.row]
            for video in arrayOfimagesOrVideo {
                if video.path == videoUrl {
                    let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEDeatilsPageTVC") as! MEDeatilsPageTVC
                    listVC.dictOfDetails = self.arrayOfimagesOrVideo.reversed()[indexPath.row]
                    listVC.videoUrlArray = self.videoUrlArray
                    listVC.videoThumbDict = self.videoThumbDict
                    listVC.type = "video"
                    self.navigationController?.pushViewController(listVC, animated: true)
                }
            }
        }
        else {
            /* let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEImageDetailViewController") as! MEImageDetailViewController
             listVC.dictOfDetails = self.arrayOfimagesOrVedio[indexPath.row]
             self.navigationController?.pushViewController(listVC, animated: true)*/
            
            let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEDeatilsPageTVC") as! MEDeatilsPageTVC
            listVC.dictOfDetails = self.arrayOfimagesOrVideo[indexPath.row]
            listVC.type = "image"
            self.navigationController?.pushViewController(listVC, animated: true)
            
        }
        
    }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
    @objc func actionPushNotificationInActiveState(_ notification: Notification) {
        
        let message = "Its time to post your post."
        
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
        let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
            self.navigationController?.present(listVC, animated: true, completion: {
                
            })
            
        })
        let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(buttonView)
        alertController.addAction(buttonCancel)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
        listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
        self.navigationController?.present(listVC, animated: true, completion: {
            
        })
    }
    // MARK: - Collection view data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return arrayOfImagesShare.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellcolletion1", for: indexPath)
        let imageView = cell.viewWithTag(2) as! UIImageView
        imageView.image = UIImage(named: self.arrayOfImagesShare[indexPath.row])
        let labelName = cell.viewWithTag(3) as! UILabel
        labelName.text = arrayOfImagesShareName[indexPath.row]
        imageView.contentMode = .scaleAspectFill
        
        
        return cell
        
    }
    /*  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
     if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) {
     return CGSize(width: 102, height: 95)
     }
     else {
     return CGSize(width: 0, height: 0)
     }
     
     
     }*/
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewOfShare.removeFromSuperview()
        
        if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) || self.arrayOfImagesShareName[indexPath.row] == "More" {
            
            if selected == 1 {
                
                if indexPath.row == 0 {
                    //Facebook
                    let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                    vc.delegate = self
                    vc.indexPath = indexPath
                    vc.type = "Image"
                    vc.modalPresentationStyle = .overCurrentContext
                  present(vc, animated:true)
                    
                }
                else if indexPath.row == 1 {
                    //Twitter
                    /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
                     API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
                     Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
                    // Swift
                    print(listOfFiles.currentList.title!)
                    if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                        // App must have at least one logged-in user to compose a Tweet
                        let composer = TWTRComposerViewController.init(initialText: listOfFiles.currentList.title ?? "", image: self.selectedImage.image!, videoURL: nil)
                        present(composer, animated: true, completion: nil)
                    } else {
                        // Log in, and then check again
                        
                        TWTRTwitter.sharedInstance().logIn { session, error in
                            if session != nil { // Log in succeeded
                                let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.selectedImage.image!, videoURL: nil)
                                self.present(composer, animated: true, completion: nil)
                            } else {
                                self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                
                            }
                        }
                    }
                    
                }
                else if indexPath.row == 2 {
                    //Instagram
                    let popup = UserDefaults.standard.bool(forKey: "popupVC")
                    if popup == true {
                        print("No popup")
                    }
                    else {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                    vc.delegate = self
                    vc.indexPath = indexPath
                    vc.type = "Image"
                    vc.modalPresentationStyle = .overCurrentContext
                  present(vc, animated:true)
                    }
                }
                
                else if indexPath.row == 3 {
                    //SnapChat
                    // Copied from Snapchat Documentation
                    
//                    let snapPhoto = SCSDKSnapPhoto(image: self.selectedImage.image!)
//                    let snap = SCSDKPhotoSnapContent(snapPhoto: snapPhoto)
//                    // snap.sticker = /* Optional, add a sticker to the Snap */
//                    // snap.caption = /* Optional, add a caption to the Snap */
//                    // snap.attachmentUrl = /* Optional, add a link to the Snap */
//                    SVProgressHUD.show()
//
//                    SCSDKSnapAPI(content: snap).startSnapping() { (error: Error?) in
//                        SVProgressHUD.dismiss()
//                        if let error = error {
//                            print(error.localizedDescription)
//                        } else {
//                            self.addshareDetails(app: "snapchat")
//                            // Successfully shared content to Snapchat!
//                        }
//                    }
                    let popup = UserDefaults.standard.bool(forKey: "popupVC")
                    if popup == true {
                        print("No popup")
                    }
                    else {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                    vc.delegate = self
                    vc.indexPath = indexPath
                    vc.type = "Image"
                    vc.modalPresentationStyle = .overCurrentContext
                  present(vc, animated:true)
                    }
                }
                else if indexPath.row == 4 {
                    // LinkedIn
                    let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                    
                    self.navigationController?.pushViewController(webVC, animated: true)
                    // self.sendToLinkedIn(image: UIImage(named: "img1")!)
                }
                else if indexPath.row == 5 {
                    let items = [ self.selectedImage.image!]
                    let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    present(ac, animated: true)
                    
                    ac.completionWithItemsHandler = { activity, completed, items, error in
                        if !completed {
                            // handle task not completed
                            return
                        }
                        else {
                            self.addshareDetails(app: "more")
                        }
                        
                    }
                    //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
                }
                else if indexPath.row == 6 {
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self;
                        mail.setCcRecipients(["yyyy@xxx.com"])
                        mail.setSubject("Your message")
                        mail.setMessageBody("Message body", isHTML: false)
                        let imageData: NSData = self.selectedImage.image!.pngData()! as NSData
                        mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                        self.present(mail, animated: true, completion: nil)
                    }
                }
                else {
                    let items = [self.selectedImage.image!]
                    let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    present(ac, animated: true)
                    
                    ac.completionWithItemsHandler = { activity, completed, items, error in
                        if !completed {
                            // handle task not completed
                            return
                        }
                        else {
                            self.addshareDetails(app: "more")
                        }
                        
                    }
                }
            }
            else {
                if indexPath.row == 0 {
                    
                    let content: ShareVideoContent = ShareVideoContent()
                    
                    DispatchQueue.main.async {
                        let video = ShareVideo()
                        print(self.destinationUrl)
                        video.videoURL =  URL(string:self.destinationUrl)
                        content.video = video
                        
                        let shareDialog = ShareDialog()
                        shareDialog.shareContent = content
                        shareDialog.mode = .native
                        shareDialog.delegate = self
                        shareDialog.show()
                    }
                    
                    
                    
                    
                }
                else if indexPath.row == 1 {
                    //Twitter
                    /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
                     API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
                     Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
                    // Swift
                    if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                        // App must have at least one logged-in user to compose a Tweet
                        let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.arrayOfimagesOrVideo[selectedIndex].path!))
                        present(composer, animated: true, completion: nil)
                    } else {
                        // Log in, and then check again
                        
                        TWTRTwitter.sharedInstance().logIn { session, error in
                            if session != nil { // Log in succeeded
                                let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.arrayOfimagesOrVideo[self.selectedIndex].path!))
                                self.present(composer, animated: true, completion: nil)
                            } else {
                                self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                
                            }
                        }
                    }
                    
                }
                else if indexPath.row == 2 {
                    //instagram
//                    DispatchQueue.main.async {
//                        let urlFeed = "instagram://library?LocalIdentifier=" + self.destinationUrl
//                        guard let url = URL(string: urlFeed) else {
//                            print("Could not open url")
//                            return
//                        }
//                        DispatchQueue.main.async {
//                            if UIApplication.shared.canOpenURL(url) {
//                                if #available(iOS 10.0, *) {
//                                    UIApplication.shared.open(url, options: [:]
//                                                              , completionHandler: { (success) in
//                                                                print("self?.delegate?.success")
//                                                              })
//                                } else {
//                                    UIApplication.shared.openURL(url)
//                                    print("self?.delegate?.success")
//                                }
//                            } else {
//                                print("Instagram not found")
//                            }
//                        }
//                    }
                    let popup = UserDefaults.standard.bool(forKey: "popupVC")
                    if popup == true {
                        print("No popup")
                    }
                    else {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                    vc.delegate = self
                    vc.indexPath = indexPath
                    vc.type = "Image"
                    vc.modalPresentationStyle = .overCurrentContext
                  present(vc, animated:true)
                    }
                }
                
                
                else if indexPath.row == 3 {
                    //SnapChat
                    // Copied from Snapchat Documentation
                    
//                    let video = SCSDKSnapVideo(videoUrl: URL(string:self.arrayOfimagesOrVideo[selectedIndex].path!)!)
//                    let videoContent = SCSDKVideoSnapContent(snapVideo: video)
//
//
//                    SVProgressHUD.show()
//
//                    SCSDKSnapAPI(content: videoContent).startSnapping() { (error: Error?) in
//                        SVProgressHUD.dismiss()
//                        if let error = error {
//                            print(error.localizedDescription)
//                        } else {
//                            self.addshareDetails(app: "snapchat")
//                            // Successfully shared content to Snapchat!
//                        }
//                    }
                    let popup = UserDefaults.standard.bool(forKey: "popupVC")
                    if popup == true {
                        print("No popup")
                    }
                    else {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                    vc.delegate = self
                    vc.indexPath = indexPath
                    vc.type = "Image"
                    vc.modalPresentationStyle = .overCurrentContext
                  present(vc, animated:true)
                    }
                    
                    
                }
                else if indexPath.row == 4 {
                    //LinkedI
//                    let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
//
//                    self.navigationController?.pushViewController(webVC, animated: true)
//                    // self.sendToLinkedIn(image: UIImage(named: "img1")!)
//                }
//                else if indexPath.row == 5 {
//                    let urlData = NSData(contentsOf: NSURL(string:self.arrayOfimagesOrVideo[selectedIndex].path!)! as URL)
//                    SVProgressHUD.show()
//                    if ((urlData) != nil){
//
//                        print(urlData)
//
//
//                        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
//                        let docDirectory = paths[0]
//                        let filePath = "\(docDirectory)/tmpVideo.mp4"
//                        urlData?.write(toFile: filePath, atomically: true)
//                        // file saved
//
//                        let videoLink = NSURL(fileURLWithPath: filePath)
//
//
//                        let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
//                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
//
//                        activityVC.setValue("Video", forKey: "subject")
//
//
//                        SVProgressHUD.dismiss()
//
//                        self.present(activityVC, animated: true, completion: nil)
//                        activityVC.completionWithItemsHandler = { activity, completed, items, error in
//                            if !completed {
//                                // handle task not completed
//                                return
//                            }
//                            else {
//                                self.addshareDetails(app: "more")
//                            }
//
//                        }
//                    }
                    let popup = UserDefaults.standard.bool(forKey: "popupVC")
                    if popup == true {
                        print("No popup")
                    }
                    else {
                    let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                    vc.delegate = self
                    vc.indexPath = indexPath
                    vc.type = "Image"
                    vc.modalPresentationStyle = .overCurrentContext
                  present(vc, animated:true)
                    }
                    //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
                }
                else if indexPath.row == 6 {
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self;
                        mail.setCcRecipients(["yyyy@xxx.com"])
                        mail.setSubject("Your message")
                        mail.setMessageBody("Message body", isHTML: false)
                        do {
                            let fileData: NSData = try Data(contentsOf: URL(string: self.arrayOfimagesOrVideo[selectedIndex].path!)!) as NSData
                            mail.addAttachmentData(fileData as Data, mimeType: "mp4", fileName: "myfile.mp4")
                            
                            self.present(mail, animated: true, completion: nil)
                            
                        }
                        catch {
                            print("error")
                        }
                    }
                }
                else {
                    let urlData = NSData(contentsOf: NSURL(string:self.arrayOfimagesOrVideo[selectedIndex].path!)! as URL)
                    
                    if ((urlData) != nil){
                        SVProgressHUD.show()
                        print(urlData)
                        
                        
                        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                        let docDirectory = paths[0]
                        let filePath = "\(docDirectory)/tmpVideo.mp4"
                        urlData?.write(toFile: filePath, atomically: true)
                        // file saved
                        
                        let videoLink = NSURL(fileURLWithPath: filePath)
                        
                        
                        let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        
                        activityVC.setValue("Video", forKey: "subject")
                        
                        SVProgressHUD.dismiss()
                        
                        
                        self.present(activityVC, animated: true, completion: nil)
                        activityVC.completionWithItemsHandler = { activity, completed, items, error in
                            if !completed {
                                // handle task not completed
                                return
                            }
                            else {
                                self.addshareDetails(app: "more")
                            }
                            
                        }
                    }
                    
                }
            }
        }
        else {
            self.showAlertWithOkButton(message: "Go to settings page to turn on this app" )
            
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.addshareDetails(app: "mail")
        controller.dismiss(animated: true, completion: nil)
    }
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        self.addshareDetails(app: "twitter")
        print("success")
    }
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        self.addshareDetails(app: "facebook")
        print("")
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("")
    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        print("")
    }
    func showSharePopup() {
        DispatchQueue.main.async {
            let window = UIApplication.shared.delegate?.window!
            
            
            self.viewOfShare.frame = CGRect(x: 0, y: (window?.frame.height)! - 397, width: (window?.frame.width)!, height: 397)
            window!.addSubview(self.viewOfShare)
            self.viewOfShare.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            
            UIView.animate(withDuration: 0.33, animations: {
                self.viewOfShare.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
    func dismissPopUpView(){
        UIView.animate(withDuration: 0.33, animations: {
            
        }, completion: { (completed) in
            
        })
        UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            
        }, completion: { (completed) in
            self.viewOfShare.removeFromSuperview()
            
        })
    }
    func addshareDetails(app:String){
        
        ServiceManager.sharedInstance.post(withServiceName: "addshare", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)","fileid":"\(arrayOfimagesOrVideo[selectedIndex].fileid!)","socialapp":app], withHud: true) { (success, result, error) in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    
                }
                else {
                    //  self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                }
            }
            else {
                // self.showAlertWithOkButton(message: "Please try again")
            }
        }
        
        
    }
    func downloadVideoLinkAndCreateAsset(path:String,completion: @escaping (String) -> Void) {
        
        // use guard to make sure you have a valid url
        
        SVProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: path),
               let urlData = NSData(contentsOf: url) {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/tempFile.mp4"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    var videoAssetPlaceholder:PHObjectPlaceholder!
                    PHPhotoLibrary.shared().performChanges({
                        let request =  PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                        videoAssetPlaceholder = request!.placeholderForCreatedAsset
                    }) { completed, error in
                        SVProgressHUD.dismiss()
                        if completed {
                            print("Video is saved!")
                            let localID = NSString(string: videoAssetPlaceholder.localIdentifier)
                            let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: NSString.CompareOptions.regularExpression, range: NSRange())
                            let ext = "mp4"
                            let assetURLStr =
                                "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                            
                            completion(assetURLStr)
                        }
                    }
                }
            }
        }
    }
    
    func continueSharingImage(indexPath: IndexPath) {
        
        
        if indexPath.row == 0 {
            //Facebook
         
          let sharePhoto = SharePhoto()
          sharePhoto.image = self.selectedImage.image!
          sharePhoto.isUserGenerated = true
          sharePhoto.caption = ""
          let photoContent = SharePhotoContent()
          photoContent.photos = [sharePhoto]
          let showDialog = ShareDialog.init(fromViewController: self, content: photoContent, delegate: self)
          if (showDialog.canShow) {
              showDialog.show()
          } else {
              self.showAlertWithOkButton(message: "It looks like you don't have the Facebook mobile app on your device.")
          }
        }
        else if indexPath.row == 1 {
          //Twitter
        /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
            API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
            Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
            // Swift
           if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                      // App must have at least one logged-in user to compose a Tweet
            let composer = TWTRComposerViewController.init(initialText: self.dictOfDetails.title, image: self.selectedImage.image!, videoURL: nil)
              self.present(composer, animated: true, completion: nil)
                  } else {
                      // Log in, and then check again
                  
                      TWTRTwitter.sharedInstance().logIn { session, error in
                          if session != nil { // Log in succeeded
                              let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.selectedImage.image!, videoURL: nil)
                              self.present(composer, animated: true, completion: nil)
                          } else {
                              self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                             
                          }
                      }
                  }
            
        }
        else if indexPath.row == 2 {
            //Instagram
            
          PHPhotoLibrary.shared().performChanges({
            DispatchQueue.main.async {
                PHAssetChangeRequest.creationRequestForAsset(from: self.selectedImage.image!)
            }
          }, completionHandler: { [weak self] success, error in
              if success {
                  let fetchOptions = PHFetchOptions()
                  fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                  let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                  if let lastAsset = fetchResult.firstObject {
                      let localIdentifier = lastAsset.localIdentifier
                      
                      let urlFeed = "instagram://library?LocalIdentifier=" + localIdentifier
                      guard let url = URL(string: urlFeed) else {
                          print("Could not open url")
                          return
                      }
                      DispatchQueue.main.async {
                          if UIApplication.shared.canOpenURL(url) {
                              if #available(iOS 10.0, *) {
                                  UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                                      print("self?.delegate?.success")
                                  })
                              } else {
                                  UIApplication.shared.openURL(url)
                                  print("self?.delegate?.success")
                                  
                              }
                          } else {
                              DispatchQueue.main.async {
                              self!.showAlertWithOkButton(message: "It looks like you don't have the Instagram mobile app on your device.")
                              } }
                      }
                  }
              } else if let error = error {
                  DispatchQueue.main.async {
                      print(error.localizedDescription)
                      self!.showAlertWithOkButton(message: "Photos access not allowed. Kindly change the settings of your Instagram App.")
                  }
              }
              else {
                  print("Could not save the photo")
              }
          })
        }
      
        else if indexPath.row == 3 {
            //SnapChat
            // Copied from Snapchat Documentation

          let snapPhoto = SCSDKSnapPhoto(image: self.selectedImage.image!)
                  let snap = SCSDKPhotoSnapContent(snapPhoto: snapPhoto)
                  // snap.sticker = /* Optional, add a sticker to the Snap */
          snap.caption = self.dictOfDetails.title ?? ""
                  // snap.attachmentUrl = /* Optional, add a link to the Snap */
                    SVProgressHUD.show()
          
//            SCSDKSnapAPI().startSending(snap) { (error: Error?) in
//                ARSLineProgress.hide()
//               if let error = error {
//                   print(error.localizedDescription)
//               } else {
//                    self.addshareDetails(app: "snapchat")
//                   // Successfully shared content to Snapchat!
//               }
//           }
//
//            let api = SCSDKSnapAPI(content: snap)
//            api.startSnapping { error in
//
//                if let error = error {
//                    print(error.localizedDescription)
//                } else {
//                    // success
//
//                }
//            }
          
          
                  SCSDKSnapAPI(content: snap).startSnapping() { (error: Error?) in
                       SVProgressHUD.dismiss()
                      if let error = error {
                          print(error.localizedDescription)
                      } else {
                           self.addshareDetails(app: "snapchat")
                          // Successfully shared content to Snapchat!
                      }
                  }
                 
                 
         
        
        }
        else if indexPath.row == 4 {
           // LinkedIn
        let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                                                       
          self.navigationController?.pushViewController(webVC, animated: true)
           // self.sendToLinkedIn(image: UIImage(named: "img1")!)
        }
        else if indexPath.row == 5 {
            let items = [ self.selectedImage.image!]
              let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
          self.present(ac, animated: true)
          
          ac.completionWithItemsHandler = { activity, completed, items, error in
                                       if !completed {
                                           // handle task not completed
                                           return
                                       }
                                       else {
                                            self.addshareDetails(app: "more")
                                       }
                                     
                                   }
            //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
        }
        else if indexPath.row == 6 {
            if MFMailComposeViewController.canSendMail() {
                   let mail = MFMailComposeViewController()
                   mail.mailComposeDelegate = self;
                   mail.setCcRecipients([""])
              mail.setSubject(self.dictOfDetails.title)
              mail.setMessageBody(self.dictOfDetails.descriptionFile ?? "", isHTML: false)
              let imageData: NSData = self.selectedImage.image!.pngData()! as NSData
                   mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                   self.present(mail, animated: true, completion: nil)
                 }
        }
        else {
            let items = [self.selectedImage.image!]
                  let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
          self.present(ac, animated: true)
          
          ac.completionWithItemsHandler = { activity, completed, items, error in
                                       if !completed {
                                           // handle task not completed
                                           return
                                       }
                                       else {
                                            self.addshareDetails(app: "more")
                                       }
                                     
                                   }
        }
                  
              }
    
    func continueSharingVideo(indexPath: IndexPath) {
        if indexPath.row == 0 {
                      
                  let content: ShareVideoContent = ShareVideoContent()
       
                DispatchQueue.main.async {
                                         let video = ShareVideo()
                    print(self.destinationUrl)
                    video.videoURL =  URL(string:self.destinationUrl)
                                         content.video = video
                                         
                                         let shareDialog = ShareDialog()
                                         shareDialog.shareContent = content
                                         shareDialog.mode = .native
                                         shareDialog.delegate = self
                                         shareDialog.show()
                                     }
          
                     
                      
                
        }
        else if indexPath.row == 1 {
            //Twitter
        /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
            API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
            Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
            // Swift
           if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                      // App must have at least one logged-in user to compose a Tweet
                      let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.dictOfDetails.path!))
            self.present(composer, animated: true, completion: nil)
                  } else {
                      // Log in, and then check again
                  
                      TWTRTwitter.sharedInstance().logIn { session, error in
                          if session != nil { // Log in succeeded
                            let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.dictOfDetails.path!))
                              self.present(composer, animated: true, completion: nil)
                          } else {
                              self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                             
                          }
                      }
                  }
            
        }
        else if indexPath.row == 2 {
            //instagram
                   DispatchQueue.main.async {
                    let urlFeed = "instagram://library?LocalIdentifier=" + self.destinationUrl
                             guard let url = URL(string: urlFeed) else {
                                 print("Could not open url")
                                 return
                             }
                             DispatchQueue.main.async {
                                 if UIApplication.shared.canOpenURL(url) {
                                     if #available(iOS 10.0, *) {
                                         UIApplication.shared.open(url, options: [:]
             , completionHandler: { (success) in
                                             print("self?.delegate?.success")
                                         })
                                     } else {
                                         UIApplication.shared.openURL(url)
                                         print("self?.delegate?.success")
                                     }
                                 } else {
                                     print("Instagram not found")
                                 }
                             }
                         }
        }
            
 
        else if indexPath.row == 3 {
            //SnapChat
            // Copied from Snapchat Documentation
             
            let video = SCSDKSnapVideo(videoUrl: URL(string:self.dictOfDetails.path!)!)
            let videoContent = SCSDKVideoSnapContent(snapVideo: video)
               
          
                  SVProgressHUD.show()
                 
                  SCSDKSnapAPI(content: videoContent).startSnapping() { (error: Error?) in
                      SVProgressHUD.dismiss()
                      if let error = error {
                          print(error.localizedDescription)
                      } else {
                         self.addshareDetails(app: "snapchat")
                          // Successfully shared content to Snapchat!
                      }
                  }
                 
           
        
        }
        else if indexPath.row == 4 {
            //LinkedI
          let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                                                      
                    self.navigationController?.pushViewController(webVC, animated: true)
           // self.sendToLinkedIn(image: UIImage(named: "img1")!)
        }
        else if indexPath.row == 5 {
       let urlData = NSData(contentsOf: NSURL(string:self.dictOfDetails.path!)! as URL)
  SVProgressHUD.show()
                  if ((urlData) != nil){

                      print(urlData)


                     let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                      let docDirectory = paths[0]
                      let filePath = "\(docDirectory)/tmpVideo.mp4"
                     urlData?.write(toFile: filePath, atomically: true)
                      // file saved

                      let videoLink = NSURL(fileURLWithPath: filePath)


                      let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
                      let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                      activityVC.setValue("Video", forKey: "subject")
               

                      SVProgressHUD.dismiss()
                    self.present(activityVC, animated: true, completion: nil)
                    activityVC.completionWithItemsHandler = { activity, completed, items, error in
                        if !completed {
                            // handle task not completed
                            return
                        }
                        else {
                             self.addshareDetails(app: "more")
                        }
                      
                    }
                  
                  }
            //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
        }
        else if indexPath.row == 6 {
           if MFMailComposeViewController.canSendMail() {
                   let mail = MFMailComposeViewController()
                   mail.mailComposeDelegate = self;
                   mail.setCcRecipients([""])
            mail.setSubject(self.dictOfDetails.title)
            mail.setMessageBody(self.dictOfDetails.descriptionFile ?? "", isHTML: false)
               do {
                let fileData: NSData = try Data(contentsOf: URL(string: self.dictOfDetails.path!)!) as NSData
                mail.addAttachmentData(fileData as Data, mimeType: "mp4", fileName: "myfile.mp4")

                   self.present(mail, animated: true, completion: nil)
                 
            }
            catch {
                print("error")
            }
            }
        }
        else {
            let urlData = NSData(contentsOf: NSURL(string:self.dictOfDetails.path!)! as URL)

         if ((urlData) != nil){

             print(urlData)


            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
             let docDirectory = paths[0]
             let filePath = "\(docDirectory)/tmpVideo.mp4"
            urlData?.write(toFile: filePath, atomically: true)
             // file saved

             let videoLink = NSURL(fileURLWithPath: filePath)


             let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
             let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

             activityVC.setValue("Video", forKey: "subject")

           

            self.present(activityVC, animated: true, completion: nil)
            activityVC.completionWithItemsHandler = { activity, completed, items, error in
                          if !completed {
                              // handle task not completed
                              return
                          }
                          else {
                               self.addshareDetails(app: "more")
                          }
            }
        
         }
        }
            
         //
         }
    
    
    @objc func tapBlurButton(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
            case .down:
                print("Swiped down")
                self.dismissPopUpView()
            case .left:
                print("Swiped left")
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    @objc func refresh(sender:AnyObject) {
        // Code to refresh table view
        index = 0
        if self.selected == 1{
            self.arrayOfimagesOrVideo.removeAll()
            index = 0
            self.tableView.reloadData()
            self.getLastestList(type: "Image")
        }
        else{
            index = 0
            self.arrayOfimagesOrVideo.removeAll()
            self.tableView.reloadData()
            self.getLastestList(type: "Video")
        }
    }
    override func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        isLoadMoreData = true
        if isAvailableData == true
        {
            index += 5
            if self.selected == 0 {
                self.getLastestList(type: "Image")
            }
            else{
                self.getLastestList(type: "Video")
            }
        }
        
    }
    func getLastestList(type:String) {
        //type:Video,Image
        ServiceManager.sharedInstance.post(withServiceName: "filedisplay", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)","file_type":type,"offset":index,"limit":10], withHud: true) { (success, result, error) in
            print(MEUserDetails.currentUser.id!, type, self.index)
            self.refreshContrl.endRefreshing()
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    self.arrayOfimagesOrVideo.removeAll()
                    let listDetailsDict = result as! NSDictionary
                    
                    for items in listDetailsDict.value(forKey: "user_details") as! NSArray {
                        let obj  = listOfFiles().initWithDictionary(userDictionary: items as! NSDictionary)
                        self.arrayOfimagesOrVideo.append(obj)
                    }
                    
                    self.tableView.reloadData()
                    
                }
                else {
                    self.tableView.reloadData()
                }
            }
            else {
                self.showAlertWithOkButton(message: "Please try again")
            }
        }}
    
    func getVideoList() {
        ServiceManager.sharedInstance.post(withServiceName: "filelisting", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: false) { (success, result, error) in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    SVProgressHUD.show()
                //    ars_dispatchAfter(0.01) {
                        let listDetailsDict = result as!  NSDictionary
                        let videoDetails = listDetailsDict.value(forKey: "video") as! NSArray
                        self.videoUrlArray.removeAll()
                    for video in videoDetails{
                        let path = (video as! NSDictionary).value(forKey: "path") as! String
                        self.videoUrlArray.append(path)
                        if let pathURL = URL(string: path) {
                            let image = self.thumbnailForVideoAtURL(url: pathURL as NSURL)
                            self.videoThumbDict[path] = image
                        }
                    }
                        
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                            SVProgressHUD.dismiss()
                        }
               //     }
                }
                else {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
            else {
                self.showAlertWithOkButton(message: "Please try again")
            }
        }
        
    }
    
    //MARK: - To generate thumbnail from URL
    func thumbnailForVideoAtURL(url: NSURL) -> UIImage? {
        let asset = AVAsset(url: url as URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform = true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            print("Thumbail generated")
            return UIImage(cgImage: imageRef)
        } catch {
            print("error in generating thumbnail: \(error)")
            return nil
        }
    }
    
    //MARK:- Show Alert With Ok Button
    func showAlertWithOkButton(message:String){
        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
            
        }
    }
    /*
     override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
     let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)
     
     // Configure the cell...
     
     return cell
     }
     */
    
    /*
     // Override to support conditional editing of the table view.
     override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the specified item to be editable.
     return true
     }
     */
    
    /*
     // Override to support editing the table view.
     override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
     if editingStyle == .delete {
     // Delete the row from the data source
     tableView.deleteRows(at: [indexPath], with: .fade)
     } else if editingStyle == .insert {
     // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
     }
     }
     */
    
    /*
     // Override to support rearranging the table view.
     override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {
     
     }
     */
    
    /*
     // Override to support conditional rearranging of the table view.
     override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
     // Return false if you do not want the item to be re-orderable.
     return true
     }
     */
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
