//
//  MEImageDetailViewController.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 09/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import AVKit
import FBSDKShareKit
import TwitterKit
import SCSDKCreativeKit
import SCSDKStoryKit
import MessageUI
import Social
import TikTokOpenSDK
import SVProgressHUD

class MEImageDetailViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout,UIImagePickerControllerDelegate,UINavigationControllerDelegate,UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate,TWTRComposerViewControllerDelegate,SharingDelegate {
   
    @IBOutlet var btnFlash                         : UIButton!
    @IBOutlet var btnCamera                        : UIButton!
    @IBOutlet var btnShare                         : UIButton!
    @IBOutlet var btnDowload                       : UIButton!
    @IBOutlet var labelName                        : UILabel!
    @IBOutlet var labelTag                         : UILabel!
    @IBOutlet var imageview                        : UIImageView!
    @IBOutlet var collectionViewShare              : UICollectionView!
    @IBOutlet var viewOfShare                      : UIView!
    var imagePicker                = UIImagePickerController()

   var dictOfDetails = listOfFiles()
   var arrayOfImagesShare = ["fb_round","twitter_round","insta_round","snapchat_round","linkedin_round","whatsapp_round","email_round","more"]
      var arrayOfImagesShareName = ["Facebook","Twitter","Instagram","Snapchat","Linkedin","Whatsapp","Email","More"]
    @IBOutlet var viewShare                        : UIView!
          var viewBackground                             = UIView()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
         self.customUI()
        // Do any additional setup after loading the view.
    
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
          
           self.dismissPopUpView()
       }
    //MARK:- Set Up NavigationBar
       func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = kAppThemeYellowTop
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
           self.navigationController?.navigationBar.isHidden = false
           self.navigationItem.setHidesBackButton(true, animated: true)
           self.navigationItem.title = "Videos"
           self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
         
           self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
         let sideButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backAction))
           self.navigationItem.leftBarButtonItem = sideButtonButton
           
       }
          //MARK:- CustomUI
             func customUI() {
                let width = (kScreenWidth - 60) / 3
                let columnLayout = ColumnFlowLayout.init(cellsPerRow: 3, minimumInteritemSpacing: 5.0, minimumLineSpacing: 10.0, sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), cellHeight: 100, cellWidth: width,scrollDirec: .vertical)
                
                  collectionViewShare?.collectionViewLayout = columnLayout
                  collectionViewShare.reloadData()
                
               let swipeDown = UISwipeGestureRecognizer(target: self, action:#selector(tapBlurButton))
                swipeDown.direction = .down
                         self.view.addGestureRecognizer(swipeDown)
               self.btnShare.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 0, cornerRadius: 30)
               self.btnDowload.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 0, cornerRadius: 30)
                self.imageview.downloaded(from: dictOfDetails.path!)
                
                self.labelTag.text = MEUserDetails.currentUser.name!
                self.labelName.text = "@\(MEUserDetails.currentUser.name!)"
                
    }
              @objc func backAction() {
                self.navigationController?.popViewController(animated: true)
               
              }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
         @objc func actionPushNotificationInActiveState(_ notification: Notification) {
    
          let message = "Its time to post your post."
               
                 let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
                 let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
                    let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                      listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                     self.navigationController?.present(listVC, animated: true, completion: {
                        
                     })
                  
                 })
                 let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                     self.dismiss(animated: true, completion: nil)
                 })
                 alertController.addAction(buttonView)
                 alertController.addAction(buttonCancel)
                 self.present(alertController, animated: true, completion: nil)
                 
             
         }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
              let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                              listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                             self.navigationController?.present(listVC, animated: true, completion: {
                                
                             })
      }
     //MARK:- Button Actions
              @IBAction func buttonShareAction(_ sender: UIButton) {
                self.showSharePopup()
             }
              @IBAction func buttonDownloadAction(_ sender: UIButton) {
               /* let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
               let actionGallery = UIAlertAction(title: "Download image to  gallery", style: .default) { (action) in
                UIImageWriteToSavedPhotosAlbum(self.imageview.image!, nil, nil, nil)

                             }
                             alertController.addAction(actionGallery)
                             self.present(alertController, animated: true) { }
                             let actionCancel = UIAlertAction(title:"cancel", style: .destructive) { (action) in
                             }
                             alertController.addAction(actionCancel)*/
                
                let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEDeatilsPageTVC") as! MEDeatilsPageTVC
                                     listVC.dictOfDetails = self.dictOfDetails
                               listVC.type = "image"
                                    self.navigationController?.pushViewController(listVC, animated: true)
              }
              @IBAction func buttonFlashAction(_ sender: UIButton) {
                            
              }
              @IBAction func buttonCameraAction(_ sender: UIButton) {
                  self.imagePicker = UIImagePickerController()
                  self.imagePicker.delegate = self
                  self.imagePicker.allowsEditing = true
                
                
                      let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
                        let actionTakePhoto = UIAlertAction(title: "Take a photo", style: .default) { (action) in
                            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
                            {
                                self.imagePicker.sourceType = .camera
                                self.present(self.imagePicker,animated : true,completion : nil)
                            }
                            else
                            {
                                let alert = UIAlertController(title: "oops", message:"Camera not available", preferredStyle: .alert)
                                let actionOk = UIAlertAction(title:"OK", style: .default) { (action) in
                                }
                                alert.addAction(actionOk)
                                self.present(alert, animated: true){
                                    
                                }
                            }
                        }
                        alertController.addAction(actionTakePhoto)
                        self.present(alertController, animated: true) { }
                        let actionCancel = UIAlertAction(title:"cancel", style: .destructive) { (action) in
                        }
                        alertController.addAction(actionCancel)
                 }
             @IBAction func buttonCloseAction(_ sender: UIButton) {
                self.dismissPopUpView()
             }
             @IBAction func buttonCloseShareFileAction(_ sender: UIButton) {
                 self.dismissPopUpView()
                 }
    // MARK: - Collection view data source
         func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
             
               return arrayOfImagesShare.count
             
         }
         
         func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
             
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellcolletion1", for: indexPath)
                 let imageView = cell.viewWithTag(2) as! UIImageView
                 imageView.image = UIImage(named: self.arrayOfImagesShare[indexPath.row])
                 let labelName = cell.viewWithTag(3) as! UILabel
                 labelName.text = arrayOfImagesShareName[indexPath.row]
             
                 return cell
             
         }
        /* func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
             
           if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) {
                                 return CGSize(width: 102, height: 95)
                           }
                           else {
                                return CGSize(width: 0, height: 0)
                           }
             
         }*/
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewOfShare.removeFromSuperview()

        if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) || self.arrayOfImagesShareName[indexPath.row] == "More" {
                                 
          if indexPath.row == 0 {
              //Facebook
           
            
            let sharePhoto = SharePhoto()
            sharePhoto.image = self.imageview.image
            sharePhoto.isUserGenerated = true
            sharePhoto.caption = ""
            let photoContent = SharePhotoContent()
            photoContent.photos = [sharePhoto]
            let showDialog = ShareDialog.init(fromViewController: self, content: photoContent, delegate: self)
            if (showDialog.canShow) {
                showDialog.show()
            } else {
                self.showAlertWithOkButton(message: "It looks like you don't have the Facebook mobile app on your device.")
            }
          }
          else if indexPath.row == 1 {
            //Twitter
          /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
              API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
              Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
              // Swift
             if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                        // App must have at least one logged-in user to compose a Tweet
                        let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.imageview.image, videoURL: nil)
                        present(composer, animated: true, completion: nil)
                    } else {
                        // Log in, and then check again
                    
                        TWTRTwitter.sharedInstance().logIn { session, error in
                            if session != nil { // Log in succeeded
                                let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.imageview.image, videoURL: nil)
                                self.present(composer, animated: true, completion: nil)
                            } else {
                                self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                               
                            }
                        }
                    }
              
          }
          else if indexPath.row == 2 {
              //Instagram
            PHPhotoLibrary.shared().performChanges({
                PHAssetChangeRequest.creationRequestForAsset(from: self.imageview.image!)
            }, completionHandler: { [weak self] success, error in
                if success {
                    let fetchOptions = PHFetchOptions()
                    fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                    let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                    if let lastAsset = fetchResult.firstObject {
                        let localIdentifier = lastAsset.localIdentifier
                        
                        let urlFeed = "instagram://library?LocalIdentifier=" + localIdentifier
                        guard let url = URL(string: urlFeed) else {
                            print("Could not open url")
                            return
                        }
                        DispatchQueue.main.async {
                            if UIApplication.shared.canOpenURL(url) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                                        print("self?.delegate?.success")
                                    })
                                } else {
                                    UIApplication.shared.openURL(url)
                                    print("self?.delegate?.success")
                                    
                                }
                            } else {
                                DispatchQueue.main.async {
                                    self!.showAlertWithOkButton(message: "It looks like you don't have the Instagram mobile app on your device.")
                                }
                            }
                        }
                    }
                } else if let error = error {
                    DispatchQueue.main.async {
                        print(error.localizedDescription)
                        self!.showAlertWithOkButton(message: "Photos access not allowed. Kindly change the settings of your Instagram App.")
                    }
                }
                else {
                    print("Could not save the photo")
                }
            })
          }
         /* else if indexPath.row == 3 {
            //TikTok
               guard let imageData = self.imageview.image!.jpegData(compressionQuality: 100) else {
                                   return
                               }

                               // build the file URL
                               let path = (NSTemporaryDirectory() as NSString).appendingPathComponent("titok.ig")
                               let fileUrl = URL(fileURLWithPath: path)

                               // write the image data to the file URL
                               do {
                                   try imageData.write(to: fileUrl, options: .atomic)
                               } catch {
                                   // could not write image data
                                   return
                               }
                    /*Note:

                    The aspect ratio of the images or videos should between: [1/2.2, 2.2]
                    If mediaType is Image:
                    The number of images should be more than one and up to 12.
                    If mediaType is Video:
                    Total video duration should be longer than 1 seconds.
                    No more than 12 videos can be shared
                    Videos with brand logo or watermark will lead to the videos being deleted or the respective accounts disabled. Make sure your application shares content without a watermark.*/
                    
                    let req = TikTokOpenSDKShareRequest()
                    req.mediaType = TikTokOpenSDKShareMediaType.image
                    //Or
                   // req.mediaType = TikTokOpenSDKShareMediaTypeVideo
            CustomPhotoAlbum.sharedInstance.saveImage(image: self.imageview.image!)
                    var selectedAssets = [PHAsset]()
                    selectedAssets.append(CustomPhotoAlbum.sharedInstance.fetch()!)
                  //  selectedAssets.append(CustomPhotoAlbum.sharedInstance.fetch()!)
                    var mediaLocalIdentifiers: [String]? = []
                    for asset in selectedAssets {
                        mediaLocalIdentifiers?.append(asset.localIdentifier)
                    }
                    req.localIdentifiers = mediaLocalIdentifiers!
          }*/
          else if indexPath.row == 3 {
              //SnapChat
              // Copied from Snapchat Documentation

            let snapPhoto = SCSDKSnapPhoto(image: self.imageview.image!)
                    let snap = SCSDKPhotoSnapContent(snapPhoto: snapPhoto)
                    // snap.sticker = /* Optional, add a sticker to the Snap */
                    // snap.caption = /* Optional, add a caption to the Snap */
                    // snap.attachmentUrl = /* Optional, add a link to the Snap */
                      SVProgressHUD.show()
                   
                    SCSDKSnapAPI(content: snap).startSnapping() { (error: Error?) in
                        SVProgressHUD.dismiss()
                        if let error = error {
                            print(error.localizedDescription)
                        } else {
                            // Successfully shared content to Snapchat!
                        }
                    }
                   
                   
           
          
          }
          else if indexPath.row == 4 {
             // LinkedIn
          let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                                                         
            self.navigationController?.pushViewController(webVC, animated: true)
             // self.sendToLinkedIn(image: UIImage(named: "img1")!)
          }
          else if indexPath.row == 5 {
              let items = [ self.imageview.image!]
                let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                               present(ac, animated: true)
              //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
          }
          else if indexPath.row == 6 {
              if MFMailComposeViewController.canSendMail() {
                     let mail = MFMailComposeViewController()
                     mail.mailComposeDelegate = self;
                     mail.setCcRecipients(["yyyy@xxx.com"])
                     mail.setSubject("Your message")
                     mail.setMessageBody("Message body", isHTML: false)
                let imageData: NSData = self.imageview.image!.pngData()! as NSData
                     mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                     self.present(mail, animated: true, completion: nil)
                   }
          }
          else {
              let items = [self.imageview.image]
                    let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    present(ac, animated: true)
          }
            }
                else {
            self.showAlertWithOkButton(message: "Go to settings page to turn on this app" )
                      
                  }
                 
      }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
           controller.dismiss(animated: true, completion: nil)
       }
    //MARK:- IMAGEPICKER DELEGATE METHOD
            
            func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
              let image = info[.editedImage] != nil ? (info[.editedImage] as! UIImage) : nil
              
               dismiss(animated: true, completion: nil)
            
                   
            }
          
         //MARK:- Image Picker Cancel
            func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
            {
                dismiss(animated: true, completion: nil)
            }
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
           print("")
       }
       
       func sharer(_ sharer: Sharing, didFailWithError error: Error) {
            print("")
       }
       
       func sharerDidCancel(_ sharer: Sharing) {
            print("")
       }
       
    func showSharePopup() {
        let window = UIApplication.shared.delegate?.window!
       
      
        self.viewOfShare.frame = CGRect(x: 0, y: (window?.frame.height)! - 397, width: (window?.frame.width)!, height: 397)
         window!.addSubview(self.viewOfShare)
        self.viewOfShare.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
        
        UIView.animate(withDuration: 0.33, animations: {
            self.viewOfShare.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        })
    }
    // MARK: Show Share Popup
       func showFbSharePopup() {
              let window = UIApplication.shared.delegate?.window!
             viewBackground = UIView(frame: CGRect(x: 0, y: 0, width: window!.frame.size.width, height:  window!.frame.size.height))
                      viewBackground.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
                      window!.addSubview(viewBackground)
            
              self.viewShare.frame = CGRect(x: 15, y: ((window?.frame.height)! - 94) / 2, width: (window?.frame.width)! - 30, height: 94)
               window!.addSubview(self.viewBackground)
            self.viewBackground.addSubview(self.viewShare)
               self.viewBackground.bringSubviewToFront(self.viewShare)
              self.viewBackground.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
              
              UIView.animate(withDuration: 0.33, animations: {
                  self.viewBackground.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
              })
          }
    func dismissPopUpView(){
              UIView.animate(withDuration: 0.33, animations: {
                
              }, completion: { (completed) in
                  
              })
              UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
                  
              }, completion: { (completed) in
                  self.viewOfShare.removeFromSuperview()
                
              })
          }
    
    @objc func tapBlurButton(gesture: UIGestureRecognizer) {

    if let swipeGesture = gesture as? UISwipeGestureRecognizer {

        switch swipeGesture.direction {
        case .right:
            print("Swiped right")
        case .down:
            print("Swiped down")
              self.dismissPopUpView()
        case .left:
            print("Swiped left")
        case .up:
            print("Swiped up")
        default:
            break
        }
    }
    }
    //MARK:- Show Alert With Ok Button
     func showAlertWithOkButton(message:String){
                   AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                       
                   }
     }
         

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
