//
//  MELastestVideosVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 08/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import SideMenu
import UIKit
import AVFoundation
import MediaPlayer
import AVKit
import FBSDKShareKit
import TwitterKit
import SCSDKCreativeKit
import SCSDKStoryKit
import MessageUI
import Social
import TikTokOpenSDK
import SVProgressHUD

var arrayOfSavedApp = [String]()
class MELastestVideosVC: UIViewController,UITableViewDelegate,UITableViewDataSource,UICollectionViewDelegate,UICollectionViewDataSource,UIGestureRecognizerDelegate, UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate,TWTRComposerViewControllerDelegate,SharingDelegate {
    
    
 
    
    @IBOutlet weak var imageThumbnail: UIImageView!
    @IBOutlet weak var videoThumbnail: UIImageView!
    @IBOutlet var labelNoOfImages                  : UILabel!
    @IBOutlet var labelNoOfVideos                  : UILabel!
    @IBOutlet var imageViewImages                  : UIImageView!
    @IBOutlet var imageViewVideos                  : UIImageView!
    @IBOutlet var btnViewAll                       : UIButton!
    @IBOutlet var tableView                        : UITableView!
    @IBOutlet var collectionViewShare              : UICollectionView!
    @IBOutlet var viewOfShare                      : UIView!
    
    var arrayOfImagesShare = ["fb_round","twitter_round","insta_round","snapchat_round","linkedin_round","whatsapp_round","email_round","more"]
    var arrayOfImagesShareName = ["Facebook","Twitter","Instagram","Snapchat","Linkedin","Whatsapp","Email","More"]
    var arrayOfimages = [listOfFiles]()
    var arrayOfVedio  = [listOfFiles]()
    var videoUrlArray = [String]()
    var selectedIndex = 0
    var typeOfFile = ""
    var selectedImage = UIImageView()
    var destinationUrl = ""
    var videoThumbDict = [String: UIImage]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.customUI()
        imageThumbnail.image = kplaceholderImage
        videoThumbnail.image = kplaceholderImage
      
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.getLastestList()
        NotificationCenter.default.addObserver(self, selector: #selector(self.showExpiredAlert), name: Notification.Name(rawValue: "showExpiredAlert"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "showExpiredAlert"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        self.dismissPopUpView()
    }
    
    @objc func showExpiredAlert(){
        AJAlertController.initialization().showAlertWithOkButton_Title(aStrTitle: "Expired", aStrMessage: "Thank you for trying Memorrowd. Your current pack has expired. Please select a new subscription plan to continue.") { (_, _) in
          
            
                let viewController = kStoryboard.instantiateViewController(withIdentifier: "MEUpgradePlanTVC") as! MEUpgradePlanTableViewController
            self.navigationController?.pushViewController(viewController, animated: true)
            
          
        }
    }
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = kAppThemeYellowTop
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        let logo = UIImage(named: "appLogo")
        let imageView = UIImageView(image:logo)
        self.navigationItem.titleView = imageView
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
        
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
        let sideButtonButton = UIBarButtonItem(image: UIImage(named: "side")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(sideMenuAction))
        self.navigationItem.leftBarButtonItem = sideButtonButton
        //         let rightMenuButton = UIBarButtonItem(image: UIImage(named: "dot")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(rightMenuAction))
        //         self.navigationItem.rightBarButtonItem = rightMenuButton
    }
    //MARK:- CustomUI
    func customUI() {
        let width = (kScreenWidth - 60) / 4
        let columnLayout = ColumnFlowLayout.init(cellsPerRow: 3, minimumInteritemSpacing: 5.0, minimumLineSpacing: 10.0, sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), cellHeight: 100, cellWidth: width,scrollDirec: .vertical)
        
        collectionViewShare?.collectionViewLayout = columnLayout
        collectionViewShare.reloadData()
        
        let swipeDown = UISwipeGestureRecognizer(target: self, action:#selector(tapBlurButton))
        swipeDown.direction = .down
        self.view.addGestureRecognizer(swipeDown)
        self.imageViewImages.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
        self.imageViewVideos.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
        self.btnViewAll.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 1, cornerRadius: 14.0)
        NotificationCenter.default.addObserver(self,selector: #selector(self.settingSelected),name: Notification.Name("Settings"),object: nil)
    }
    @objc func sideMenuAction() {
        // Define the menu
        
        // SideMenuNavigationController is a subclass of UINavigationController, so do any additional configuration
        // of it here like setting its viewControllers. If you're using storyboards, you'll want to do something like:
        let darkView = UIView(frame: CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height))
        darkView.backgroundColor = .black
        //self.navigationController?.tabBarController.addSubview(darkView)
        let menu = storyboard!.instantiateViewController(withIdentifier: "RightMenu") as! SideMenuNavigationController
        
        menu.presentationStyle = .viewSlideOutMenuPartialIn
        menu.blurEffectStyle = .dark
        present(menu, animated: true, completion: nil)
        
    }
    @objc func rightMenuAction() {
        
    }
    
    //MARK: - To generate thumbnail from URL
    func thumbnailForVideoAtURL(url: NSURL) -> UIImage? {
        let asset = AVAsset(url: url as URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform = true
        
        var time = asset.duration
        time.value = min(time.value, 2)
        
        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            print("Thumbnail generated")
            return UIImage(cgImage: imageRef)
        } catch {
            print("error in generating thumbnail: \(error)")
            return nil
        }
    }
    
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
    @objc func actionPushNotificationInActiveState(_ notification: Notification) {
        
        let message = "Its time to post your post."
        
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
        let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
            self.navigationController?.present(listVC, animated: true, completion: {
                
            })
            
        })
        let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(buttonView)
        alertController.addAction(buttonCancel)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
        listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
        self.navigationController?.present(listVC, animated: true, completion: {
            
        })
    }
    @objc private func settingSelected(notification: NSNotification){
        
        let SettingsTVC = kStoryboard.instantiateViewController(withIdentifier:"SettingsTVC") as! SettingsTVC
        
        self.navigationController?.pushViewController(SettingsTVC, animated: true)
        
        //do stuff using the userInfo property of the notification object
    }
    //MARK:- Button Actions
    @IBAction func buttonImagesAction(_ sender: UIButton) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MEListImagesVideosTVC") as! MEListImagesVideosTVC
        listVC.from = "image"
        listVC.noOfVideo = self.labelNoOfVideos.text!
        listVC.noOfimages = self.labelNoOfImages.text!
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    
    @IBAction func buttonVideosAction(_ sender: UIButton) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MEListImagesVideosTVC") as! MEListImagesVideosTVC
        listVC.from = "video"
        listVC.noOfVideo = self.labelNoOfVideos.text!
        listVC.noOfimages = self.labelNoOfImages.text!
        listVC.videoThumbDict = self.videoThumbDict
        listVC.videoUrlArray = self.videoUrlArray
        self.navigationController?.pushViewController(listVC, animated: true)
        
    }
    @IBAction func buttonViewAllAction(_ sender: UIButton) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MEListImagesVideosTVC") as! MEListImagesVideosTVC
        listVC.from = "image"
        listVC.noOfVideo = self.labelNoOfVideos.text!
        listVC.noOfimages = self.labelNoOfImages.text!
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    
    @IBAction func buttonShareAction(_ sender: UIButton) {
        guard let cell = sender.superview?.superview as? lastestVideoTableViewCell else {
            return // or fatalError() or whatever
        }
        
        let indexPath = tableView.indexPath(for: cell)
        typeOfFile = indexPath?.section == 0 ? "video" : "image"
        selectedIndex = sender.tag
        if typeOfFile ==  "image" {
            guard let cell = sender.superview?.superview as? lastestVideoTableViewCell else {
                return // or fatalError() or whatever
            }
            let indexPath = self.tableView.indexPath(for: cell)
            selectedImage.image = cell.imageViewVideos.image!
            self.showSharePopup()
        }
        else {
            self.downloadVideoLinkAndCreateAsset(path: self.arrayOfVedio[selectedIndex].path!) { (url) in
                self.destinationUrl = url
                self.showSharePopup()
            }
        }
    }
    @IBAction func buttonCloseAction(_ sender: UIButton) {
        self.dismissPopUpView()
    }
    @IBAction func buttoPlayAction(_ sender: UIButton) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEDeatilsPageTVC") as! MEDeatilsPageTVC
        listVC.dictOfDetails = self.arrayOfVedio[sender.tag]
        listVC.videoThumbDict = self.videoThumbDict
        listVC.videoUrlArray = self.videoUrlArray
        listVC.type = "video"
        self.navigationController?.pushViewController(listVC, animated: true)
    }
    // MARK: - Table view data source
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        if videoUrlArray.count == 0{
            return 1
        }
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        //    return  section == 0 ? self.arrayOfVedio.count :  self.arrayOfimages.count
        if videoUrlArray.count == 0{
            return 1
        }
        return  section == 0 ? self.videoUrlArray.count :  self.arrayOfimages.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if videoUrlArray.count == 0{
            return 232
        }
        return 168
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if videoUrlArray.count == 0{
            let cell = tableView.dequeueReusableCell(withIdentifier: "noFileCell", for: indexPath)
            return cell
        }
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell") as! lastestVideoTableViewCell
        cell.btnShare.tag = indexPath.row
        cell.btnPlay.tag = indexPath.row
        
        cell.labelVideosTime.text = ""
        cell.imageViewVideos.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
        cell.imageViewVideos.contentMode = .scaleAspectFill
        cell.imageViewVideos.layer.masksToBounds = true
        cell.imageViewVideos.clipsToBounds = true
        if indexPath.section == 0 {
            cell.btnPlay.isHidden = false
            //        let videoUrl = URL(fileURLWithPath: videoUrlArray[indexPath.row])
            //       cell.imageViewVideos.image = thumbnailForVideoAtURL(url: videoUrl as NSURL)
            if let image = videoThumbDict[videoUrlArray[indexPath.row]]{
                cell.imageViewVideos.image = image
            }else{
                cell.imageViewVideos.image = UIImage(named: "placeholder")
            }
           
        }
        else {
            cell.btnPlay.isHidden = true
            cell.imageViewVideos.downloaded(from:  self.arrayOfimages[indexPath.row].path!)
        }
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0 {
            let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEDeatilsPageTVC") as! MEDeatilsPageTVC
            listVC.dictOfDetails = self.arrayOfVedio[indexPath.row]
            listVC.type = "video"
            listVC.videoThumbDict = self.videoThumbDict
            listVC.videoUrlArray = self.videoUrlArray
            self.navigationController?.pushViewController(listVC, animated: true)
        }
        else {
            // let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEImageDetailViewController") as! MEImageDetailViewController
            let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEDeatilsPageTVC") as! MEDeatilsPageTVC
            listVC.dictOfDetails = self.arrayOfimages[indexPath.row]
            listVC.type = "image"
            self.navigationController?.pushViewController(listVC, animated: true)
        }
    }
    // MARK: - Collection view data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return self.arrayOfImagesShare.count
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellcolletion1", for: indexPath)
        let imageView = cell.viewWithTag(2) as! UIImageView
        imageView.image = UIImage(named: self.arrayOfImagesShare[indexPath.row])
        let labelName = cell.viewWithTag(3) as! UILabel
        labelName.text = arrayOfImagesShareName[indexPath.row]
        imageView.contentMode = .scaleAspectFill
        
        return cell
        
    }
    /* func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
     
     if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) {
     return CGSize(width: 102, height: 95)
     }
     else {
     return CGSize(width: 0, height: 0)
     }
     
     }*/
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.viewOfShare.removeFromSuperview()
        if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) || self.arrayOfImagesShareName[indexPath.row] == "More" {
            
            
            if typeOfFile == "image" {
                
                if indexPath.row == 0 {
                    //Facebook
                    
                    let sharePhoto = SharePhoto()
                    sharePhoto.image = self.selectedImage.image!
                    sharePhoto.isUserGenerated = true
                    sharePhoto.caption = ""
                    let photoContent = SharePhotoContent()
                    photoContent.photos = [sharePhoto]
                    let showDialog = ShareDialog.init(fromViewController: self, content: photoContent, delegate: self)
                    if (showDialog.canShow) {
                        showDialog.show()
                    } else {
                        self.showAlertWithOkButton(message: "It looks like you don't have the Facebook mobile app on your device.")
                    }
                }
                else if indexPath.row == 1 {
                    //Twitter
                    /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
                     API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
                     Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
                    // Swift
                    if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                        // App must have at least one logged-in user to compose a Tweet
                        let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.selectedImage.image!, videoURL: nil)
                        present(composer, animated: true, completion: nil)
                    } else {
                        // Log in, and then check again
                        
                        TWTRTwitter.sharedInstance().logIn { session, error in
                            if session != nil { // Log in succeeded
                                let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: self.selectedImage.image!, videoURL: nil)
                                self.present(composer, animated: true, completion: nil)
                            } else {
                                self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                
                            }
                        }
                    }
                    
                }
                else if indexPath.row == 2 {
                    //Instagram
                    PHPhotoLibrary.shared().performChanges({
                        PHAssetChangeRequest.creationRequestForAsset(from: self.selectedImage.image!)
                    }, completionHandler: { [weak self] success, error in
                        if success {
                            let fetchOptions = PHFetchOptions()
                            fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                            let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                            if let lastAsset = fetchResult.firstObject {
                                let localIdentifier = lastAsset.localIdentifier
                                
                                let urlFeed = "instagram://library?LocalIdentifier=" + localIdentifier
                                guard let url = URL(string: urlFeed) else {
                                    print("Could not open url")
                                    return
                                }
                                DispatchQueue.main.async {
                                    if UIApplication.shared.canOpenURL(url) {
                                        if #available(iOS 10.0, *) {
                                            UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                                                print("self?.delegate?.success")
                                            })
                                        } else {
                                            UIApplication.shared.openURL(url)
                                            print("self?.delegate?.success")
                                            
                                        }
                                    } else {
                                        DispatchQueue.main.async {
                                            self!.showAlertWithOkButton(message: "It looks like you don't have the Instagram mobile app on your device.")
                                        }
                                    }
                                }
                            }
                        } else if let error = error {
                            DispatchQueue.main.async {
                                print(error.localizedDescription)
                                self!.showAlertWithOkButton(message: "Photos access not allowed. Kindly change the settings of your Instagram App.")
                            }
                        }
                        else {
                            print("Could not save the photo")
                        }
                    })
                }
                
                else if indexPath.row == 3 {
                    //SnapChat
                    // Copied from Snapchat Documentation
                    
                    let snapPhoto = SCSDKSnapPhoto(image: self.selectedImage.image!)
                    let snap = SCSDKPhotoSnapContent(snapPhoto: snapPhoto)
                    // snap.sticker = /* Optional, add a sticker to the Snap */
                    // snap.caption = /* Optional, add a caption to the Snap */
                    // snap.attachmentUrl = /* Optional, add a link to the Snap */
                    SVProgressHUD.show()
                    
                    SCSDKSnapAPI(content: snap).startSnapping() { (error: Error?) in
                        SVProgressHUD.dismiss()
                        if let error = error {
                            print(error.localizedDescription)
                        } else {
                            self.addshareDetails(app: "snapchat")
                            // Successfully shared content to Snapchat!
                        }
                    }
                    
                    
                    
                    
                }
                else if indexPath.row == 4 {
                    // LinkedIn
                    let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                    
                    self.navigationController?.pushViewController(webVC, animated: true)
                    // self.sendToLinkedIn(image: UIImage(named: "img1")!)
                }
                else if indexPath.row == 5 {
                    let items = [ self.selectedImage.image!]
                    let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    present(ac, animated: true)
                    ac.completionWithItemsHandler = { activity, completed, items, error in
                        if !completed {
                            // handle task not completed
                            return
                        }
                        else {
                            self.addshareDetails(app: "more")
                        }
                        
                    }
                    //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
                }
                else if indexPath.row == 6 {
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self;
                        mail.setCcRecipients(["yyyy@xxx.com"])
                        mail.setSubject("Your message")
                        mail.setMessageBody("Message body", isHTML: false)
                        let imageData: NSData = self.selectedImage.image!.pngData()! as NSData
                        mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                        self.present(mail, animated: true, completion: nil)
                    }
                }
                else {
                    let items = [self.selectedImage.image!]
                    let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
                    present(ac, animated: true)
                    ac.completionWithItemsHandler = { activity, completed, items, error in
                        if !completed {
                            // handle task not completed
                            return
                        }
                        else {
                            self.addshareDetails(app: "more")
                        }
                        
                    }
                }
            }
            else {
                if indexPath.row == 0 {
                    
                    
                    
                    let content: ShareVideoContent = ShareVideoContent()
                    
                    DispatchQueue.main.async {
                        let video = ShareVideo()
                        video.videoURL = URL(string: self.destinationUrl)
                        content.video = video
                        
                        let shareDialog = ShareDialog()
                        shareDialog.shareContent = content
                        shareDialog.mode = .native
                        shareDialog.delegate = self
                        shareDialog.show()
                    }
                    
                    
                    
                    
                }
                else if indexPath.row == 1 {
                    //Twitter
                    /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
                     API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
                     Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
                    // Swift
                    if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                        // App must have at least one logged-in user to compose a Tweet
                        let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.arrayOfVedio[selectedIndex].path!))
                        present(composer, animated: true, completion: nil)
                    } else {
                        // Log in, and then check again
                        
                        TWTRTwitter.sharedInstance().logIn { session, error in
                            if session != nil { // Log in succeeded
                                let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: nil, videoURL:URL(string: self.arrayOfVedio[self.selectedIndex].path!))
                                self.present(composer, animated: true, completion: nil)
                            } else {
                                self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                
                            }
                        }
                    }
                    
                }
                else if indexPath.row == 2 {
                    //instagram
                    DispatchQueue.main.async {
                        let urlFeed = "instagram://library?LocalIdentifier=" + self.destinationUrl
                        guard let url = URL(string: urlFeed) else {
                            print("Could not open url")
                            return
                        }
                        DispatchQueue.main.async {
                            if UIApplication.shared.canOpenURL(url) {
                                if #available(iOS 10.0, *) {
                                    UIApplication.shared.open(url, options: [:]
                                                              , completionHandler: { (success) in
                                                                print("self?.delegate?.success")
                                                              })
                                } else {
                                    UIApplication.shared.openURL(url)
                                    print("self?.delegate?.success")
                                }
                            } else {
                                print("Instagram not found")
                            }
                        }
                    }
                }
                
                
                else if indexPath.row == 3 {
                    //SnapChat
                    // Copied from Snapchat Documentation
                    
                    let video = SCSDKSnapVideo(videoUrl: URL(string:self.arrayOfVedio[selectedIndex].path!)!)
                    let videoContent = SCSDKVideoSnapContent(snapVideo: video)
                    
                    
                    SVProgressHUD.show()
                    
                    SCSDKSnapAPI(content: videoContent).startSnapping() { (error: Error?) in
                        SVProgressHUD.dismiss()
                        if let error = error {
                            print(error.localizedDescription)
                        } else {
                            self.addshareDetails(app: "snapchat")
                            // Successfully shared content to Snapchat!
                        }
                    }
                    
                    
                    
                }
                else if indexPath.row == 4 {
                    //LinkedI
                    let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                    
                    self.navigationController?.pushViewController(webVC, animated: true)
                    // self.sendToLinkedIn(image: UIImage(named: "img1")!)
                }
                else if indexPath.row == 5 {
                    let urlData = NSData(contentsOf: NSURL(string:self.arrayOfVedio[selectedIndex].path!)! as URL)
                    SVProgressHUD.show()
                    if ((urlData) != nil){
                        
                        print(urlData)
                        
                        
                        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                        let docDirectory = paths[0]
                        let filePath = "\(docDirectory)/tmpVideo.mp4"
                        urlData?.write(toFile: filePath, atomically: true)
                        // file saved
                        
                        let videoLink = NSURL(fileURLWithPath: filePath)
                        
                        
                        let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        
                        activityVC.setValue("Video", forKey: "subject")
                        
                        
                        SVProgressHUD.dismiss()
                        
                        self.present(activityVC, animated: true, completion: nil)
                        
                        activityVC.completionWithItemsHandler = { activity, completed, items, error in
                            if !completed {
                                // handle task not completed
                                return
                            }
                            else {
                                self.addshareDetails(app: "more")
                            }
                            
                        }
                    }
                    //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
                }
                else if indexPath.row == 6 {
                    if MFMailComposeViewController.canSendMail() {
                        let mail = MFMailComposeViewController()
                        mail.mailComposeDelegate = self;
                        mail.setCcRecipients(["yyyy@xxx.com"])
                        mail.setSubject("Your message")
                        mail.setMessageBody("Message body", isHTML: false)
                        do {
                            let fileData: NSData = try Data(contentsOf: URL(string: self.arrayOfVedio[selectedIndex].path!)!) as NSData
                            mail.addAttachmentData(fileData as Data, mimeType: "mp4", fileName: "myfile.mp4")
                            
                            self.present(mail, animated: true, completion: nil)
                            
                        }
                        catch {
                            print("error")
                        }
                    }
                }
                else {
                    let urlData = NSData(contentsOf: NSURL(string:self.arrayOfVedio[selectedIndex].path!)! as URL)
                    
                    if ((urlData) != nil){
                        SVProgressHUD.show()
                        print(urlData)
                        
                        
                        let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                        let docDirectory = paths[0]
                        let filePath = "\(docDirectory)/tmpVideo.mp4"
                        urlData?.write(toFile: filePath, atomically: true)
                        // file saved
                        
                        let videoLink = NSURL(fileURLWithPath: filePath)
                        
                        
                        let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
                        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
                        
                        activityVC.setValue("Video", forKey: "subject")
                        
                        SVProgressHUD.dismiss()
                        
                        
                        self.present(activityVC, animated: true, completion: nil)
                        activityVC.completionWithItemsHandler = { activity, completed, items, error in
                            if !completed {
                                // handle task not completed
                                return
                            }
                            else {
                                self.addshareDetails(app: "more")
                            }
                            
                        }
                    }
                }
            }
        }
        else {
            self.showAlertWithOkButton(message: "Go to settings page to turn on this app" )
        }
    }
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.addshareDetails(app: "mail")
        controller.dismiss(animated: true, completion: nil)
    }
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        self.addshareDetails(app: "twitter")
        print("success")
    }
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        self.addshareDetails(app: "facebook")
        print("")
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
        print("")
    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        print("")
    }
    func showSharePopup() {
        DispatchQueue.main.async {
            let window = UIApplication.shared.delegate?.window!
            
            self.viewOfShare.frame = CGRect(x: 0, y: (window!.frame.height) - 397, width: (window!.frame.width), height: 397)
            window!.addSubview(self.viewOfShare)
            self.viewOfShare.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
            
            UIView.animate(withDuration: 0.33, animations: {
                self.viewOfShare.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
            })
        }
    }
    
    func dismissPopUpView(){
        UIView.animate(withDuration: 0.33, animations: {
            
        }, completion: { (completed) in
            
        })
        UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
            
        }, completion: { (completed) in
            self.viewOfShare.removeFromSuperview()
            
        })
    }
    
    func addshareDetails(app:String){
        
        ServiceManager.sharedInstance.post(withServiceName: "addshare", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)","fileid":"\(typeOfFile ==  "image" ? self.arrayOfimages[selectedIndex].fileid! : self.arrayOfVedio[selectedIndex].fileid!)","socialapp":app], withHud: true) { (success, result, error) in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    
                }
                else {
                    //  self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                }
            }
            else {
                // self.showAlertWithOkButton(message: "Please try again")
            }
        }
        
        
    }
    @objc func tapBlurButton(gesture: UIGestureRecognizer) {
        
        if let swipeGesture = gesture as? UISwipeGestureRecognizer {
            
            switch swipeGesture.direction {
            case .right:
                print("Swiped right")
            case .down:
                print("Swiped down")
                self.dismissPopUpView()
            case .left:
                print("Swiped left")
            case .up:
                print("Swiped up")
            default:
                break
            }
        }
    }
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    func getLastestList() {
        let user = MEUserDetails.currentUser.id
        ServiceManager.sharedInstance.post(withServiceName: "filelisting", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { (success, result, error) in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    self.arrayOfimages.removeAll()
                    self.arrayOfVedio.removeAll()
                    DispatchQueue.main.async {
                        SVProgressHUD.show()
                    }
                        
                        let listDetailsDict = result as!  NSDictionary
                        self.labelNoOfVideos.text =  "\(listDetailsDict.value(forKey: "videocount") as! Int)"
                        self.labelNoOfImages.text =  "\(listDetailsDict.value(forKey: "imagecount") as! Int)"
                        for items in listDetailsDict.value(forKey: "video") as! NSArray {
                            let obj  = listOfFiles().initWithDictionary(userDictionary: items as! NSDictionary)
                            self.arrayOfVedio.append(obj)
                        }
                    
                        self.videoUrlArray.removeAll()
                    
                        let videoDetails = listDetailsDict.value(forKey: "video") as! NSArray
                        if videoDetails.count > 0 {
                            
                            for i in 0...videoDetails.count-1 {
                                let path = (videoDetails[i] as! NSDictionary).value(forKey: "path") as! String
                                self.videoUrlArray.append(path)
                            }
                            print("videos: \(self.videoUrlArray[0].count)")
                            
                           
                            for i in 0...videoDetails.count-1 {
                                let path = (videoDetails[i] as! NSDictionary).value(forKey: "path") as! String
                                if let pathURL = URL(string: path) {
                                    let image = self.thumbnailForVideoAtURL(url: pathURL as NSURL)
                                    self.videoThumbDict[path] = image
                                   
                                }
                            }
                                SVProgressHUD.dismiss()
                                DispatchQueue.main.async {
                                    self.videoThumbnail.image = self.videoThumbDict.first?.value
                                    self.tableView.reloadData()
                                }
                            
                        }
                    
                    let imageArray = listDetailsDict.value(forKey: "image") as! NSArray
                    if imageArray.count > 0 {
                        let path = (imageArray[0] as! NSDictionary).value(forKey: "path") as! String
                        DispatchQueue.main.async {
                        self.imageThumbnail.kf.setImage(with: URL(string:path), placeholder:kplaceholderImage)
                        }
                    }
                        //                for items in videoDetails.value(forKey: "path") as! String {
                        //
                        //                }
                        /*  for items in listDetailsDict.value(forKey: "image") as! NSArray {
                         let obj  = listOfFiles().initWithDictionary(userDictionary: items as! NSDictionary)
                         self.arrayOfimages.append(obj)
                         }*/
                          
                           
                        DispatchQueue.main.async {
                            print("Reloading TableView")
                            self.tableView.reloadData()
//                            self.tableView.beginUpdates()
//                            self.tableView.endUpdates()
//                            ARSLineProgress.hide()
                            //ARSLineProgress.hide()
                        }
                        
                    
                    
                }
                else {
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
            }
            else {
                self.showAlertWithOkButton(message: "Please try again")
            }
            self.getAppSaved()
        }
        
    }
    func getAppSaved(){
        
        SVProgressHUD.show()
        ServiceManager.sharedInstance.post(withServiceName: "listapp", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) {  (success, result, error) in
            SVProgressHUD.dismiss()
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let app =  (result as! NSDictionary)["socialapps"] as! String
                    arrayOfSavedApp = app.components(separatedBy: ",")
                }
                else {
                }
            }
            else {
            }
            
            
        }
        
        
    }
    func downloadVideoLinkAndCreateAsset(path:String,completion: @escaping (String) -> Void) {
        
        // use guard to make sure you have a valid url
        
        SVProgressHUD.show()
        
        DispatchQueue.global(qos: .background).async {
            if let url = URL(string: path),
               let urlData = NSData(contentsOf: url) {
                let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
                let filePath="\(documentsPath)/tempFile.mp4"
                DispatchQueue.main.async {
                    urlData.write(toFile: filePath, atomically: true)
                    var videoAssetPlaceholder:PHObjectPlaceholder!
                    PHPhotoLibrary.shared().performChanges({
                        let request =  PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                        videoAssetPlaceholder = request!.placeholderForCreatedAsset
                    }) { completed, error in
                        SVProgressHUD.dismiss()
                        if completed {
                            print("Video is saved!")
                            let localID = NSString(string: videoAssetPlaceholder.localIdentifier)
                            let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: NSString.CompareOptions.regularExpression, range: NSRange())
                            let ext = "mp4"
                            let assetURLStr =
                                "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                            
                            completion(assetURLStr)
                        }
                    }
                }
            }
        }
    }
    
    //MARK:- Show Alert With Ok Button
    func showAlertWithOkButton(message:String){
        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
            
        }
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     // Get the new view controller using segue.destination.
     // Pass the selected object to the new view controller.
     }
     */
    
}
