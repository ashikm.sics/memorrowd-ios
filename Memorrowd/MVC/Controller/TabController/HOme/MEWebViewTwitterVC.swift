//
//  MEWebViewTwitterVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 18/08/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import WebKit
import TwitterKit
class MEWebViewTwitterVC: UIViewController {
    @IBOutlet var webView             : WKWebView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
      if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
            // App must have at least one logged-in user to compose a Tweet
            let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: UIImage(named: "img1"), videoURL: nil)
            present(composer, animated: true, completion: nil)
        } else {
            // Log in, and then check again
        
            TWTRTwitter.sharedInstance().logIn { session, error in
                if session != nil { // Log in succeeded
                    let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: UIImage(named: "img1"), videoURL: nil)
                    self.present(composer, animated: true, completion: nil)
                } else {
                    self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                   
                }
            }
        }
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
          
       }
    //MARK:- Show Alert With Ok Button
              func showAlertWithOkButton(message:String){
                  AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                      
                  }
              }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
         @objc func actionPushNotificationInActiveState(_ notification: Notification) {
    
          let message = "Its time to post your post."
               
                 let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
                 let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
                    let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                      listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                     self.navigationController?.present(listVC, animated: true, completion: {
                        
                     })
                  
                 })
                 let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                     self.dismiss(animated: true, completion: nil)
                 })
                 alertController.addAction(buttonView)
                 alertController.addAction(buttonCancel)
                 self.present(alertController, animated: true, completion: nil)
                 
             
         }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
              let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                              listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                             self.navigationController?.present(listVC, animated: true, completion: {
                                
                             })
      }
}
extension MEWebViewTwitterVC: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished navigating to url \(webView.url)")
        let url = webView.url!
               print(url)
               

        if url.host == "www.srishtis.com" {
                   if url.absoluteString.range(of:"code") != nil {
                       // Extract the authorization code.
                       let urlParts = url.absoluteString.components(separatedBy:"?")
                    let code = urlParts[1].components(separatedBy:"=")[1]
                       
                    TWTRTwitter.sharedInstance().logIn { session, error in
                                   if session != nil { // Log in succeeded
                                       let composer = TWTRComposerViewController.init(initialText: "just setting up my Twitter Kit", image: UIImage(named: "img1"), videoURL: nil)
                                       self.present(composer, animated: true, completion: nil)
                                   } else {
                                       self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                      
                                   }
                               }
                   }
               }
               
             
    }

}

extension MEWebViewTwitterVC: TWTRComposerViewControllerDelegate {
    func composerDidCancel(_ controller: TWTRComposerViewController) {
        dismiss(animated: false, completion: nil)
    }
    
    func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
        dismiss(animated: false, completion: nil)
    }
    
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        dismiss(animated: false, completion: nil)
    }
}
