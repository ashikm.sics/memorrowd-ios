//
//  MEWebViewVc.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 04/08/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import WebKit
import SVProgressHUD

class MEWebViewVc: UIViewController {
    @IBOutlet var webView             : WKWebView!
    @IBOutlet var viewLinkedin             : UIView!
    @IBOutlet var imageView             : UIImageView!

    var viewBackground                             = UIView()
    
    @IBOutlet var btnPost                          : UIButton!
    var id = ""
    // MARK: Constants
    
    let linkedInKey = "86i7xc1dyjkueu"
    
    let linkedInSecret = "VbrgCmbHud6r9CGu"
    
    let authorizationEndPoint = "https://www.linkedin.com/uas/oauth2/authorization"
    
    let accessTokenEndPoint = "https://www.linkedin.com/uas/oauth2/accessToken"
    override func viewDidLoad() {
        super.viewDidLoad()
       
        starLinkedIntAuthorization()
       
        // Do any additional setup after loading the view.
    }
    override func viewWillAppear(_ animated: Bool) {
         webView.navigationDelegate = self
   
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
           self.dismissPopUpView()
       }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
         @objc func actionPushNotificationInActiveState(_ notification: Notification) {
    
          let message = "Its time to post your post."
               
                 let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
                 let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
                   let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                      listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                     self.navigationController?.present(listVC, animated: true, completion: {
                        
                     })
                  
                 })
                 let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                     self.dismiss(animated: true, completion: nil)
                 })
                 alertController.addAction(buttonView)
                 alertController.addAction(buttonCancel)
                 self.present(alertController, animated: true, completion: nil)
                 
             
         }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
              let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                              listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                             self.navigationController?.present(listVC, animated: true, completion: {
                                
                             })
      }
    // MARK: LinkedIn
      
       
          
          func starLinkedIntAuthorization() {
          
              // Specify the response type which should always be "code".
              let responseType = "code"
              
              // Set the redirect URL. Adding the percent escape characthers is necessary.
              let redirectURL = "https://www.srishtis.com/".addingPercentEncoding(withAllowedCharacters: .alphanumerics)
             
              
              // Create a random string based on the time intervale (it will be in the form linkedin12345679).
              let state = "linkedin\(Int(NSDate().timeIntervalSince1970))"
              
              // Set preferred scope.
              let scope = "w_member_social,r_emailaddress,r_liteprofile"
              
              
              // Create the authorization URL string.
              var authorizationURL = "\(authorizationEndPoint)?"
              authorizationURL += "response_type=\(responseType)&"
              authorizationURL += "client_id=\(linkedInKey)&"
              authorizationURL += "redirect_uri=\(String(describing: redirectURL!))&"
              authorizationURL += "state=\(state)&"
              authorizationURL += "scope=\(scope)"
              
              print(authorizationURL)
              if let url = URL(string: authorizationURL) {
               webView.load(URLRequest(url: url))  }
              
             
          }
          
          
          func requestForAccessToken(authorizationCode: String) {
            ServiceManager.sharedInstance.requestForAccessToken(authorizationCode: authorizationCode) { (success, result, error) in
                if success == true {
                  
                       
                    let accessToken = (result!)["access_token"] as! String
                       print(accessToken)
                    UserDefaults.standard.set(accessToken, forKey: "LIAccessToken")
                    UserDefaults.standard.synchronize()
                    self.getProfileInfo()
                }
            }
          }
    // MARK: LinkedIn
   func getProfileInfo() {
    ServiceManager.sharedInstance.getProfileInfo { (success, result, error) in
        if success == true {
            
            self.id = (result!)["id"] as! String
            DispatchQueue.main.async {
                self.showSharePopup()
            }
            
           
        }
    }
       }
  
    func registerImage(id:String){
        SVProgressHUD.show()
        ServiceManager.sharedInstance.registerImageToLinkedIn(id: id) { (success, result, error) in
            if success == true {
                let asst = ((result!)["value"] as! NSDictionary)["asset"] as! String
                let uploadUrl = ((((result!)["value"] as! NSDictionary)["uploadMechanism"] as! NSDictionary)["com.linkedin.digitalmedia.uploading.MediaUploadHttpRequest"] as! NSDictionary)["uploadUrl"] as! String
                self.uploadBinaryImage(assest: asst, uploadUrl: uploadUrl, id: id)
            }
            else {
               SVProgressHUD.dismiss()
            }
        }
    }
    func uploadBinaryImage(assest:String,uploadUrl:String,id:String) {
       // UIImage(named: "img1")!
        ServiceManager.sharedInstance.createMultipart(uploadUrl: uploadUrl, fileData: UIImage(named: "img1")!.pngData()!, fileName: "upload-file.png") { (fileURL, e) in
            if e == nil {
                  self.sharePost(assest: assest, id: id)
                   print("FILE URL: " + fileURL!)
            }
        }
      /* ServiceManager.sharedInstance.UploadImageBinaryFileToLinkedIn(assest: assest, uploadUrl: uploadUrl, id: id) {  (success, error) in
            if success == true {
                self.sharePost(assest: assest, id: id)
            }
            else {
               ARSLineProgress.hide()
            }
        }*/
        
    }
    func sharePost(assest:String,id:String) {
        ServiceManager.sharedInstance.shareFileToLinkedIn(assest: assest, id: id) { (success, error) in
             if success == true {
                 SVProgressHUD.dismiss()
                 DispatchQueue.main.async {
                    self.viewBackground.removeFromSuperview()
                    self.showAlertWithOkButton(message: "Image Posted")
               
                }
            }
                
            else {
               SVProgressHUD.dismiss()
            }
        }
    }
    //MARK:- Show Alert With Ok Button
               func showAlertWithOkButton(message:String){
                   AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                 
                     self.navigationController?.popViewController(animated: true)
                   }
               }
   @IBAction func buttonPostAction(_ sender: UIButton) {
    self.registerImage(id: id)
     
                 }
 // MARK: Show Share Popup
    func showSharePopup() {
           let window = UIApplication.shared.delegate?.window!
          viewBackground = UIView(frame: CGRect(x: 0, y: 0, width: window!.frame.size.width, height:  window!.frame.size.height))
                   viewBackground.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
                   window!.addSubview(viewBackground)
         
           self.viewLinkedin.frame = CGRect(x: 15, y: ((window?.frame.height)! - 312) / 2, width: (window?.frame.width)! - 30, height: 312)
            window!.addSubview(self.viewBackground)
         self.viewBackground.addSubview(self.viewLinkedin)
            self.viewBackground.bringSubviewToFront(self.viewLinkedin)
           self.viewBackground.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
           
           UIView.animate(withDuration: 0.33, animations: {
               self.viewBackground.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
           })
       }
       func dismissPopUpView(){
                 UIView.animate(withDuration: 0.33, animations: {
                   
                 }, completion: { (completed) in
                     
                 })
                 UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
                     
                 }, completion: { (completed) in
                     self.viewBackground.removeFromSuperview()
                   
                 })
             }
    
}
extension MEWebViewVc: WKNavigationDelegate {

    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("Finished navigating to url \(webView.url)")
        let url = webView.url!
               print(url)
               

        if url.host == "www.srishtis.com" {
                   if url.absoluteString.range(of:"code") != nil {
                       // Extract the authorization code.
                       let urlParts = url.absoluteString.components(separatedBy:"?")
                    let code = urlParts[1].components(separatedBy:"=")[1]
                       
                    requestForAccessToken(authorizationCode: code)
                   }
               }
               
             
    }

}
