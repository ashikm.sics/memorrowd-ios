//
//  MEDeatilsPageTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 08/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import AVFoundation
import MediaPlayer
import AVKit
import FBSDKShareKit
import TwitterKit
import SCSDKCreativeKit
import SCSDKStoryKit
import MessageUI
import Social
import TikTokOpenSDK
import SVProgressHUD

class MEDeatilsPageTVC: UITableViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UITextViewDelegate,UICollectionViewDelegateFlowLayout,UIDocumentInteractionControllerDelegate, MFMailComposeViewControllerDelegate,TWTRComposerViewControllerDelegate,SharingDelegate,popUpDelegate{
   
    @IBOutlet var tblView: UITableView!
    @IBOutlet weak var popUpImageView: UIImageView!
    @IBOutlet weak var alertPopUpView: UIView!
    @IBOutlet var labelVideoTitle                  : UILabel!
       @IBOutlet var labelDate                        : UILabel!
       @IBOutlet var labelTime                        : UILabel!
       @IBOutlet var labelTitle                       : UILabel!

    @IBOutlet weak var textFieldDate: UITextField!
    @IBOutlet weak var textFieldTime: UITextField!
    @IBOutlet var btnEditFile                      : UIButton!
       @IBOutlet var btnSetNOtification               : UIButton!
       @IBOutlet var btnShare                         : UIButton!
       @IBOutlet var viewPlayVideo                    : UIView!
       @IBOutlet var viewShare                        : UIView!
       @IBOutlet var imgViewSelectedName              : UIImageView!
       var viewBackground                             = UIView()
       var activeTextField                  =  UITextField()
       var playerLayer                      = AVPlayerLayer()
       let playerController                 = AVPlayerViewController()
         var txtVideoTitle      = ""
         var txtVideoCategory   = ""
         var txtViewDescription = ""
           var notificationDate = ""
          var notificationTime = ""
          @IBOutlet var datePicker                : UIDatePicker!
          var arrayOfImages = ["fb_Square","twitter_square","insta_Square","snapchat_Square","linkedin_Square","whatsapp_square","email_square"]
    
         var arrayOfImagesShare = ["fb_round","twitter_round","insta_round","snapchat_round","linkedin_round","whatsapp_round","email_round","more"]
         var arrayOfImagesShareName = ["Facebook","Twitter","Instagram","Snapchat","Linkedin","Whatsapp","Email","More"]
       var arrayOfSelectedApp = [String]()
    var videoUrlArray = [String]()
    var videoThumbDict = [String: UIImage]()
          var selectedButton = 0
          var webView:  WKWebView!
          var arrayOfimagesOrVedio = [listOfFiles]()
          var dictOfDetails = listOfFiles()
          var destinationUrl = ""
     var type = ""
    fileprivate lazy var snapAPI = {
            return SCSDKSnapAPI()
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print(arrayOfSavedApp)
        UserDefaults.standard.setValue("0", forKey: "popup")
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        self.setUpNavigationBar()
        self.getVideoList()
        self.customUI()
        self.getLastestList()
        print(UserDefaults.standard.bool(forKey: "popupVC"))
        print("Hello")
    }
    override func viewWillAppear(_ animated: Bool) {
             super.viewWillAppear(animated)
               if type ==  "video" {
                self.imgViewSelectedName.isHidden = true
                  self.playVideo(videoUrl: URL(string: self.dictOfDetails.path!)!)
                   self.downloadVideoLinkAndCreateAsset(){ stringUrl in
                                             self.destinationUrl = stringUrl
                }
               }
               else {
                  self.imgViewSelectedName.isHidden = false
                self.imgViewSelectedName.downloaded(from:  dictOfDetails.path!)

                 
               }
       
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
          
       }
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
                   let navBarAppearance = UINavigationBarAppearance()
                   navBarAppearance.configureWithOpaqueBackground()
                   navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.backgroundColor = kAppThemeYellowTop
                   self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                   self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
               }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        
        self.navigationItem.title = type == "image" ? "Image" : "Video"
        self.selectedButton = type == "image" ? 1 : 0
        if type == "image" {
        self.btnEditFile.backgroundColor = UIColor.gray
                   self.btnSetNOtification.backgroundColor = kAppThemeYellowTop
                   self.btnShare.backgroundColor = kAppThemeYellowTop
            self.labelTitle.text = "Edit File"
        }
               self.txtViewDescription = self.dictOfDetails.descriptionFile!
               self.txtVideoTitle = self.dictOfDetails.title!
               self.txtVideoCategory = self.dictOfDetails.category!
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
      
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
        let sideButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backAction))
        self.navigationItem.leftBarButtonItem = sideButtonButton
        
    }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
         @objc func actionPushNotificationInActiveState(_ notification: Notification) {
    
          let message = "Its time to post your post."
               
                 let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
                 let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
                    let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                      listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                     self.navigationController?.present(listVC, animated: true, completion: {
                        
                     })
                  
                 })
                 let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                     self.dismiss(animated: true, completion: nil)
                 })
                 alertController.addAction(buttonView)
                 alertController.addAction(buttonCancel)
                 self.present(alertController, animated: true, completion: nil)
                 
             
         }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
              let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                              listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                             self.navigationController?.present(listVC, animated: true, completion: {
                                
                             })
      }
       //MARK:- CustomUI
          func customUI() {
            
           
            self.btnEditFile.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 0, cornerRadius: 22)
             self.btnSetNOtification.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 0, cornerRadius: 22)
             self.btnShare.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 0, cornerRadius: 22)
            self.viewShare.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 1, cornerRadius: 8)
            self.labelDate.text = self.dictOfDetails.date!
            self.labelTime.text = self.dictOfDetails.time!
            self.labelVideoTitle.text = self.dictOfDetails.title!
            
         
          }
          @objc func backAction() {
              self.navigationController?.popViewController(animated: true)
             
            }
    //MARK:- Play Video
    func playVideo(videoUrl: URL)  {
           guard let path = Bundle.main.path(forResource: "YogaTutorial", ofType:"mp4") else {
               debugPrint("Dummy Video not found")
               return
           }
          /* if dictOfVideo.video! != "" {
           let player = AVPlayer(url: URL(string: dictOfVideo.video!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!)!)
           playerController.player = player
           }*/
        let player = AVPlayer(url: videoUrl)                    //URL(string: self.dictOfDetails.path!)!)
           playerController.player = player
           self.viewPlayVideo.addSubview(playerController.view)
           playerController.view.frame = CGRect(x: 0, y: 0, width: self.viewPlayVideo.frame.width, height: self.viewPlayVideo.frame.height)
           playerController.view.tintColor = kAppThemeYellowTop
           playerController.view.setBorderAndCornerRadius(borderColor: .clear, borderWidth: 0.0, cornerRadius: 5.0)
           playerController.player?.play()
        
       }
    //MARK:- Button Actions
          @IBAction func buttonEditFileAction(_ sender: UIButton) {
            self.labelTitle.text = "Edit File"
            self.selectedButton = sender.tag
            print(sender.tag)
            sender.backgroundColor = UIColor.gray
            self.btnSetNOtification.backgroundColor = kAppThemeYellowTop
            self.btnShare.backgroundColor = kAppThemeYellowTop
            self.tableView.reloadData()
          }
         
           @IBAction func buttonSetNOtificationAction(_ sender: UIButton) {
             self.labelTitle.text = "Set Notification"
             self.selectedButton = sender.tag
            print(sender.tag)
             sender.backgroundColor = UIColor.gray
            self.btnEditFile.backgroundColor = kAppThemeYellowTop
            self.btnShare.backgroundColor = kAppThemeYellowTop
               self.tableView.reloadData()
           }
            @IBAction func buttonShareAction(_ sender: UIButton) {
                 self.labelTitle.text = ""
               self.selectedButton = sender.tag
                print(sender.tag)
                 sender.backgroundColor = UIColor.gray
                self.btnEditFile.backgroundColor = kAppThemeYellowTop
                self.btnSetNOtification.backgroundColor = kAppThemeYellowTop
                 self.tableView.reloadData()
           }
            @IBAction func buttonSubmitNotificationAction(_ sender: UIButton) {
                self.setNotificationDetails()
            }
    
    @IBAction func buttonPickerAction(_ sender: UIDatePicker) {
        if self.activeTextField.tag == 10 {
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "yyyy-MM-dd"
            self.notificationDate = dateFormater.string(from: datePicker.date)
            self.activeTextField.text = dateFormater.string(from: datePicker.date)
        } else {
            let dateFormater = DateFormatter()
            dateFormater.dateFormat = "HH:mm"
            self.notificationTime = dateFormater.string(from: datePicker.date)
            
            self.activeTextField.text = dateFormater.string(from: datePicker.date)
        }
    }
    
            @IBAction func buttonSubmitFileAction(_ sender: UIButton) {
                   self.editVideoDetails()
               }
            @IBAction func buttonDeleteFileAction(_ sender: UIButton) {
               
                self.deleteVideoDetails()
               }
            @IBAction func buttonShareFileAction(_ sender: FBShareButton) {
                                    let photo:SharePhoto = SharePhoto()
                                    var message:String?
                                    photo.image = UIImage(named: "img1")
                                    photo.isUserGenerated = true
                                    let content = SharePhotoContent()
                                    content.photos = [photo];
                                   
                                    sender.shareContent = content
                                  
                                              
                  }
             @IBAction func buttonCloseShareFileAction(_ sender: UIButton) {
                self.dismissPopUpView()
                }
   // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 4
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if section == 0 {
   //         return self.arrayOfimagesOrVedio.count
            return self.videoUrlArray.count
        }
       
        else {
          return 1
        }
        
    }
  
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if self.selectedButton == 0 {
            if indexPath.section == 0  {
                return 168
            }
            else {
                return 0
            }
        }
        else if self.selectedButton == 2 {
             if indexPath.section == 1  {
                 return 397
             }
             else {
                 return 0
             }
        }
        else if self.selectedButton == 1 {
            if indexPath.section == 2  {
               return 397
            }
            else {
               return 0
             }
                  
        }
        else  {
            if indexPath.section == 3  {
                  return 397
             }
            else {
                  return 0
            }
          }
       }
   override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
      if indexPath.section == 0 {
       let cell  = tableView.dequeueReusableCell(withIdentifier: "cell1") as! lastestVideoTableViewCell
       cell.imageViewVideos.setBorderAndCornerRadius(borderColor: UIColor.clear.withAlphaComponent(0.5), borderWidth: 0.0, cornerRadius: 5.0)
        cell.imageViewVideos.contentMode = .scaleAspectFill
      
            cell.btnPlay.isHidden = false
      //      cell.imageViewVideos.image =   UIImage(named: "img1")
//         let videoUrl = URL(fileURLWithPath: videoUrlArray[indexPath.row])
//            cell.imageViewVideos.image = thumbnailForVideoAtURL(url: videoUrl as NSURL)
        cell.imageViewVideos.image = videoThumbDict[videoUrlArray[indexPath.row]]

        
         return cell
       }
        else if indexPath.section == 1 {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell2") as! videoNotificationTableViewCell
            cell.txtSelectDate.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressedForDate))
            cell.txtSelectTime.addInputViewTimeDatePicker(target: self, selector: #selector(doneButtonPressedForTime))
        let collectionViewShare1 = cell.viewWithTag(100) as! UICollectionView
        let width = (kScreenWidth - 160) / 4
         let columnLayout = ColumnFlowLayout.init(cellsPerRow: 3, minimumInteritemSpacing: 5.0, minimumLineSpacing: 10.0, sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), cellHeight: 45, cellWidth: width,scrollDirec: .vertical)
         collectionViewShare1.collectionViewLayout = columnLayout
         collectionViewShare1.reloadData()
            return cell
        }
        else if indexPath.section == 2 {
        let cell  = tableView.dequeueReusableCell(withIdentifier: "cell3") as! videoEditTableViewCell
        
        cell.txtViewDescription.text = self.dictOfDetails.descriptionFile!
        cell.txtVideoTitle.text = self.dictOfDetails.title!
        cell.txtVideoCategory.text = self.dictOfDetails.category!
        cell.labelTitle.text = type == "image" ? "Image Title" : "Video Title"
        cell.labelAlbum.text = type == "image" ? "Image  Category/Album" : "Video  Category/Album"

        return cell
              }
        else {
              let cell  = tableView.dequeueReusableCell(withIdentifier: "cell4")
              let collectionViewShare = cell!.viewWithTag(3) as! UICollectionView
                let width = (kScreenWidth - 60) / 3
                   let columnLayout = ColumnFlowLayout.init(cellsPerRow: 3, minimumInteritemSpacing: 5.0, minimumLineSpacing: 10.0, sectionInset: UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0), cellHeight: 100, cellWidth: width,scrollDirec: .vertical)
                   
                     collectionViewShare.collectionViewLayout = columnLayout
                   
               collectionViewShare.reloadData()
               return cell!
              }
   }
    
    @objc func doneButtonPressedForDate() {
        if let cell = tblView.cellForRow(at: IndexPath(row: 0, section: 1)) as? videoNotificationTableViewCell {
            if let  datePicker = cell.txtSelectDate.inputView as? UIDatePicker {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                cell.txtSelectDate.text = dateFormatter.string(from: datePicker.date)
            }
            cell.txtSelectDate.resignFirstResponder()
        }
     }
    
    @objc func doneButtonPressedForTime() {
      
        if let cell = tblView.cellForRow(at: IndexPath(row: 0, section: 1)) as? videoNotificationTableViewCell {
            if let  datePicker = cell.txtSelectTime.inputView as? UIDatePicker {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                cell.txtSelectTime.text = dateFormatter.string(from: datePicker.date)
            }
            cell.txtSelectTime.resignFirstResponder()
        }
     }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
       // let videoUrl = videoUrlArray[indexPath.row]
        //self.playVideo(videoUrl: URL(string: videoUrl)!)
    }
    
    // MARK: - Collection view data source
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView.tag == 100 {
        return arrayOfImages.count - 1
        }
        else {
          return arrayOfImagesShare.count - 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if collectionView.tag == 100 {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellcolletion", for: indexPath)
        let imageView = cell.viewWithTag(1) as! UIImageView
        imageView.image = UIImage(named: self.arrayOfImages[indexPath.row])
             imageView.contentMode = .scaleAspectFill
            if  self.arrayOfSelectedApp.contains(arrayOfImagesShareName[indexPath.row]) {
         
                cell.layer.borderColor = UIColor.blue.cgColor
                cell.layer.borderWidth = 1
                cell.isSelected = true
            }
            else {
              
                let cell = collectionView.cellForItem(at: indexPath)
                cell?.layer.borderColor = UIColor.clear.cgColor
                cell?.layer.borderWidth = 1
                cell?.isSelected = false
            }
         
        return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellcolletion1", for: indexPath)
            let imageView = cell.viewWithTag(2) as! UIImageView
            imageView.image = UIImage(named: self.arrayOfImagesShare[indexPath.row])
            let labelName = cell.viewWithTag(3) as! UILabel
            labelName.text = arrayOfImagesShareName[indexPath.row]
            
          /*  if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) || self.arrayOfImagesShareName[indexPath.row] == "More" {
                               cell.contentView.isHidden = false
                         }
                         else {
                          cell.contentView.isHidden = true
                         }*/
            return cell
        }
    }
  /*  func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
          if collectionView.tag == 10 {
            if  self.arrayOfSelectedApp.contains(arrayOfImagesShareName[indexPath.row]) {
                
            }
            else {
                
            }
        }
    }*/
  /*  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) {
                        if collectionView.tag == 10 {
                                return CGSize(width: 45, height: 41)
                               }
                                else {
                                return CGSize(width: 102, height: 95)
                               }
                  }
                  else {
                       return CGSize(width: 0, height: 0)
                  }
        
       
    }*/
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
       if collectionView.tag == 100 {
         let cell = collectionView.cellForItem(at: indexPath)
         if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) || self.arrayOfImagesShareName[indexPath.row] == "More" {
        if  !self.arrayOfSelectedApp.contains(arrayOfImagesShareName[indexPath.row]) {
        self.arrayOfSelectedApp.append(arrayOfImagesShareName[indexPath.row])
            cell!.layer.borderColor = UIColor.blue.cgColor
            cell!.layer.borderWidth = 1
            cell!.isSelected = true
        }
        else {
            let index = self.arrayOfSelectedApp.firstIndex(of: arrayOfImagesShareName[indexPath.row])
                    self.arrayOfSelectedApp.remove(at: index!)
            let cell = collectionView.cellForItem(at: indexPath)
            cell?.layer.borderColor = UIColor.clear.cgColor
            cell?.layer.borderWidth = 1
            cell?.isSelected = false
        }
        }
        else {
         self.showAlertWithOkButton(message: "Go to settings page to turn on this app" )
                   
               }
        }
       else {
        if arrayOfSavedApp.contains(self.arrayOfImagesShareName[indexPath.row]) || self.arrayOfImagesShareName[indexPath.row] == "More" {
           
                                       
        if type == "image" {
            if indexPath.row == 6{
                if MFMailComposeViewController.canSendMail() {
                       let mail = MFMailComposeViewController()
                       mail.mailComposeDelegate = self;
                       mail.setCcRecipients([""])
                  mail.setSubject(dictOfDetails.title)
                  mail.setMessageBody("Title - \(self.dictOfDetails.title ?? "")\n Description - \(self.dictOfDetails.descriptionFile ?? "")\n Category - \(self.dictOfDetails.category ?? "")" ?? "", isHTML: false)
                  let imageData: NSData = self.imgViewSelectedName.image!.pngData()! as NSData
                       mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                       self.present(mail, animated: true, completion: nil)
                     }
                else if let emailUrl = createEmailUrl(to: "", subject: self.dictOfDetails.title, body: "Title - \(self.dictOfDetails.title ?? "")\n Description - \(self.dictOfDetails.descriptionFile ?? "")\n Category - \(self.dictOfDetails.category ?? "")" ?? "") {
                                UIApplication.shared.open(emailUrl)
                            }
            }
            else if indexPath.row == 0{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                print(popup)
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Image"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
            }
            
            else if indexPath.row == 2{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Image"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            else if indexPath.row == 3{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Image"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            else if indexPath.row == 4{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Image"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            else if indexPath.row == 5{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Image"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            
            else {
                continueSharingImage(indexPath: indexPath)
            }
            //
        }
        else {
            
            if indexPath.row == 6{
                if MFMailComposeViewController.canSendMail() {
                       let mail = MFMailComposeViewController()
                       mail.mailComposeDelegate = self;
                       mail.setCcRecipients([""])
                  mail.setSubject(dictOfDetails.title)
                  mail.setMessageBody(dictOfDetails.descriptionFile ?? "", isHTML: false)
                  let imageData: NSData = self.imgViewSelectedName.image!.pngData()! as NSData
                       mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                       self.present(mail, animated: true, completion: nil)
                     }
            }
            else if indexPath.row == 0{
                let popup = UserDefaults.standard.string(forKey: "popupVC")
                if popup == "0" {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Video"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            
            else if indexPath.row == 2{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Video"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            else if indexPath.row == 3{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Video"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            else if indexPath.row == 4{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Video"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            else if indexPath.row == 5{
                let popup = UserDefaults.standard.bool(forKey: "popupVC")
                if popup == true {
                    print("No popup")
                }
                else {
                let vc = storyboard?.instantiateViewController(withIdentifier: "PopUpVC") as! PopUpVC
                vc.delegate = self
                vc.indexPath = indexPath
                vc.type = "Video"
                vc.modalPresentationStyle = .overCurrentContext
              present(vc, animated:true)
                }
             
            }
            
            else {
                continueSharingVideo(indexPath: indexPath)
            }
            
        }
      
                                        }
                                        else {
  self.showAlertWithOkButton(message: "Go to settings page to turn on this app" )
            
        }
    }
    }
    
    private func createEmailUrl(to: String, subject: String, body: String) -> URL? {
                let subjectEncoded = subject.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                let bodyEncoded = body.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed)!
                
                let gmailUrl = URL(string: "googlegmail://co?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
                let outlookUrl = URL(string: "ms-outlook://compose?to=\(to)&subject=\(subjectEncoded)")
                let yahooMail = URL(string: "ymail://mail/compose?to=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
                let sparkUrl = URL(string: "readdle-spark://compose?recipient=\(to)&subject=\(subjectEncoded)&body=\(bodyEncoded)")
                let defaultUrl = URL(string: "mailto:\(to)?subject=\(subjectEncoded)&body=\(bodyEncoded)")
                
                if let gmailUrl = gmailUrl, UIApplication.shared.canOpenURL(gmailUrl) {
                    return gmailUrl
                } else if let outlookUrl = outlookUrl, UIApplication.shared.canOpenURL(outlookUrl) {
                    return outlookUrl
                } else if let yahooMail = yahooMail, UIApplication.shared.canOpenURL(yahooMail) {
                    return yahooMail
                } else if let sparkUrl = sparkUrl, UIApplication.shared.canOpenURL(sparkUrl) {
                    return sparkUrl
                }
                
                return defaultUrl
            }
    
   
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        self.addshareDetails(app: "mail")
        controller.dismiss(animated: true, completion: nil)
    }
    
    func topViewController()-> UIViewController{
        var topViewController:UIViewController = UIApplication.shared.keyWindow!.rootViewController!

        while ((topViewController.presentedViewController) != nil) {
            topViewController = topViewController.presentedViewController!;
        }

        return topViewController
    }

    func showShareActivity(msg:String?, image:UIImage?, url:String?, sourceRect:CGRect?){
        var objectsToShare = [AnyObject]()

        if let url = url {
            objectsToShare = [url as AnyObject]
        }

        if let image = image {
            objectsToShare = [image as AnyObject]
        }

        if let msg = msg {
            objectsToShare = [msg as AnyObject]
        }

        let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)
        activityVC.modalPresentationStyle = .popover
        activityVC.popoverPresentationController?.sourceView = topViewController().view
        if let sourceRect = sourceRect {
            activityVC.popoverPresentationController?.sourceRect = sourceRect
        }

        topViewController().present(activityVC, animated: true, completion: nil)
    }
    
    // MARK: WhatsApp
    func sendVideoToWhtsApp() {

        let path = Bundle.main.path(forResource: "YogaTutorial", ofType:"mp4")!
        let fileUrl = NSURL(fileURLWithPath: path)

        let docController = UIDocumentInteractionController(url: fileUrl as URL)
        docController.uti = "net.whatsapp.movie"
        docController.presentPreview(animated: true)
    }
  
   
    func composerDidSucceed(_ controller: TWTRComposerViewController, with tweet: TWTRTweet) {
        self.addshareDetails(app: "twitter")
        print("success")
    }
    func composerDidFail(_ controller: TWTRComposerViewController, withError error: Error) {
         print("Fail")
    }
    
    func sharer(_ sharer: Sharing, didCompleteWithResults results: [String : Any]) {
        self.addshareDetails(app: "facebook")
        print("")
    }
    
    func sharer(_ sharer: Sharing, didFailWithError error: Error) {
         print("")
    }
    
    func sharerDidCancel(_ sharer: Sharing) {
        print("")
    }
   
    //MARK:- Continue Sharing

    func continueSharingImage(indexPath:IndexPath)
        {
            
            
            if indexPath.row == 0 {
                //Facebook
             
              let sharePhoto = SharePhoto()
              sharePhoto.image = self.imgViewSelectedName.image!
              sharePhoto.isUserGenerated = true
              sharePhoto.caption = ""
              let photoContent = SharePhotoContent()
              photoContent.photos = [sharePhoto]
              let showDialog = ShareDialog.init(fromViewController: self, content: photoContent, delegate: self)
              if (showDialog.canShow) {
                  showDialog.show()
              } else {
                  self.showAlertWithOkButton(message: "It looks like you don't have the Facebook mobile app on your device.")
              }
            }
            else if indexPath.row == 1 {
              //Twitter
            /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
                API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
                Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
                // Swift
               if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                          // App must have at least one logged-in user to compose a Tweet
                let composer = TWTRComposerViewController.init(initialText: "Title - \(self.dictOfDetails.title ?? "")\nDescription - \(self.dictOfDetails.descriptionFile ?? "")", image: self.imgViewSelectedName.image!, videoURL: nil)
                  self.present(composer, animated: true, completion: nil)
                      } else {
                          // Log in, and then check again
                      
                          TWTRTwitter.sharedInstance().logIn { session, error in
                              if session != nil { // Log in succeeded
                                let composer = TWTRComposerViewController.init(initialText: "Title - \(self.dictOfDetails.title ?? "")\n Description - \(self.dictOfDetails.descriptionFile ?? "")\n Category - \(self.dictOfDetails.category ?? "")", image: self.imgViewSelectedName.image!, videoURL: nil)
                                  self.present(composer, animated: true, completion: nil)
                              } else {
                                  self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                                 
                              }
                          }
                      }
                
            }
            else if indexPath.row == 2 {
                //Instagram
             /*
              PHPhotoLibrary.shared().performChanges({
                DispatchQueue.main.async {
                    PHAssetChangeRequest.creationRequestForAsset(from: self.imgViewSelectedName.image!)
                }
              }, completionHandler: { [weak self] success, error in
                  if success {
                      let fetchOptions = PHFetchOptions()
                      fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]
                      let fetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
                      if let lastAsset = fetchResult.firstObject {
                          let localIdentifier = lastAsset.localIdentifier
                          
                          let urlFeed = "instagram://library?LocalIdentifier=" + localIdentifier
                          guard let url = URL(string: urlFeed) else {
                              print("Could not open url")
                              return
                          }
                          DispatchQueue.main.async {
                              if UIApplication.shared.canOpenURL(url) {
                                  if #available(iOS 10.0, *) {
                                      UIApplication.shared.open(url, options: [:], completionHandler: { (success) in
                                          print("self?.delegate?.success")
                                      })
                                  } else {
                                      UIApplication.shared.openURL(url)
                                      print("self?.delegate?.success")
                                      
                                  }
                              } else {
                                  DispatchQueue.main.async {
                                  self!.showAlertWithOkButton(message: "It looks like you don't have the Instagram mobile app on your device.")
                                  } }
                          }
                      }
                  } else if let error = error {
                      DispatchQueue.main.async {
                          print(error.localizedDescription)
                          self!.showAlertWithOkButton(message: "Photos access not allowed. Kindly change the settings of your Instagram App.")
                      }
                  }
                  else {
                      print("Could not save the photo")
                  }
              })*/
            }
          
            else if indexPath.row == 3 {
                //SnapChat
                // Copied from Snapchat Documentation

              let snapPhoto = SCSDKSnapPhoto(image: self.imgViewSelectedName.image!)
                      let snap = SCSDKPhotoSnapContent(snapPhoto: snapPhoto)
                      // snap.sticker = /* Optional, add a sticker to the Snap */
              snap.caption = self.dictOfDetails.title ?? ""
                      // snap.attachmentUrl = /* Optional, add a link to the Snap */
//                        ARSLineProgress.show()
              
  //            SCSDKSnapAPI().startSending(snap) { (error: Error?) in
  //                ARSLineProgress.hide()
  //               if let error = error {
  //                   print(error.localizedDescription)
  //               } else {
  //                    self.addshareDetails(app: "snapchat")
  //                   // Successfully shared content to Snapchat!
  //               }
  //           }
  //
              let api = SCSDKSnapAPI(content: snap)
              api.startSending(snap) { error in
  
                  if let error = error {
                      print(error.localizedDescription)
                  } else {
                      // success
  
                  }
              }
//                view.isUserInteractionEnabled = false
//                snapAPI.startSending(snap)
//                { [weak self] (error: Error?) in
//                    ARSLineProgress.hide()
//                    DispatchQueue.main.async {
//                        self?.view.isUserInteractionEnabled = true
//                    }
//                   if let error = error {
//                       print(error.localizedDescription)
//                   } else {
//                    self?.addshareDetails(app: "snapchat")
//                       // Successfully shared content to Snapchat!
//                   }
//                }
              
//                      SCSDKSnapAPI(content: snap).startSnapping()
//                      { (error: Error?) in
//                           ARSLineProgress.hide()
//                          if let error = error {
//                              print(error.localizedDescription)
//                          } else {
//                               self.addshareDetails(app: "snapchat")
//                              // Successfully shared content to Snapchat!
//                          }
//                      }
                     
                     
             
            
            }
            else if indexPath.row == 4 {
               // LinkedIn
            let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                                                           
              self.navigationController?.pushViewController(webVC, animated: true)
               // self.sendToLinkedIn(image: UIImage(named: "img1")!)
            }
            else if indexPath.row == 5 {
                let items = [ self.imgViewSelectedName.image!]
                  let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
              self.present(ac, animated: true)
              
              ac.completionWithItemsHandler = { activity, completed, items, error in
                                           if !completed {
                                               // handle task not completed
                                               return
                                           }
                                           else {
                                                self.addshareDetails(app: "more")
                                           }
                                         
                                       }
                //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
            }
            else if indexPath.row == 6 {
                if MFMailComposeViewController.canSendMail() {
                       let mail = MFMailComposeViewController()
                       mail.mailComposeDelegate = self;
                       mail.setCcRecipients([""])
                  mail.setSubject(self.dictOfDetails.title)
                  mail.setMessageBody(self.dictOfDetails.descriptionFile ?? "", isHTML: false)
                  let imageData: NSData = self.imgViewSelectedName.image!.pngData()! as NSData
                       mail.addAttachmentData(imageData as Data, mimeType: "image/png", fileName: "imageName.png")
                       self.present(mail, animated: true, completion: nil)
                     }
            }
            else {
                let items = [self.imgViewSelectedName.image!]
                      let ac = UIActivityViewController(activityItems: items, applicationActivities: nil)
              self.present(ac, animated: true)
              
              ac.completionWithItemsHandler = { activity, completed, items, error in
                                           if !completed {
                                               // handle task not completed
                                               return
                                           }
                                           else {
                                                self.addshareDetails(app: "more")
                                           }
                                         
                                       }
            }
                      
                  }
    
    
    func continueSharingVideo(indexPath:IndexPath){
        if indexPath.row == 0 {
                      
                  let content: ShareVideoContent = ShareVideoContent()
       
                DispatchQueue.main.async {
                                         let video = ShareVideo()
                    print(self.destinationUrl)
                    video.videoURL =  URL(string:self.destinationUrl)
                                         content.video = video
                                         
                                         let shareDialog = ShareDialog()
                                         shareDialog.shareContent = content
                                         shareDialog.mode = .native
                                         shareDialog.delegate = self
                                         shareDialog.show()
                                     }
          
                     
                      
                
        }
        else if indexPath.row == 1 {
            //Twitter
        /*    API key : Yrbr1QkdEcUjUQ88eZwIdtixx
            API secret key :  HZdMsUnxND0nBk2Trw4XId7gKwj30YOQsNF0gTYKU6Fq2CxTCw
            Bearer token :AAAAAAAAAAAAAAAAAAAAAFaCGwEAAAAAGubrjkk3dgU5qkEe6hPfqVnENTE%3DPGNIZQdvPbOQxzvxV45HAWebsromjc2AtZ24lxjXaazSFEkTJP*/
            // Swift
           if (TWTRTwitter.sharedInstance().sessionStore.hasLoggedInUsers()) {
                      // App must have at least one logged-in user to compose a Tweet
            let composer = TWTRComposerViewController.init(initialText: self.dictOfDetails.title, image: nil, videoURL:URL(string: self.dictOfDetails.path!))
            self.present(composer, animated: true, completion: nil)
                  } else {
                      // Log in, and then check again
                  
                      TWTRTwitter.sharedInstance().logIn { session, error in
                          if session != nil { // Log in succeeded
                            let composer = TWTRComposerViewController.init(initialText: self.dictOfDetails.title, image: nil, videoURL:URL(string: self.dictOfDetails.path!))
                              self.present(composer, animated: true, completion: nil)
                          } else {
                              self.showAlertWithOkButton(message: "No Twitter Accounts Available,You must log in before presenting a composer.")
                             
                          }
                      }
                  }
            
        }
        else if indexPath.row == 2 {
            //instagram
                   DispatchQueue.main.async {
                    let urlFeed = "instagram://library?LocalIdentifier=" + self.destinationUrl
                             guard let url = URL(string: urlFeed) else {
                                 print("Could not open url")
                                 return
                             }
                             DispatchQueue.main.async {
                                 if UIApplication.shared.canOpenURL(url) {
                                     if #available(iOS 10.0, *) {
                                         UIApplication.shared.open(url, options: [:]
             , completionHandler: { (success) in
                                             print("self?.delegate?.success")
                                         })
                                     } else {
                                         UIApplication.shared.openURL(url)
                                         print("self?.delegate?.success")
                                     }
                                 } else {
                                     print("Instagram not found")
                                 }
                             }
                         }
        }
            
 
        else if indexPath.row == 3 {
            //SnapChat
            // Copied from Snapchat Documentation
             
            let video = SCSDKSnapVideo(videoUrl: URL(string:self.dictOfDetails.path!)!)
            let videoContent = SCSDKVideoSnapContent(snapVideo: video)
               
          
                  SVProgressHUD.show()
                 
                  SCSDKSnapAPI(content: videoContent).startSnapping() { (error: Error?) in
                      SVProgressHUD.dismiss()
                      if let error = error {
                          print(error.localizedDescription)
                      } else {
                         self.addshareDetails(app: "snapchat")
                          // Successfully shared content to Snapchat!
                      }
                  }
                 
           
        
        }
        else if indexPath.row == 4 {
            //LinkedI
          let webVC = kStoryboard.instantiateViewController(withIdentifier:"MEWebViewVc") as! MEWebViewVc
                                                      
                    self.navigationController?.pushViewController(webVC, animated: true)
           // self.sendToLinkedIn(image: UIImage(named: "img1")!)
        }
        else if indexPath.row == 5 {
       let urlData = NSData(contentsOf: NSURL(string:self.dictOfDetails.path!)! as URL)
  SVProgressHUD.show()
                  if ((urlData) != nil){

                      print(urlData)


                     let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
                      let docDirectory = paths[0]
                      let filePath = "\(docDirectory)/tmpVideo.mp4"
                     urlData?.write(toFile: filePath, atomically: true)
                      // file saved

                      let videoLink = NSURL(fileURLWithPath: filePath)


                      let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
                      let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

                      activityVC.setValue("Video", forKey: "subject")
               

                      SVProgressHUD.dismiss()
                    self.present(activityVC, animated: true, completion: nil)
                    activityVC.completionWithItemsHandler = { activity, completed, items, error in
                        if !completed {
                            // handle task not completed
                            return
                        }
                        else {
                             self.addshareDetails(app: "more")
                        }
                      
                    }
                  
                  }
            //self.sendImageTowhtsApp(image:  UIImage(named: "img1")!)
        }
        else if indexPath.row == 6 {
            print("Title - \(self.dictOfDetails.title)\n Description - \(self.dictOfDetails.description)\n Category - \(self.dictOfDetails.category)")
           if MFMailComposeViewController.canSendMail() {
                   let mail = MFMailComposeViewController()
                   mail.mailComposeDelegate = self;
                   mail.setCcRecipients([""])
            mail.setSubject(self.dictOfDetails.title)
            mail.setMessageBody(self.dictOfDetails.descriptionFile ?? "", isHTML: false)
               do {
                let fileData: NSData = try Data(contentsOf: URL(string: self.dictOfDetails.path!)!) as NSData
                mail.addAttachmentData(fileData as Data, mimeType: "mp4", fileName: "myfile.mp4")

                   self.present(mail, animated: true, completion: nil)
                 
            }
            catch {
                print("error")
            }
            }
        }
        else {
            let urlData = NSData(contentsOf: NSURL(string:self.dictOfDetails.path!)! as URL)

         if ((urlData) != nil){
  SVProgressHUD.show()
             print(urlData)


            let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)
             let docDirectory = paths[0]
             let filePath = "\(docDirectory)/tmpVideo.mp4"
            urlData?.write(toFile: filePath, atomically: true)
             // file saved

             let videoLink = NSURL(fileURLWithPath: filePath)


             let objectsToShare = [videoLink] //comment!, imageData!, myWebsite!]
             let activityVC = UIActivityViewController(activityItems: objectsToShare, applicationActivities: nil)

             activityVC.setValue("Video", forKey: "subject")

  SVProgressHUD.dismiss()
           

            self.present(activityVC, animated: true, completion: nil)
            activityVC.completionWithItemsHandler = { activity, completed, items, error in
                          if !completed {
                              // handle task not completed
                              return
                          }
                          else {
                               self.addshareDetails(app: "more")
                          }
            }
        
         }
        }
            
         //
         }
    
    
    
    // MARK: Show Share Popup
    func showSharePopup() {
           let window = UIApplication.shared.delegate?.window!
          viewBackground = UIView(frame: CGRect(x: 0, y: 0, width: window!.frame.size.width, height:  window!.frame.size.height))
                   viewBackground.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.7)
                   window!.addSubview(viewBackground)
         
           self.viewShare.frame = CGRect(x: 15, y: ((window?.frame.height)! - 94) / 2, width: (window?.frame.width)! - 30, height: 94)
            window!.addSubview(self.viewBackground)
         self.viewBackground.addSubview(self.viewShare)
            self.viewBackground.bringSubviewToFront(self.viewShare)
           self.viewBackground.transform = CGAffineTransform(scaleX: 0.2, y: 0.2)
           
           UIView.animate(withDuration: 0.33, animations: {
               self.viewBackground.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
           })
       }
       func dismissPopUpView(){
                 UIView.animate(withDuration: 0.33, animations: {
                   
                 }, completion: { (completed) in
                     
                 })
                 UIView.animate(withDuration: 0.33, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 10, options: UIView.AnimationOptions(rawValue: 0), animations: {
                     
                 }, completion: { (completed) in
                     self.viewBackground.removeFromSuperview()
                   
                 })
             }
   
    //MARK:- TextField Delgate
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        self.activeTextField.tag = textField.tag
//        print(textField.tag)
//        print(self.activeTextField.tag)
//        self.activeTextField = textField
//        let toolbarDone = UIToolbar.init()
//        toolbarDone.sizeToFit()
//        let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done,target: self, action: #selector(self.doneButtonClicked))
//        barBtnDone.tintColor = kAppThemeYellowTop
//        let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//        toolbarDone.items = [flexSpace,barBtnDone]
//        textField.inputAccessoryView = toolbarDone
//
//        let formatter = DateFormatter()
//        if textField.tag == 10 {
//            datePicker.datePickerMode = .date
//            datePicker.minimumDate = Date()
//            if #available(iOS 13.4, *) {
//                datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
//            } else {
//                // Fallback on earlier versions
//            }
//            textField.inputView = datePicker
//
//        }
//
//        else if textField.tag == 20 {
//            datePicker.datePickerMode = .time
//            datePicker.minimumDate = Date()
//            if #available(iOS 13.4, *) {
//                datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
//            } else {
//                // Fallback on earlier versions
//            }
//            textField.inputView = datePicker
//        }
//        textField.becomeFirstResponder()
//
//    }
          func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
             
              return true
          }
             func textFieldDidEndEditing(_ textField: UITextField) {
                if textField.tag == 1 {
                    self.txtVideoTitle = textField.text!
                }
                else if textField.tag == 2{
                      self.txtVideoCategory = textField.text!
                }
                else if textField.tag == 3{
                      self.txtViewDescription = textField.text!
                 }
                else if textField.tag == 10{
                     self.notificationDate = textField.text!
                }
                else if textField.tag == 20{
                     self.notificationTime = textField.text!
                }
                 textField.resignFirstResponder()
             }
      
       func textFieldShouldReturn(_ textField: UITextField) -> Bool {
            textField.resignFirstResponder()
           return true
       }
    //MARK:- TextView Delgate
//    func textViewDidBeginEditing(_ textView: UITextView) {
//        let toolbarDone = UIToolbar.init()
//                  toolbarDone.sizeToFit()
//                  let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done,target: self, action: #selector(self.doneButtonClicked))
//                  barBtnDone.tintColor = kAppThemeYellowTop
//                  let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//                  toolbarDone.items = [flexSpace,barBtnDone]
//        textView.inputAccessoryView = toolbarDone
//        textView.becomeFirstResponder()
//    }
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.tag == 3 {
            self.txtViewDescription = textView.text!
            textView.resignFirstResponder()
        }
    }
    //MARK:- Done button action keyboard
//          @objc func doneButtonClicked() {
//
//              self.view.endEditing(true)
//          }
    func getLastestList() {
        ServiceManager.sharedInstance.post(withServiceName: "filedisplay", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)","file_type":"Video","offset":0,"limit":10] as [String : Any], withHud: true) { (success, result, error) in
                  
                  if success == true {
                      if (result as! NSDictionary)["message"] as! String == "Success" {
                          self.arrayOfimagesOrVedio.removeAll()
                          let listDetailsDict = result as! NSDictionary
                        
                          for items in listDetailsDict.value(forKey: "user_details") as! NSArray {
                              let obj  = listOfFiles().initWithDictionary(userDictionary: items as! NSDictionary)
                              self.arrayOfimagesOrVedio.append(obj)
                          }
                        self.tableView.reloadData()

                      }
                      else {
                          //  self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                      }
                  }
                  else {
                        self.showAlertWithOkButton(message: "Please try again")
                  }
              }}
    
    func getVideoList() {
        ServiceManager.sharedInstance.post(withServiceName: "filelisting", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { (success, result, error) in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let listDetailsDict = result as!  NSDictionary
                    let videoDetails = listDetailsDict.value(forKey: "video") as! NSArray
                    self.videoUrlArray.removeAll()
                    for video in videoDetails{
                        let path = (video as! NSDictionary).value(forKey: "path") as! String
                        self.videoUrlArray.append(path)
                        if let pathURL = URL(string: path) {
                            let image = self.thumbnailForVideoAtURL(url: pathURL as NSURL)
                            self.videoThumbDict[path] = image
                        }
                    }
                    
                    DispatchQueue.main.async {
                        self.tableView.reloadData()
                    }
                }
                else {
                    DispatchQueue.main.async {
                    self.tableView.reloadData()
                }
                }
            }
            else {
                self.showAlertWithOkButton(message: "Please try again")
            }
        }
        
    }
    
       func editVideoDetails(){
        self.view.endEditing(true)
           if !self.txtVideoTitle.isEmpty && !self.txtVideoCategory.isEmpty && !self.txtViewDescription.isEmpty  {
               ServiceManager.sharedInstance.post(withServiceName: "update_content", andParameters: ["fileid":self.dictOfDetails.fileid!,"title":self.txtVideoTitle,"category":self.txtVideoCategory,"description":self.txtViewDescription], withHud: true) { (success, result, error) in
                         if success == true {
                             if (result as! NSDictionary)["message"] as! String == "Success" {
                                self.showAlertWithOkButton(message: "Updated Successfully")
                             }
                             else {
                                   self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                             }
                         }
                         else {
                               self.showAlertWithOkButton(message: "Please try again")
                         }
                     }
             }
           else {
              self.showAlertWithOkButton(message: "Please fill the fields")
             }
               
       }
    
       func deleteVideoDetails(){
        
               ServiceManager.sharedInstance.post(withServiceName: "delete_file", andParameters: ["fileid":self.dictOfDetails.fileid!], withHud: true) { (success, result, error) in
                         if success == true {
                             if (result as! NSDictionary)["message"] as! String == "Success" {
                                 self.navigationController?.popViewController(animated: true)
                                // self.showAlertWithOkButton(message: "Updated Sucessfully" )
                             }
                             else {
                                   self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                             }
                         }
                         else {
                               self.showAlertWithOkButton(message: "Please try again")
                         }
                     }
       }
    func setNotificationDetails(){
           if !self.notificationDate.isEmpty && !self.notificationTime.isEmpty  {
            if self.arrayOfSelectedApp.count > 0 {
                ServiceManager.sharedInstance.post(withServiceName: "socialsharesave", andParameters: ["fileid":self.dictOfDetails.fileid!,"date":self.notificationDate,"time":self.notificationTime,"socialmedia":self.arrayOfSelectedApp.joined(separator: ","),"id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { (success, result, error) in
                           if success == true {
                               if (result as! NSDictionary)["message"] as! String == "Success" {
                                self.alertWithOneAction(title: "", message: "Successfully Set", actionTitle: "Ok") { action in
                                    let shareVC = kStoryboard.instantiateViewController(withIdentifier:"MESharedFilesTVC") as! MESharedFilesTVC
                                     self.navigationController?.pushViewController(shareVC, animated: true)
                                }
                               }
                               else {
                                     self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                               }
                           }
                           else {
                                 self.showAlertWithOkButton(message: "Please try again")
                           }
                       }
            }
            else {
                self.showAlertWithOkButton(message: "Please Select Any App")
            }
            }
                      else {
                         self.showAlertWithOkButton(message: "Please fill the fields")
                        }
         }
    
    func addshareDetails(app:String){
          
        ServiceManager.sharedInstance.post(withServiceName: "addshare", andParameters: ["user_id":self.dictOfDetails.fileid!,"fileid":"\(self.dictOfDetails.fileid!)","socialapp":app], withHud: true) { (success, result, error) in
                           if success == true {
                               if (result as! NSDictionary)["message"] as! String == "Success" {
                                  
                               }
                               else {
                                  //   self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                               }
                           }
                           else {
                               //  self.showAlertWithOkButton(message: "Please try again")
                           }
                       }
            
                 
         }
    func downloadVideoLinkAndCreateAsset(completion: @escaping (String) -> Void) {

        // use guard to make sure you have a valid url
      
         SVProgressHUD.show()

       DispatchQueue.global(qos: .background).async {
           if let url = URL(string: self.dictOfDetails.path!),
               let urlData = NSData(contentsOf: url) {
               let documentsPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0];
               let filePath="\(documentsPath)/tempFile.mp4"
               DispatchQueue.main.async {
                   urlData.write(toFile: filePath, atomically: true)
                 var videoAssetPlaceholder:PHObjectPlaceholder!
                   PHPhotoLibrary.shared().performChanges({
                       let request =  PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: URL(fileURLWithPath: filePath))
                     videoAssetPlaceholder = request!.placeholderForCreatedAsset
                   }) { completed, error in
                       SVProgressHUD.dismiss()
                       if completed {
                           print("Video is saved!")
                        let localID = NSString(string: videoAssetPlaceholder.localIdentifier)
                        let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: NSString.CompareOptions.regularExpression, range: NSRange())
                        let ext = "mp4"
                        let assetURLStr =
                        "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                        
                        completion(assetURLStr)
                       }
                   }
               }
           }
       }
    }
    func createAssetURL(url: URL, completion: @escaping (String) -> Void) {
           let photoLibrary = PHPhotoLibrary.shared()
           var videoAssetPlaceholder:PHObjectPlaceholder!
           photoLibrary.performChanges({
               let request = PHAssetChangeRequest.creationRequestForAssetFromVideo(atFileURL: url)
               videoAssetPlaceholder = request!.placeholderForCreatedAsset
           },completionHandler: { success, error in
               if success {
                   let localID = NSString(string: videoAssetPlaceholder.localIdentifier)
                   let assetID = localID.replacingOccurrences(of: "/.*", with: "", options: NSString.CompareOptions.regularExpression, range: NSRange())
                   let ext = "mp4"
                   let assetURLStr =
                   "assets-library://asset/asset.\(ext)?id=\(assetID)&ext=\(ext)"
                   
                   completion(assetURLStr)
               }
           })
       }
    
    //MARK: - To generate thumbnail from URL
    func thumbnailForVideoAtURL(url: NSURL) -> UIImage? {
        let asset = AVAsset(url: url as URL)
        let assetImageGenerator = AVAssetImageGenerator(asset: asset)
        assetImageGenerator.appliesPreferredTrackTransform = true

        var time = asset.duration
        time.value = min(time.value, 2)

        do {
            let imageRef = try assetImageGenerator.copyCGImage(at: time, actualTime: nil)
            print("Thumbail generated")
            return UIImage(cgImage: imageRef)
        } catch {
            print("error in generating thumbnail: \(error)")
            return nil
        }
    }
    //MARK:- Show Alert With Ok Button
            func showAlertWithOkButton(message:String){
                AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                    
                }
            }
    
    func alertWithOneAction(title: String!, message: String!, actionTitle: String!, completion: ((UIAlertAction)-> Void)?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .default, handler: completion)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
      

    @IBAction func closePopUp(_ sender: Any) {
        alertPopUpView.removeFromSuperview()
    }
    
}

extension MEDeatilsPageTVC{
    func showAlertPopUp(completion:()->Void){
        self.view.center = alertPopUpView.center
        self.view.frame = alertPopUpView.frame
        self.view.addSubview(alertPopUpView)
        guard let confettiImageView = UIImageView.fromGif(frame: view.frame, resourceName: "popup") else {completion()
            return }
        view.addSubview(confettiImageView)
        confettiImageView.startAnimating()
        popUpImageView = confettiImageView
        completion()
    }
}
