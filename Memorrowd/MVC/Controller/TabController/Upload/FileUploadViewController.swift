//
//  FileUploadViewController.swift
//  Memorrowd
//
//  Created by Xavier Joseph on 11/01/22.
//  Copyright © 2022 Srishti Innovative. All rights reserved.
//

import UIKit
import Photos
import HSGoogleDrivePicker
import MobileCoreServices
import AVFoundation
import SVProgressHUD
import YPImagePicker
import AVKit

class FileUploadViewController: UIViewController, GoogleDriveViewControllerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate{

    @IBOutlet weak var galleryButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var selectedImageView: UIImageView!
    @IBOutlet weak var galleryCollectionView: UICollectionView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var googleDriveButton: UIButton!
    @IBOutlet weak var dropBoxButton: UIButton!
    
    var images = [UIImage]()
    var imagePicker                = UIImagePickerController()
    var arrayOfImageData = [Data]()
    var arrayOfvideoData = [Data]()
    var videoThumbNail = UIImage()
    var fileSize : Int?
    let flowLayout = MyFlowLayout()
    var selectedItems = [YPMediaItem]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.setUpNavigationBar()
        self.customUI()
        galleryCollectionView.collectionViewLayout = flowLayout
        galleryButton.setTitle("", for: .normal)
        backButton.setTitle("", for: .normal)
        googleDriveButton.setTitle("", for: .normal)
        dropBoxButton.setTitle("", for: .normal)
        cameraButton.setTitle("", for: .normal)
        getPhotos()
//        showPicker()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    
    //MARK:- CustomUI
    func customUI() {
        self.googleDriveButton.setBorderAndCornerRadius(borderColor: UIColor.gray.withAlphaComponent(0.5), borderWidth: 1.0, cornerRadius: 5.0)
        self.dropBoxButton.setBorderAndCornerRadius(borderColor: UIColor.gray.withAlphaComponent(0.5), borderWidth: 1.0, cornerRadius: 5.0)
        self.googleDriveButton.backgroundColor = .white
        self.dropBoxButton.backgroundColor = .white
        self.googleDriveButton.layer.cornerRadius = 15
        self.dropBoxButton.layer.cornerRadius = 15
        
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
    }
    
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = kAppThemeYellowTop
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.title = "Upload File"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
        
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
        //self.navigationController.navigationBar.titleTextAttributes = [NSFontAttributeName: UIFont(name: "CaviarDreams", size: 20)!]
        //let logoButton = UIBarButtonItem(image: Images.kLogoSmallWhite, style: .plain, target: self, action: #selector(logoTouched))
        // self.navigationItem.leftBarButtonItem = logoButton
        // let rightMenuButton = UIBarButtonItem(image: Images.kMenuIcon, style: .plain, target: self, action: #selector(rightMenuTouched))
        //self.navigationItem.rightBarButtonItem = rightMenuButton
    }
    
    @IBAction func galleryButtonSelected(_ sender: UIButton) {
        
//        self.imagePicker = UIImagePickerController()
//        self.imagePicker.delegate = self
//        self.imagePicker.allowsEditing = true
//        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//
//        let actionGallery = UIAlertAction(title: "Pick from gallery", style: .default) { (action) in
//            self.imagePicker.sourceType = .savedPhotosAlbum
//            self.imagePicker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
//            self.present(self.imagePicker, animated: true, completion: nil)
//        }
//        alertController.addAction(actionGallery)
//        self.present(alertController, animated: true) { }
//        let actionCancel = UIAlertAction(title:"cancel", style: .destructive) { (action) in
//        }
//        alertController.addAction(actionCancel)
    }
    
    @IBAction func uploadButtonSelected(_ sender: UIButton) {
    }
    
    @IBAction func cameraButtonSelected(_ sender: UIButton) {
        
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        
//        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
//        let actionTakePhoto = UIAlertAction(title: "Take a photo or Video", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
            {
                self.imagePicker.sourceType = .camera
                self.imagePicker.mediaTypes = [kUTTypeMovie as String,kUTTypeImage as String]
                
                self.present(self.imagePicker,animated : true,completion : nil)
            }
            else
            {
                let alert = UIAlertController(title: "oops", message:"Camera not available", preferredStyle: .alert)
                let actionOk = UIAlertAction(title:"OK", style: .default) { (action) in
                }
                alert.addAction(actionOk)
                self.present(alert, animated: true){
                    
                }
            }
//        }
//        alertController.addAction(actionTakePhoto)
//        self.present(alertController, animated: true) { }
//        let actionCancel = UIAlertAction(title:"cancel", style: .destructive) { (action) in
//        }
//        alertController.addAction(actionCancel)
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton) {
    }
    
    @IBAction func googleDriveButtonPressed(_ sender: UIButton) {
        
        let picker = HSDrivePicker()
        
        picker.pick(from: self) {
            (manager, file) in
            
            guard let fileName = file?.name else {
                print("No file picked")
                return
            }
            
            
            //Download the file
            let destURL =  URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
            let destinationPath = destURL.path
            manager?.downloadFile(file, toPath: destinationPath, withCompletionHandler: { error in
                self.arrayOfvideoData.removeAll()
                self.arrayOfImageData.removeAll()
                SVProgressHUD.show()
                if error != nil {
                    SVProgressHUD.dismiss()
                    print("Error downloading : \(error?.localizedDescription ?? "")")
                } else {
                    if file!.mimeType!  == "image/png" || file!.mimeType! == "image/jpg" || file!.mimeType! == "image/jpeg" {
                        let tempImage = UIImage(contentsOfFile: destinationPath)
                        self.fileSize = Int((tempImage?.jpegData(compressionQuality: 0.6))!.count)
                        print("FileSize of Image from GDrive : \(self.fileSize)")
                        self.arrayOfImageData.append((tempImage?.jpegData(compressionQuality: 0.6)!)! )
                    }
                    else if file!.mimeType! == "video/mp4" {
                        var videoAsData = NSData()
                        
  //                      videoAsData.write(toFile: destinationPath, atomically: true)
                        let url = destURL
                        print("destURL is \(destURL)")
                          do {
                            videoAsData = try Data(contentsOf: url) as NSData
                             } catch let error {
                               print(error)
                             }
                        
                        self.fileSize = Int((videoAsData as Data).count)
                        self.arrayOfvideoData.append(videoAsData as Data)
                        print("Video As Data : \(videoAsData)")
                        print(self.arrayOfvideoData)
 //                       self.videoThumbImageView.image = self.thumbnailForVideoAtURL(url: destURL as NSURL)
//                        let videoData = NSData(contentsOfFile: destinationPath) as Data?
//                        self.fileSize = Int(videoData!.count)
                        print("FileSize of video from GDrive : \((videoAsData as Data).count)")
    
                    }
                    self.uploadFile()
                    print("Success downloading to : \(destinationPath)")
                }
                
            })
        }
    }
    
    @IBAction func dropBoxButtonPressed(_ sender: UIButton) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MEDropboxFilesListVC") as! MEDropboxFilesListVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func getFile(data: NSData, mimeType: String) {
        self.arrayOfvideoData.removeAll()
        self.arrayOfImageData.removeAll()
        SVProgressHUD.show()
        if mimeType == "image" {
            self.arrayOfImageData.append(data as Data)
            fileSize = Int((data as Data).count)
            print("Filesize of Dropbox Image: \(fileSize)")
            self.uploadFile()
        }
        else if mimeType == "video"{
            self.arrayOfvideoData.append(data as Data)
            fileSize = Int((data as Data).count)
            print("Filesize of Dropbox Video: \(fileSize)")
            self.uploadFile()//http://stackoverflow.com/questions/30717302/how-to-play-movie-file-from-the-web-with-nsdata
        }
        else{
            print(mimeType)
        }
    }
    
    @objc func actionPushNotificationInActiveState(_ notification: Notification) {
        
        let message = "Its time to post your post."
        
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
        let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
            self.navigationController?.present(listVC, animated: true, completion: {
                
            })
        })
        let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(buttonView)
        alertController.addAction(buttonCancel)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    
    @objc func actionPushNotificationBGState(_ notification: Notification) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
        listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
        self.navigationController?.present(listVC, animated: true, completion: {
            
        })
    }
    
    //MARK:- IMAGEPICKER DELEGATE METHOD
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.editedImage] != nil ? (info[.editedImage] as! UIImage) : nil
        
        
        let url = info[.mediaURL] != nil ? (info[.mediaURL] as! URL) : nil
        
        
        self.pickeedDataFormUImagePicker(url: url, image: image)
        dismiss(animated: true, completion: nil)
        //        //uncomment this if you want to save the video file to the media library
        //        if UIVideoAtPathIsCompatibleWithSavedPhotosAlbum(url.path) {
        //            UISaveVideoAtPathToSavedPhotosAlbum(url.path, self, nil, nil)
        //        }
        
    }
    
    //MARK:- Image Picker Cancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
    func pickeedDataFormUImagePicker(url:URL?,image:UIImage?) {
        self.arrayOfvideoData.removeAll()
        self.arrayOfImageData.removeAll()
        SVProgressHUD.show()
        if url != nil {
            print("url is \(url)")
            let path = url?.path
            do {
                let resources = try url!.resourceValues(forKeys:[.fileSizeKey])
                fileSize = Int(resources.fileSize!)
                print ("Size of video file: \(fileSize)")
            } catch {
                print("Error in getting the video file size: \(error)")
            }
            let vData = (try? Data(contentsOf: url!))
            self.arrayOfvideoData.append(vData!)
        } else {
            let imgData = image?.jpegData(compressionQuality: 0.8)
            self.arrayOfImageData.append(imgData!)
            print("Size of Image(bytes):\(imgData?.count ?? 0)")
            fileSize = Int(imgData!.count)
        }
        self.uploadFile()
    }
    
    fileprivate func getPhotos() {

        let manager = PHImageManager.default()
        let requestOptions = PHImageRequestOptions()
        requestOptions.isSynchronous = false
        requestOptions.deliveryMode = .highQualityFormat
        // .highQualityFormat will return better quality photos
        let fetchOptions = PHFetchOptions()
        fetchOptions.sortDescriptors = [NSSortDescriptor(key: "creationDate", ascending: false)]

        let results: PHFetchResult = PHAsset.fetchAssets(with: .image, options: fetchOptions)
        if results.count > 0 {
            for i in 0..<results.count {
                let asset = results.object(at: i)
                let size = CGSize(width: 700, height: 700)
                manager.requestImage(for: asset, targetSize: size, contentMode: .aspectFill, options: requestOptions) { (image, _) in
                    if let image = image {
                        self.images.append(image)
                        self.selectedImageView.image = self.images[0]
                        self.galleryCollectionView.reloadData()
                    } else {
                        print("error asset to image")
                    }
                }
            }
        } else {
            print("no photos to display")
        }

    }
    
    func showPicker() {
        var config = YPImagePickerConfiguration()
        config.library.mediaType = .photoAndVideo
        config.library.itemOverlayType = .grid
        config.shouldSaveNewPicturesToAlbum = false
        config.video.compression = AVAssetExportPresetPassthrough
        config.startOnScreen = .library
        config.screens = [.library]
        config.library.minWidthForItem = UIScreen.main.bounds.width * 0.8
        config.video.libraryTimeLimit = 500.0
        config.showsCrop = .rectangle(ratio: (16/9))
        config.library.maxNumberOfItems = 5
        config.gallery.hidesRemoveButton = false
        let picker = YPImagePicker(configuration: config)
        picker.imagePickerDelegate = self
        picker.didFinishPicking { [weak picker] items, _ in
             self.selectedItems = items
             self.selectedImageView.image = items.singlePhoto?.image
            picker?.dismiss(animated: true, completion: nil)
         }

         picker.didFinishPicking { [weak picker] items, cancelled in
             if cancelled { picker?.dismiss(animated: true, completion: nil); return }
        
            self.selectedItems = items
            self.selectedImageView.image = items.singleVideo?.thumbnail
        
            let assetURL = items.singleVideo!.url
            let playerVC = AVPlayerViewController()
            let player = AVPlayer(playerItem: AVPlayerItem(url:assetURL))
            playerVC.player = player
        
             picker?.dismiss(animated: true, completion: { [weak self] in
                self?.present(playerVC, animated: true, completion: nil)
                print("😀 \(String(describing: self?.resolutionForLocalVideo(url: assetURL)!))")
            })
        }
        present(picker, animated: true, completion: nil)
    }
    
    func uploadFile() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         let dateString = dateFormatter.string(from: date)
            print("DateString:\(dateString)")
           
        SVProgressHUD.show()
        let params : NSDictionary = ["user_id":"\(MEUserDetails.currentUser.id!)","upload_date":dateString]
        print(params)
        getPlanDetails {success in
            SVProgressHUD.dismiss()
            if success{
                
                ServiceManager.sharedInstance.parseLinkUsingPostMethod("upload_files", with: ["id":"\(MEUserDetails.currentUser.id!)","upload_date":dateString],arrayOfImages:self.arrayOfImageData,arrayOfVideos:self.arrayOfvideoData, withHud: true) { (success, result, error) in
                if success == true {
                    
                    self.alertWithOneAction(title: "", message: "File uploaded successfully.", actionTitle: "OK", completion: nil)
                    SVProgressHUD.dismiss()
                    ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: false) { success, result, error in
                        if success == true {
                            if (result as! NSDictionary)["message"] as! String == "Success" {
                                let planDetailsDict = result!["packages"]! as! NSDictionary
                                let newLeftSpace = planDetailsDict["left_space"] as AnyObject
                                
                                print("Space Left after new upload: \(newLeftSpace)")
                            }
                        } else {
                            print("Failed to update server")
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.alertWithOneAction(title: "", message: "File Uploading Failed.", actionTitle: "OK", completion: nil)
                    }
                }
             //   ARSLineProgress.hide()
            }
            }
        }
//        ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { success, result, error in
//            if success == true {
//                if (result as! NSDictionary)["message"] as! String == "Success" {
//                    let planDetailsDict = result!["packages"]! as! NSDictionary
//                    let leftSpace = planDetailsDict["left_space"] as AnyObject
//
//                    print("Space Left : \(leftSpace)")
//                    if (leftSpace as! Int) > self.fileSize! {
//
//                        DispatchQueue.main.async {
//                            ARSLineProgress.show()
//
//                        ars_dispatchAfter(0.1) {
//
//
//                        ServiceManager.sharedInstance.parseLinkUsingPostMethod("upload_files", with: ["id":"\(MEUserDetails.currentUser.id!)","upload_date":dateString],arrayOfImages:self.arrayOfImageData,arrayOfVideos:self.arrayOfvideoData) { (success, result, error) in
//                            if success == true {
//                                self.alertWithOneAction(title: "", message: "File uploaded successfully.", actionTitle: "OK", completion: nil)
//
//                                ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { success, result, error in
//                                    if success == true {
//                                        if (result as! NSDictionary)["message"] as! String == "Success" {
//                                            let planDetailsDict = result!["packages"]! as! NSDictionary
//                                            let newLeftSpace = planDetailsDict["left_space"] as AnyObject
//
//                                            print("Space Left after new upload: \(newLeftSpace)")
//                                        }
//                                    } else {
//                                        print("Failed to update server")
//                                    }
//                                }
//                            } else {
//                                DispatchQueue.main.async {
//                                    self.alertWithOneAction(title: "", message: "File Uploading Failed.", actionTitle: "OK", completion: nil)
//                                }
//                            }
//                         //   ARSLineProgress.hide()
//                        }
//
//                        }
//
//                        }
//                    } else {
//                        DispatchQueue.main.async {
//                            self.alertWithOneAction(title: "Not enough space left!", message: "Please upgrade your plan", actionTitle: "OK", completion: nil)
//                        }
//                    }
//                }
//            } else {
//                DispatchQueue.main.async {
//                    self.alertWithOneAction(title: "", message: "Please try again.", actionTitle: "OK", completion: nil)
//                }
//            }
//
//        }
    }
    
    func getPlanDetails(completion:@escaping(Bool)->Void){
        ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: false) { success, result, error in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let planDetailsDict = result!["packages"]! as! NSDictionary
                    let leftSpace = planDetailsDict["left_space"] as AnyObject
                    print(self.fileSize!)
                    print("Space Left : \(leftSpace)")
                    if (leftSpace as! Int) > self.fileSize! {
                        completion(true)
                    } else {
                        completion(true)
                        DispatchQueue.main.async {
//                            self.alertWithOneAction(title: "Not enough space left!", message: "Please upgrade your plan", actionTitle: "OK", completion: nil)
                        }
                    }
                }
            } else {
                completion(false)
                DispatchQueue.main.async {
                    SVProgressHUD.dismiss()
                    self.alertWithOneAction(title: "", message: "Please try again.", actionTitle: "OK", completion: nil)
                }
            }
           
        }
    }
    
    //MARK:- Show Alert With One Action
    func alertWithOneAction(title: String!, message: String!, actionTitle: String!, completion: ((UIAlertAction)-> Void)?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .default, handler: completion)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
}


extension FileUploadViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print("images:\(images.count)")
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GalleryCell", for: indexPath) as! GalleryImageCell
        cell.galleryImageView.image = images[indexPath.row]
        return cell
    }
    
    func collectionView(collectinView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1
    }

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: UIScreen.main.bounds.width/6 - 2, height: UIScreen.main.bounds.width/6 - 2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
}

extension FileUploadViewController {
    /* Gives a resolution for the video by URL */
    func resolutionForLocalVideo(url: URL) -> CGSize? {
        guard let track = AVURLAsset(url: url).tracks(withMediaType: AVMediaType.video).first else { return nil }
        let size = track.naturalSize.applying(track.preferredTransform)
        return CGSize(width: abs(size.width), height: abs(size.height))
    }
}

// YPImagePickerDelegate
extension FileUploadViewController: YPImagePickerDelegate {
    func imagePickerHasNoItemsInLibrary(_ picker: YPImagePicker) {
        // PHPhotoLibrary.shared().presentLimitedLibraryPicker(from: self)
    }

    func shouldAddToSelection(indexPath: IndexPath, numSelections: Int) -> Bool {
        return true // indexPath.row != 2
    }
}
