//
//  UploadMediaVC.swift
//  Memorrowd
//
//  Created by John Srishti on 14/01/22.
//  Copyright © 2022 Srishti Innovative. All rights reserved.
//

import UIKit
import Photos
import SVProgressHUD
import HSGoogleDrivePicker


class UploadMediaVC: UIViewController {
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var previewImage: UIImageView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var driveButton: UIButton!
    @IBOutlet weak var dropBoxButton: UIButton!
    
    var assetCollection: PHAssetCollection!
    var photosAsset: PHFetchResult<AnyObject>!
    var assetThumbnailSize: CGSize!
    private var imageAssets = [PHAsset]()
    private var thumbNails = [UIImage]()
    var selectedAsset = PHAsset()
    
    var imagePicker   = UIImagePickerController()
    var arrayOfImageData = [Data]()
    var arrayOfvideoData = [Data]()
    var fileSize : Int?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uploadButton.layer.cornerRadius = 8
        dropBoxButton.layer.cornerRadius = dropBoxButton.frame.height/2
        driveButton.layer.cornerRadius = driveButton.frame.height/2
        collectionView.delegate = self
        collectionView.dataSource = self
        let layout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
              layout.minimumInteritemSpacing = 0
              layout.minimumLineSpacing = 0
              collectionView!.collectionViewLayout = layout
        getAllPhotos()

    }

    
    private func getAllPhotos(){
        Loader.Show()
        PHPhotoLibrary.requestAuthorization { [self] status in
            if status == .authorized{
                let options = PHFetchOptions()
                
                let assets = PHAsset.fetchAssets(with: options)
             
                
                assets.enumerateObjects { [self] asset, _, _ in
                    getImageFromAsset(asset: asset, size: 100, thumbnail: true) { image in
                        if let safeImage = image{
                            thumbNails.append(safeImage)
                            self.imageAssets.append(asset)
                        }
                       
                    }
                   
                }
                if imageAssets.count > 0{
                    selectedAsset = imageAssets.first!
                    getImageFromAsset(asset: selectedAsset, size: 720, thumbnail: false) { image in
                        DispatchQueue.main.async {
                            previewImage.image = image
                        }
                    }
                }
                
                DispatchQueue.main.async {
                    Loader.Stop()
                    self.collectionView.reloadData()
                }
                
            }
        }
    }
    
    private func getImageFromAsset(asset:PHAsset,size:Int,thumbnail:Bool,completion:@escaping(UIImage?)->()){
        let manager = PHImageManager.default()
           let option = PHImageRequestOptions()
        if !thumbnail{
            option.version = .original
        }
           
           //var thumbnail = UIImage()
           option.isSynchronous = true
           manager.requestImage(for: asset, targetSize: CGSize(width: size, height: size), contentMode: .aspectFit, options: option, resultHandler: {(result, info)->Void in
            if result != nil {
            completion(result)
            }else{
                completion(nil)
            }
           })

    }
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func nextAction(_ sender: Any) {
        selectedAsset.getURL { [self] url in
            if self.selectedAsset.mediaType == .video{
              pickeedDataFormUImagePicker(url: url, image: nil)
            }else{
                pickeedDataFormUImagePicker(url: nil, image: self.previewImage.image)
            }
            DispatchQueue.main.async {
                self.navigationController?.popViewController(animated: true)
            }
           
        }
        
        
    }
    @IBAction func expandImage(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "ImageFullScreenVC") as! ImageFullScreenVC
        if let safeImage = previewImage.image{
          vc.image = safeImage
        }
        
        self.navigationController?.pushViewController(vc, animated: true)
    }
    @IBAction func cameraAction(_ sender: Any) {
        
        
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        
        let alertController = UIAlertController(title: nil, message: nil, preferredStyle: .actionSheet)
        let actionTakePhoto = UIAlertAction(title: "Take a Photo or Video", style: .default) { (action) in
            if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera)
            {
                self.imagePicker.sourceType = .camera
                //self.imagePicker.mediaTypes = [kUTTy as String,kUTTypeImage as String]
                self.present(self.imagePicker,animated : true,completion : nil)
            }
            else
            {
                let alert = UIAlertController(title: "oops", message:"Camera not available", preferredStyle: .alert)
                let actionOk = UIAlertAction(title:"OK", style: .default) { (action) in
                }
                alert.addAction(actionOk)
                self.present(alert, animated: true){
                    
                }
            }
        }
        alertController.addAction(actionTakePhoto)
        self.present(alertController, animated: true) { }
        let actionCancel = UIAlertAction(title:"Cancel", style: .destructive) { (action) in
        }
        alertController.addAction(actionCancel)
         
    }
    @IBAction func driveButtonAction(_ sender: Any) {
     
        let picker = HSDrivePicker()
        
        picker.pick(from: self) {
            (manager, file) in
            
            guard let fileName = file?.name else {
                print("No file picked")
                return
            }

            let destURL =  URL(fileURLWithPath: NSTemporaryDirectory()).appendingPathComponent(fileName)
            let destinationPath = destURL.path
            manager?.downloadFile(file, toPath: destinationPath, withCompletionHandler: { error in
                self.arrayOfvideoData.removeAll()
                self.arrayOfImageData.removeAll()
                Loader.Show()
                if error != nil {
                    Loader.Stop()
                    print("Error downloading : \(error?.localizedDescription ?? "")")
                } else {
                    if file!.mimeType!  == "image/png" || file!.mimeType! == "image/jpg" || file!.mimeType! == "image/jpeg" {
                        let tempImage = UIImage(contentsOfFile: destinationPath)
                        self.fileSize = Int((tempImage?.jpegData(compressionQuality: 0.6))!.count)
                        print("FileSize of Image from GDrive : \(self.fileSize)")
                        self.arrayOfImageData.append((tempImage?.jpegData(compressionQuality: 0.6)!)! )
                    }
                    else if file!.mimeType! == "video/mp4" {
                        var videoAsData = NSData()

                        let url = destURL
                        print("destURL is \(destURL)")
                          do {
                            videoAsData = try Data(contentsOf: url) as NSData
                             } catch let error {
                               print(error)
                             }
                        
                        self.fileSize = Int((videoAsData as Data).count)
                        self.arrayOfvideoData.append(videoAsData as Data)
                        print("Video As Data : \(videoAsData)")
                        print(self.arrayOfvideoData)

                        print("FileSize of video from GDrive : \((videoAsData as Data).count)")
    
                    }
                    self.uploadFile()
                    print("Success downloading to : \(destinationPath)")
                }
                
            })
        }
 
        
    }
    @IBAction func dropBoxButtonAction(_ sender: Any) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "MEDropboxFilesListVC") as! MEDropboxFilesListVC
        vc.delegate = self
        self.navigationController?.pushViewController(vc, animated: true)
        
        
    }

    func pickeedDataFormUImagePicker(url:URL?,image:UIImage?) {
        self.arrayOfvideoData.removeAll()
        self.arrayOfImageData.removeAll()
       // Loader.Show()
        if url != nil {
            print("url is \(url!)")
            let path = url?.path
            do {
                let resources = try url!.resourceValues(forKeys:[.fileSizeKey])
                fileSize = Int(resources.fileSize!)
                print ("Size of video file: \(fileSize!)")
            } catch {
                print("Error in getting the video file size: \(error)")
            }
            let vData = (try? Data(contentsOf: url!))
            self.arrayOfvideoData.append(vData!)
        } else {
            let imgData = image?.jpegData(compressionQuality: 0.8)
            self.arrayOfImageData.append(imgData!)
            print("Size of Image(bytes):\(imgData?.count ?? 0)")
            fileSize = Int(imgData!.count)
        }
        self.uploadFile()
    }
    func uploadFile() {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
         let dateString = dateFormatter.string(from: date)
            print("DateString:\(dateString)")
           
      //  Loader.Show()
        let params : NSDictionary = ["user_id":"\(MEUserDetails.currentUser.id!)","upload_date":dateString]
        print(params)
        getPlanDetails {success in
          //  Loader.Stop()
            if success{
                DispatchQueue.main.async {
                    
                
                ServiceManager.sharedInstance.parseLinkUsingPostMethod("upload_files", with: ["id":"\(MEUserDetails.currentUser.id!)","upload_date":dateString],arrayOfImages:self.arrayOfImageData,arrayOfVideos:self.arrayOfvideoData, withHud: true) { (success, result, error) in
                if success == true {
                    
                    self.alertWithOneAction(title: "", message: "File uploaded successfully.", actionTitle: "OK", completion: nil)
                    Loader.Stop()
                    ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: false) { success, result, error in
                        if success == true {
                            if (result as! NSDictionary)["message"] as! String == "Success" {
                                let planDetailsDict = result!["packages"]! as! NSDictionary
                                let newLeftSpace = planDetailsDict["left_space"] as AnyObject
                                
                                print("Space Left after new upload: \(newLeftSpace)")
                            }
                        } else {
                            print("Failed to update server")
                        }
                    }
                } else {
                    DispatchQueue.main.async {
                        self.alertWithOneAction(title: "", message: "File Uploading Failed.", actionTitle: "OK", completion: nil)
                    }
                }
            
            }
            }
            }
        }

    }
    func getPlanDetails(completion:@escaping(Bool)->Void){
        ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { success, result, error in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let planDetailsDict = result!["packages"]! as! NSDictionary
                    let leftSpace = planDetailsDict["left_space"] as AnyObject
                    print(self.fileSize!)
                    print("Space Left : \(leftSpace)")
                    if (leftSpace as! Int) > self.fileSize! {
                        completion(true)
                    } else {
                        completion(false)
                        DispatchQueue.main.async {
                            self.alertWithOneAction(title: "Not enough space left!", message: "Please upgrade your plan", actionTitle: "OK", completion: nil)
                        }
                    }
                }
            } else {
                completion(false)
                DispatchQueue.main.async {
                    //Loader.Stop()
                    self.alertWithOneAction(title: "", message: "Please try again.", actionTitle: "OK", completion: nil)
                }
            }
           
        }
    }
    func alertWithOneAction(title: String!, message: String!, actionTitle: String!, completion: ((UIAlertAction)-> Void)?){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: actionTitle, style: .default, handler: completion)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
}
extension UploadMediaVC:UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        thumbNails.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCollectionCell", for: indexPath) as! ImageCollectionCell
        
        cell.imageView.image = thumbNails[indexPath.row]

        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (collectionView.frame.width/4)
            return CGSize(width: width, height: width)
        }
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        self.previewImage.image = thumbNails[indexPath.row]
        self.selectedAsset = imageAssets[indexPath.row]
        getImageFromAsset(asset: selectedAsset, size: 720, thumbnail: false) { image in
            DispatchQueue.main.async {
                self.previewImage.image = image
            }
        }
    }
    
}
extension UploadMediaVC:UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let image = info[.editedImage] != nil ? (info[.editedImage] as! UIImage) : nil
        
        
        let url = info[.mediaURL] != nil ? (info[.mediaURL] as! URL) : nil
        
        
        self.pickeedDataFormUImagePicker(url: url, image: image)
        dismiss(animated: true, completion: nil)

        
    }
    
    //MARK:- Image Picker Cancel
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
    {
        dismiss(animated: true, completion: nil)
    }
}
extension UploadMediaVC:GoogleDriveViewControllerDelegate{
    func getFile(data: NSData, mimeType:String) {
        self.arrayOfvideoData.removeAll()
        self.arrayOfImageData.removeAll()
        Loader.Show()
        if mimeType == "image" {
            self.arrayOfImageData.append(data as Data)
            fileSize = Int((data as Data).count)
            print("Filesize of Dropbox Image: \(fileSize!)")
            self.uploadFile()
        }
        else if mimeType == "video"{
            self.arrayOfvideoData.append(data as Data)
            fileSize = Int((data as Data).count)
            print("Filesize of Dropbox Video: \(fileSize!)")
            self.uploadFile()//http://stackoverflow.com/questions/30717302/how-to-play-movie-file-from-the-web-with-nsdata
        }
        else{
            print(mimeType)
        }
        
    }
    
    
}
