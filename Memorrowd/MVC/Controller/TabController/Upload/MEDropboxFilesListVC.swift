//
//  MEDropboxFilesListVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 23/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import SwiftyDropbox
import AVFoundation
import SVProgressHUD

protocol GoogleDriveViewControllerDelegate: NSObjectProtocol{
    func getFile(data: NSData, mimeType: String)

}
class MEDropboxFilesListVC: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var delegate:GoogleDriveViewControllerDelegate?
    /*   func setDelegate(tdelegate:GoogleDriveViewControllerDelegate){
     self.delegate = tdelegate
     }*/
    var arrSelectFiles = [DropBoxList]()
    var isLoad = false
    let myGroup = DispatchGroup()
    var arrOtherFiles = NSMutableArray()
    var videoThumbNailImage = UIImage()
    @IBOutlet weak var tableView: UITableView!
    var rightMenuButton = UIBarButtonItem()
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.actionDropboxLogin()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if isLoad == true {
            if (DropboxClientsManager.authorizedClient != nil) {
                self.getImageFromDropbox(path: "", isBack: false)
            }
        }
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    
    func actionDropboxLogin() {
           if (DropboxClientsManager.authorizedClient != nil) {
            self.tableView.reloadData()
            self.getImageFromDropbox(path: "", isBack: false)
        } else {
            guard let rwURL = URL(string: "https://www.dropbox.com/oauth2/authorize?client_id=d578h6sozhcwyye&redirect_uri=https://memorrowd/callback&response_type=code") else { return }
           /*
            DropboxClientsManager.authorizeFromController(UIApplication.shared,
                                                            controller: self,
                                                            openURL: { (url: URL) -> Void in
                                                              UIApplication.shared.open(rwURL)
                                                            })
            
           */
            let viewcontroller = self.navigationController?.viewControllers[0]
            let scopeRequest = ScopeRequest(scopeType: .user, scopes: ["account_info.read files.content.read files.content.write files.metadata.read file_requests.read"], includeGrantedScopes: false)
            DropboxClientsManager.authorizeFromControllerV2(
                UIApplication.shared,
                controller: viewcontroller,
                loadingStatusDelegate: nil,
                openURL: {
                    
                    (url: URL) -> Void in UIApplication.shared.open(rwURL)
                    
                },
                scopeRequest: scopeRequest
            )
            self.getImageFromDropbox(path: "", isBack: false)
            isLoad = true
        }
    }
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.black]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.black]
            navBarAppearance.backgroundColor = .white
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.title = "Dropbox"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor:UIColor.black, NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
        
        let logoButton =  UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeHandler))
        self.navigationItem.leftBarButtonItem = logoButton
        
        rightMenuButton = UIBarButtonItem(title: "Sign Out", style: .plain, target: self, action: #selector(backHandler))
        self.navigationItem.rightBarButtonItem = rightMenuButton
    }
    //Fetch all images from DropBox
    func getImageFromDropbox(path:String, isBack:Bool) {
        self.arrSelectFiles.removeAll()
        self.arrOtherFiles.removeAllObjects()
        let myGroup = DispatchGroup()
        
        //   let client = DropboxClient(accessToken: "fSBh430WwX0AAAAAAAAAAXGB-_on3ZjDvxFj7yDFUam8g_h8f_1t2M4Dzv6weu3d")
let client = DropboxClientsManager.authorizedClient
        //Get list of folder of dropbox by set (path: "/")
        //Or you can get folder inside a folder by set (path: "/Photos")
        if client != nil {
            print("Client is \(client)")
            client!.files.listFolder(path: path).response(queue: DispatchQueue(label: "MyCustomSerialQueue")) { objList, error in
                if let resultList = objList {
                    //Create a for loop for getting all the entities individually
                    DispatchQueue.main.async {
                        SVProgressHUD.show()
                    }
                    for entry in resultList.entries {
                        myGroup.enter()
                        //Check if file have metadata or not
                        if let fileMetadata = entry as? Files.FileMetadata {
                            
                            //Check file type by extention .jpg/.png . You can check this by your own added extention
                            print(fileMetadata.name)
                            if self.isFileImage(filename: fileMetadata.name) == "video" || self.isFileImage(filename: fileMetadata.name) == "image" {
                                //Get Path for save image in document directory
                                let destination : (NSURL, HTTPURLResponse) -> NSURL = { temporaryURL, response in
                                    return self.getDocumentDirectoryPath(fileName: fileMetadata.name)
                                }
                                client!.files.download(path: fileMetadata.pathLower!).response { response, error in
                                    if let (url,data) = response {
                                        //let data = NSData(contentsOfURL: url)
                                        let img = UIImage(data: data)
                                        var containsFile = false
                                        for item in self.arrSelectFiles {
                                            if item.name == fileMetadata.name {
                                                containsFile = true
                                            } }
                                        if containsFile == false {
                                            if self.isFileImage(filename: fileMetadata.name) == "video" {
                                                
                                                let dict = ["name":fileMetadata.name,"type":self.isFileImage(filename:fileMetadata.name),"pathLower":fileMetadata.pathLower!,"imagData":data,"image":UIImage(named: "video-1")!] as NSDictionary
                                                let obj = DropBoxList().initWithDictionary(userDictionary: dict )
                                                self.arrSelectFiles.append(obj)
                                                self.arrOtherFiles.add(fileMetadata.name)
                                                print("Finished request \(self.arrSelectFiles.count)")
                                                myGroup.leave()
                                                if self.arrOtherFiles.count == resultList.entries.count{
                                                    SVProgressHUD.dismiss()
                                                    DispatchQueue.main.async {
                                                        self.tableView.reloadData()
                                                    }
                                                }
                                            } else {
                                                let dict = ["name":fileMetadata.name,"type":self.isFileImage(filename: fileMetadata.name),"pathLower":fileMetadata.pathLower!,"imagData":data,"image":img!] as NSDictionary
                                                
                                                let obj = DropBoxList().initWithDictionary(userDictionary: dict )
                                                self.arrSelectFiles.append(obj)
                                                self.arrOtherFiles.add(fileMetadata.name)
                                                print("Finished request \(self.arrSelectFiles.count)")
                                                myGroup.leave()
                                                if self.arrOtherFiles.count == resultList.entries.count{
                                                    SVProgressHUD.dismiss()
                                                    DispatchQueue.main.async {
                                                        self.tableView.reloadData()
                                                    }
                                                }
                                            }
                                        } else {
                                            print("Image already added to array")
                                        } } } } else {
                                            print("File is not an image")
                                            self.arrOtherFiles.add(fileMetadata.name)
                                        } } else {  //If file have not metadata it mean it is a folder.
                                            let dict = ["name":entry.name,"type":"Folder","imag":UIImage(named: "Folder")!,"pathLower":entry.pathLower!] as NSDictionary
                                            let obj = DropBoxList().initWithDictionary(userDictionary: dict )
                                            self.arrSelectFiles.append(obj)
                                            self.arrOtherFiles.add(entry.name)
                                            print("Finished request \(self.arrSelectFiles.count)")
                                            myGroup.leave()
                                            if self.arrOtherFiles.count == resultList.entries.count{
                                                SVProgressHUD.dismiss()
                                                DispatchQueue.main.async {
                                                    self.tableView.reloadData()
                                                }
                                            }
                                        }
                    }
                    if self.arrOtherFiles.count == resultList.entries.count{
                        DispatchQueue.main.async {
                            self.tableView.reloadData()
                        }
                    }
                } else {
                    print(error)
                }
            }
        } else {
            print("Client is nil")
        }
        myGroup.notify(queue: .main) {
            print("Array Selected files\(self.arrSelectFiles.count)")
            DispatchQueue.main.async {
                self.tableView.reloadData()
            }
        }
    }
    @IBAction func signOutPressed(_ sender: UIButton) {
        DropboxClientsManager.unlinkClients()
        self.navigationController?.popViewController(animated: true)
    }
    //MARK: check for file type
    private func isFileImage(filename:String) -> String {
        let lastPathComponent = (filename as NSString).pathExtension.lowercased()
        if lastPathComponent == "jpg" || lastPathComponent == "png" {
            return "image"
        }
        else if lastPathComponent == "mp4" {
            return "video"
        }
        else {
            return ""
        }
    }
    //to get document directory path
    func getDocumentDirectoryPath(fileName:String) -> NSURL {
        let fileManager = FileManager.default
        let directoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        let UUID = NSUUID().uuidString
        let pathComponent = "\(UUID)-\(fileName)"
        return directoryURL.appendingPathComponent(pathComponent) as NSURL
    }
    //Table view delegates
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int {
        return self.arrSelectFiles.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "driveCell1")
        let labelName = cell!.viewWithTag(2) as! UILabel
        let imagePic = cell!.viewWithTag(1) as! UIImageView
        labelName.text = self.arrSelectFiles[indexPath.row].name!
        imagePic.clipsToBounds = true
        imagePic.image =  self.arrSelectFiles[indexPath.row].image
        return cell!
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if self.arrSelectFiles[indexPath.row].type! == "Folder" {
            navigationItem.rightBarButtonItem?.title = "Back"
            self.getImageFromDropbox(path: self.arrSelectFiles[indexPath.row].pathLower!, isBack: true)
        } else {
            self.delegate?.getFile(data: self.arrSelectFiles[indexPath.row].imagOrVdodata as! NSData, mimeType: self.arrSelectFiles[indexPath.row].type!)
            self.navigationController?.popViewController(animated: true)
        }
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 83
    }
    @objc func closeHandler(){
        self.navigationController?.popViewController(animated: true)
    }
    @objc func backHandler(){
        if navigationItem.rightBarButtonItem?.title == "Back" {
            self.navigationItem.rightBarButtonItem?.title = "Sign Out"
            self.getImageFromDropbox(path: "", isBack: false)
        }
        else if navigationItem.rightBarButtonItem?.title == "Sign Out" {
            DropboxClientsManager.unlinkClients()
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
    @objc func actionPushNotificationInActiveState(_ notification: Notification) {
        
        let message = "Its time to post your post."
        
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
        let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
            self.navigationController?.present(listVC, animated: true, completion: {
                
            })
        })
        let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(buttonView)
        alertController.addAction(buttonCancel)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
        listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
        self.navigationController?.present(listVC, animated: true, completion: {
            
        })
    }
    
}
