//
//  MEGoogleDriveVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 20/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import MobileCoreServices
import SDWebImage
import GoogleSignIn
import GoogleAPIClientForREST
import GTMSessionFetcher
import GTMAppAuth


class MEGoogleDriveVC: UIViewController, UINavigationControllerDelegate ,UIImagePickerControllerDelegate, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, UISearchDisplayDelegate {
    
    
  
     /*  func setDelegate(tdelegate:GoogleDriveViewControllerDelegate){
           self.delegate = tdelegate
       }*/
       
       var window: UIWindow?
       let driveService : GTLRDriveService =  GTLRDriveService()
       @IBOutlet weak var tableView: UITableView!
   
       var fileList:[GTLRDrive_File]!
       var history:[String] = []
    var isBack = true
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.delegate = self
        self.tableView.dataSource = self
        tableView.tableFooterView = UIView()
        self.setUpNavigationBar()
           GIDSignIn.sharedInstance().clientID = "59368663056-1gvcq3ns1h3in4ni6vle9p8bl2l71e8s.apps.googleusercontent.com"
           GIDSignIn.sharedInstance().delegate = self
           GIDSignIn.sharedInstance().scopes = [kGTLRAuthScopeDrive]
           GIDSignIn.sharedInstance()?.signIn()
        
       
        // Do any additional setup after loading the view.
    }
    override func viewDidAppear(_ animated: Bool) {
        if ((GIDSignIn.sharedInstance()?.hasPreviousSignIn())!) {
            let currentUser = GIDSignIn.sharedInstance()?.currentUser
            print("user = \(String(describing: currentUser))")

            if currentUser != nil {
                driveService.authorizer = currentUser?.authentication.fetcherAuthorizer()
                self.listFiles(queryString: "( mimeType = 'image/jpeg' or mimeType = 'image/png' or mimeType = 'video/mp4') and 'root' in parents and trashed=false", folderId: "root", isBack: false)
            }
    
            } else {
                     GIDSignIn.sharedInstance().signIn()
            }
       
       }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    
    //MARK:- Set Up NavigationBar
           func setUpNavigationBar() {
            if #available(iOS 13.0, *) {
                       let navBarAppearance = UINavigationBarAppearance()
                       navBarAppearance.configureWithOpaqueBackground()
                       navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                       navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                       navBarAppearance.backgroundColor = kAppThemeYellowTop
                       self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                       self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
                   }
               self.navigationController?.navigationBar.isHidden = false
               self.navigationItem.setHidesBackButton(true, animated: true)
               self.navigationItem.title = "Select File"
               self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
             
               self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
               
               let logoButton =  UIBarButtonItem(title: "Close", style: .plain, target: self, action: #selector(closeHandler))
               
               self.navigationItem.leftBarButtonItem = logoButton
            let rightMenuButton = UIBarButtonItem(title: "Back", style: .plain, target: self, action: #selector(backHandler))
                //UIBarButtonItem(image: Images.kMenuIcon, style: .plain, target: self, action: #selector(bac))
               self.navigationItem.rightBarButtonItem = rightMenuButton
           }
           
       public func search(_ name: String,onCompleted: @escaping (GTLRDrive_File?, Error?) -> ()) {
           let query = GTLRDriveQuery_FilesList.query()
           query.pageSize = 1
           query.q = "name contains '\(name)'"
           self.driveService.executeQuery(query) { (ticket, results, error) in
               onCompleted((results as? GTLRDrive_FileList)?.files?.first, error)
          
           }
       }
    public func download(_ index: Int) {
        let query = GTLRDriveQuery_FilesGet.queryForMedia(withFileId: self.fileList[index].identifier!)
        
        driveService.executeQuery(query) { callbackTicket, file, error in
            if error == nil {
                var fileContent: String? = nil
               if  let data = (file as? GTLRDataObject)?.data {
                    fileContent = String(
                        data: data,
                        encoding: .utf8)
                   // self.delegate?.getFile(data: data as NSData, mimeType: self.fileList[index].mimeType!)
                                  self.dismiss(animated: true, completion: nil)
                }
               
            } else {
                if let error = error {
                    print("An error occurred: \(error)")
                }
        }
        }

     
     
    }
    public func listFiles(queryString:String, folderId:String, isBack:Bool)  {
        let query = GTLRDriveQuery_FilesList.query()
        query.pageSize = 1000
        query.q = queryString
        query.fields =  "files(id, name, thumbnailLink,mimeType)"
      //  query.q = "'\(folderID)' in parents and mimeType != 'application/vnd.google-apps.folder'"
        self.driveService.executeQuery(query) { (ticket, result, error) in
           if let fList:GTLRDrive_FileList = result as? GTLRDrive_FileList{
                                              
                 self.fileList = fList.files
                 self.tableView.reloadData()
                                              
                     if isBack == false {
                           self.history.append(folderId)
                      }
                                              
                 if let _ = fList.files {
                                                  //scroll top
                    let indexPath = NSIndexPath(item: 0, section: 0)
                 self.tableView.scrollToRow(at: indexPath as IndexPath, at: UITableView.ScrollPosition.top, animated: true)
                 }
                                              
                   }
        }
    }
    
    @objc func closeHandler(){
        self.navigationController?.popViewController(animated: true)
            }
            
            
    @objc func backHandler(){

                if(self.history.count>1) && isBack == false{
                    
                    self.history.removeLast()
                    let previousFolderId = self.history.last
                    if previousFolderId != nil {
                       self.listFiles(queryString:  "( mimeType = 'image/jpeg' or mimeType = 'image/png' or mimeType = 'video/mp4') and '\(previousFolderId!)' in parents and trashed=false", folderId: previousFolderId!, isBack: true)
                    }
                    //here we query for folders, images (jpg, png) and videos (mp4)
                    //you can change your query according to your needs.
                   
                }
            }
            
    
       //Table view delegates
    func tableView(_ tableView:UITableView, numberOfRowsInSection section:Int) -> Int
       {
           let len = (self.fileList != nil) ? self.fileList.count : 0
           return len
       }

         
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
       
        let cell = tableView.dequeueReusableCell(withIdentifier: "driveCell")
        let labelName = cell!.viewWithTag(2) as! UILabel
        let imagePic = cell!.viewWithTag(1) as! UIImageView
        labelName.text = self.fileList[indexPath.row].name!
            imagePic.clipsToBounds = true
        if self.fileList[indexPath.row].thumbnailLink != nil {
            imagePic.sd_setImage(with: NSURL(string: self.fileList[indexPath.row].thumbnailLink!)! as URL, placeholderImage: UIImage(named: "Folder"))
        }
        else {
            imagePic.image = UIImage(named: "Folder")
        }
          
           
        return cell!
       }
       
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           // if selected file is folder, then open that folder with a new query
                if(self.fileList[indexPath.row].mimeType == "application/vnd.google-apps.folder"){
                    
                 self.isBack = false
                 let selectedDriveFile = self.fileList[indexPath.row]
                     self.listFiles(queryString:   "(mimeType = 'application/vnd.google-apps.folder' or mimeType = 'image/jpeg' or mimeType = 'image/png') and '\(String(describing: selectedDriveFile.identifier))' in parents and trashed=false", folderId: selectedDriveFile.identifier!, isBack: false)
                    //here we query for folders, images (jpg, png) and videos (mp4)
                    //you can change your query according to your needs.
               
                    
                    
                }else{
                  self.isBack = true
                self.download(indexPath.row)
            //download selected file
        
    }
    }
       //
       func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
           return 83
       }
       
       // Helper to check if user is authorized
//       func isAuthorized() -> Bool {
//           return (self.driveService.authorizer as! GTMOAuth2Authentication).canAuthorize
//       }
       
      
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
    @objc func actionPushNotificationInActiveState(_ notification: Notification) {
        
        let message = "Its time to post your post."
        
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
        let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
            self.navigationController?.present(listVC, animated: true, completion: {
                
            })
        })
        let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(buttonView)
        alertController.addAction(buttonCancel)
        self.present(alertController, animated: true, completion: nil)
        
        
    }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
        listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
        self.navigationController?.present(listVC, animated: true, completion: {
            
        })
    }
}

extension MEGoogleDriveVC: GIDSignInDelegate {
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let _ = error {
            
        } else {
            print("Authenticate successfully")
             let currentUser = GIDSignIn.sharedInstance()?.currentUser
                       print("user = \(String(describing: currentUser))")

            if currentUser != nil {
               driveService.authorizer = currentUser?.authentication.fetcherAuthorizer()
               self.listFiles(queryString: "(mimeType = 'image/jpeg' or mimeType = 'image/png' or mimeType = 'video/mp4') and 'root' in parents and trashed=false", folderId: "root", isBack: false)
           }
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("Did disconnect to user")
    }
}


