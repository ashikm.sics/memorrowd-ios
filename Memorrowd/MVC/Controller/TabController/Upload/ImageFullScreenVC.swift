

import UIKit

class ImageFullScreenVC: UIViewController {
  
    
    @IBOutlet weak var imageScrollView: ImageScrollView!
    var image = UIImage()
    override func viewDidLoad() {
        super.viewDidLoad()
        imageScrollView.display(image: image)
        imageScrollView.imageContentMode = .aspectFit
        imageScrollView.setup()
    }
    @IBAction func closeAction(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
  

}
