//
//  TabBarController.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 07/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController,UITabBarControllerDelegate {
      var fileIdFromPush = 0
    let isPackageExpired = MEUserDetails.currentUser.isPackageExpired
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
        self.navigationItem.hidesBackButton = true
        self.navigationController?.isNavigationBarHidden = true
        NotificationCenter.default.addObserver(self,selector: #selector(self.tabSelected),name: Notification.Name("sideMenu"),object: nil)
        // Do any additional setup after loading the view.
    }
    @objc private func tabSelected(notification: NSNotification) {
        let ind = (notification.userInfo! as NSDictionary).value(forKey: "ind") as! Int
      
        self.selectedIndex = ind
//        if ind == 1 || ind == 2  || ind == 3 {
//            if isPackageExpired{
//                showExpiredAlert()
//            }else{
//                
//            }
//        }
        
            
        
        
        //do stuff using the userInfo property of the notification object
    }
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        /*
        print("selected Index: \(self.selectedIndex)")
        if isPackageExpired{
        switch selectedIndex {
        case 1:
            showExpiredAlert()
        case 2:
            showExpiredAlert()
        case 3:
            showExpiredAlert()
        default:
            print("Default")
        }}*/
        
    }
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        if viewController == self.viewControllers![1] || viewController == self.viewControllers![2] || viewController == self.viewControllers![3]{
            if isPackageExpired{
                showExpiredAlert()
            }
        }
    }
    func showExpiredAlert(){
        AJAlertController.initialization().showAlertWithOkButton_Title(aStrTitle: "Expired", aStrMessage: "Thank you for trying Memorrowd. Your current pack has expired. Please select a new subscription plan to continue.") { (_, _) in
            self.selectedIndex = 0
            if let vc = self.viewControllers![0] as? UINavigationController{
                let viewController = kStoryboard.instantiateViewController(withIdentifier: "MEUpgradePlanTVC") as! MEUpgradePlanTableViewController
                vc.pushViewController(viewController, animated: true)
            }
          
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
