//
//  UpgradePlanViewController.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 29/4/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//

import UIKit

class MEChoosePlanViewController: UIViewController {
    
    // @IBOutlet weak var btnTrialPhase: UIButton!
    @IBOutlet weak var btnPhase1: UIButton!
    @IBOutlet weak var btnPhase2: UIButton!
    @IBOutlet weak var btnPhase3: UIButton!
    @IBOutlet var pakageNameLabels: [UILabel]!
    @IBOutlet var packageDescriptionLabels: [UILabel]!
    @IBOutlet var packageSizeLabels: [UILabel]!
    @IBOutlet var packagePriceLabels: [UILabel]!
    var isFromSignUp: Bool = false
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpNavigationBar()
        
        // btnTrialPhase.applyGradient(colors: [UIColor(red: 0.69, green: 0.78, blue: 1.00, alpha: 1.00).cgColor, UIColor(red: 0.50, green: 0.60, blue: 1.00, alpha: 1.00).cgColor])
        btnPhase1.applyGradient(colors: [UIColor(red: 0.43, green: 0.88, blue: 0.98, alpha: 1.00).cgColor, UIColor(red: 0.31, green: 0.81, blue: 0.69, alpha: 1.00).cgColor])
        btnPhase2.applyGradient(colors: [UIColor(red: 1.00, green: 0.54, blue: 0.47, alpha: 1.00).cgColor, UIColor(red: 0.98, green: 0.55, blue: 0.70, alpha: 1.00).cgColor])
        btnPhase3.applyGradient(colors: [UIColor(red: 0.84, green: 0.51, blue: 1.00, alpha: 1.00).cgColor, UIColor(red: 0.69, green: 0.32, blue: 0.87, alpha: 1.00).cgColor])
        getDetailsOfPlans()
    }
    
    
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = kAppThemeYellowTop
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.title = "Subscription"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
    }
    
    @IBAction func subscriptionButtonPressed(_ sender: UIButton) {
        
        ServiceManager.sharedInstance.get(withServiceName: "plans", andParameter: nil, withHud: true, completion: { success, result, error in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let userDetailsArray = result!["packages"]! as! NSArray
                    
                    //                    if sender.tag == 1 {
                    //                        print("Trial Phase Selected")
                    //
                    //                        let packageID = (userDetailsArray[0] as! NSDictionary).value(forKey: "package_id") as! Int
                    //                        let packagePrice = (userDetailsArray[0] as! NSDictionary).value(forKey: "package_price") as! String
                    //                        print("package ID: \(packageID)")
                    //                        ServiceManager.sharedInstance.post(withServiceName: "register_plan", andParameters: ["stripeToken":"0", "planid":packageID,"user_id":"\(MEUserDetails.currentUser.id!)","package_price":"\(packagePrice)" ], withHud: true) { success, result, error in
                    //                            if success == true {
                    //                                if (result as! NSDictionary)["message"] as! String == "Success" {
                    //                                    print("Trial phase Activated")
                    //                                    let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                    //                                    UIApplication.shared.keyWindow!.rootViewController = viewController
                    //                                } else {
                    //                                    self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                    //                                }
                    //                            }
                    //                        }
                    //                    }
                    //else
                    if self.isFromSignUp{
                        let planSelected = (sender.tag - 1)
                        print("Trial Phase Selected")
                        let date = Date()
                        let dateFormatter = DateFormatter()
                        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
                        //dateFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
                        let dateString: String = dateFormatter.string(from: date)
                        let packageID = (userDetailsArray[planSelected] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packagePrice = (userDetailsArray[planSelected] as! NSDictionary).value(forKey: "package_price") as! String
                        print("package ID: \(packageID)")
                        ServiceManager.sharedInstance.post(withServiceName: "register_plan", andParameters: ["stripeToken":"0", "planid":packageID,"user_id":"\(MEUserDetails.currentUser.id!)","package_price":"\(packagePrice)","date":dateString ], withHud: true) { success, result, error in
                            if success == true {
                                if (result as! NSDictionary)["message"] as! String == "Success" {
                                    print("Trial phase Activated")
                                    let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                                    UIApplication.shared.keyWindow!.rootViewController = viewController
                                } else {
                                    self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                                }
                            }
                        }
//                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
//                        self.navigationController?.pushViewController(vc, animated: true)
                       
                    }else{
                    let vc = self.storyboard?.instantiateViewController(withIdentifier: "MECheckOutVC") as! MECheckOutViewController
                    if sender.tag == 1 {
                        print("Phase 1 Selected")
                        let packageID = (userDetailsArray[0] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packagePrice = (userDetailsArray[0] as! NSDictionary).value(forKey: "package_price") as! String
                        
                        vc.packageID = packageID
                        print("package ID: \(packageID)")
                        vc.packagePrice = packagePrice
                        vc.from = "choosePlan"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else if sender.tag == 2 {
                        print("Phase 2 selected")
                        let packageID = (userDetailsArray[1] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packagePrice = (userDetailsArray[1] as! NSDictionary).value(forKey: "package_price") as! String
                        
                        vc.packageID = packageID
                        print("package ID: \(packageID)")
                        vc.packagePrice = packagePrice
                        vc.from = "choosePlan"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else if sender.tag == 3 {
                        print("Phase 3 selected")
                        let packageID = (userDetailsArray[2] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packagePrice = (userDetailsArray[2] as! NSDictionary).value(forKey: "package_price") as! String
                        
                        vc.packageID = packageID
                        print("package ID: \(packageID)")
                        vc.packagePrice = packagePrice
                        vc.from = "choosePlan"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    }
                    
                }
                else {
                    self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                }
            }
            else {
                self.showAlertWithOkButton(message: "Please try again")
            }
        })
    }
    
    func getDetailsOfPlans() {
        ServiceManager.sharedInstance.get(withServiceName: "plans", andParameter: nil, withHud: true, completion: { success, result, error in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let userDetailsArray = result!["packages"]! as! NSArray
                    
                    for i in 0...self.pakageNameLabels.count - 1 {
                        //the trial period section is no more needed so put i + 1
                        let index = i // 1
                        
                        let packageID = (userDetailsArray[index] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packageName = (userDetailsArray[index] as! NSDictionary).value(forKey: "package_name") as! String
                        let packageDescription = (userDetailsArray[index] as! NSDictionary).value(forKey: "package_description") as! String
                        let packageSize = (userDetailsArray[index] as! NSDictionary).value(forKey: "package_size") as! String
                        let packageSizeInMB = Double(packageSize)! / 1000000
                        self.pakageNameLabels[index].text = packageName
                        self.packageDescriptionLabels[index].text = packageDescription
                        self.packageSizeLabels[index].text = "\(String(format: "%.2f", packageSizeInMB))MB"
                        
                        print("pricelabelarraycount:\(self.packagePriceLabels.count)")
                        for i in 0...self.packagePriceLabels.count - 1 {
                            let packagePrice = (userDetailsArray[i] as! NSDictionary).value(forKey: "package_price") as! String
                            print(packagePrice)
                            self.packagePriceLabels[i].text = "$\(packagePrice)"
                        }
                        
                    }
                }
                else {
                    self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                }
            }
            else {
                self.showAlertWithOkButton(message: "Please try again")
            }
        })
    }
    
    //MARK:- Show Alert With Ok Button
    func showAlertWithOkButton(message:String){
        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
            
        }
    }
}

extension UIButton
{
    func applyGradient(colors: [CGColor])
    {
        layer.cornerRadius = 8
        clipsToBounds = true
        layer.shadowRadius = 5
        layer.shadowOpacity = 0.5
        layer.shadowColor = UIColor.lightGray.cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.cornerRadius = layer.cornerRadius
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = bounds
        layer.insertSublayer(gradientLayer, at: 0)
    }
}
