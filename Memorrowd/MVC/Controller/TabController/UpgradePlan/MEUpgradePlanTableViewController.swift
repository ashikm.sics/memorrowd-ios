//
//  MEUpgradePlanTableViewController.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 4/5/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//

import UIKit

class MEUpgradePlanTableViewController: UITableViewController {
    
    @IBOutlet weak var btnPhase1: UIButton!
    @IBOutlet weak var btnPhase2: UIButton!
    @IBOutlet weak var btnPhase3: UIButton!
    @IBOutlet var packageNameLabels: [UILabel]!
    @IBOutlet var packageSizeLabels: [UILabel]!
    @IBOutlet var packageDescriptionLabels: [UILabel]!
    @IBOutlet var packagePriceLabels: [UILabel]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        btnPhase1.applyGradient(colors: [UIColor(red: 0.43, green: 0.88, blue: 0.98, alpha: 1.00).cgColor, UIColor(red: 0.31, green: 0.81, blue: 0.69, alpha: 1.00).cgColor])
        btnPhase2.applyGradient(colors: [UIColor(red: 1.00, green: 0.54, blue: 0.47, alpha: 1.00).cgColor, UIColor(red: 0.98, green: 0.55, blue: 0.70, alpha: 1.00).cgColor])
        btnPhase3.applyGradient(colors: [UIColor(red: 0.84, green: 0.51, blue: 1.00, alpha: 1.00).cgColor, UIColor(red: 0.69, green: 0.32, blue: 0.87, alpha: 1.00).cgColor])
        getDetailsOfPlans()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
    }
    
    //MARK:- Set Up NavigationBar
    func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
            let navBarAppearance = UINavigationBarAppearance()
            navBarAppearance.configureWithOpaqueBackground()
            navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
            navBarAppearance.backgroundColor = kAppThemeYellowTop
            self.navigationController?.navigationBar.standardAppearance = navBarAppearance
            self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
        }
        self.navigationController?.navigationBar.isHidden = false
        self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.title = "Upgrade Plan"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
        
        self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
        let leftButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(leftButtonAction))
        self.navigationItem.leftBarButtonItem = leftButtonButton
    }
    
    @objc func leftButtonAction() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @IBAction func subscriptionButtonPressed(_ sender: UIButton) {
        if sender.tag == 1 {
            print("Phase 1 Selected")
            ServiceManager.sharedInstance.get(withServiceName: "plans", andParameter: nil, withHud: true, completion: { success, result, error in
                if success == true {
                    if (result as! NSDictionary)["message"] as! String == "Success" {
                        let userDetailsArray = result!["packages"]! as! NSArray
                        let packageID = (userDetailsArray[0] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packagePrice = (userDetailsArray[0] as! NSDictionary).value(forKey: "package_price") as! String
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MECheckOutVC") as! MECheckOutViewController
                        vc.packageID = packageID
                        print("package ID: \(packageID)")
                        vc.packagePrice = packagePrice
                        vc.from = "upgradePlan"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                    }
                }
                else {
                    self.showAlertWithOkButton(message: "Please try again")
                }
            })
        }
        else if sender.tag == 2 {
            print("Phase 2 selected")
            ServiceManager.sharedInstance.get(withServiceName: "plans", andParameter: nil, withHud: true, completion: { success, result, error in
                if success == true {
                    if (result as! NSDictionary)["message"] as! String == "Success" {
                        let userDetailsArray = result!["packages"]! as! NSArray
                        let packageID = (userDetailsArray[1] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packagePrice = (userDetailsArray[1] as! NSDictionary).value(forKey: "package_price") as! String
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MECheckOutVC") as! MECheckOutViewController
                        vc.packageID = packageID
                        print("package ID: \(packageID)")
                        vc.packagePrice = packagePrice
                        vc.from = "upgradePlan"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                    }
                }
                else {
                    self.showAlertWithOkButton(message: "Please try again")
                }
            })
        }
        else if sender.tag == 3 {
            print("Phase 3 selected")
            ServiceManager.sharedInstance.get(withServiceName: "plans", andParameter: nil, withHud: true, completion: { success, result, error in
                if success == true {
                    if (result as! NSDictionary)["message"] as! String == "Success" {
                        let userDetailsArray = result!["packages"]! as! NSArray
                        let packageID = (userDetailsArray[2] as! NSDictionary).value(forKey: "package_id") as! Int
                        let packagePrice = (userDetailsArray[2] as! NSDictionary).value(forKey: "package_price") as! String
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "MECheckOutVC") as! MECheckOutViewController
                        vc.packageID = packageID
                        print("package ID: \(packageID)")
                        vc.packagePrice = packagePrice
                        vc.from = "upgradePlan"
                        self.navigationController?.pushViewController(vc, animated: true)
                    }
                    else {
                        self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                    }
                }
                else {
                    self.showAlertWithOkButton(message: "Please try again")
                }
            })
        }
    }
    
    func getDetailsOfPlans() {
        ServiceManager.sharedInstance.get(withServiceName: "plans", andParameter: nil, withHud: true, completion: { success, result, error in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let userDetailsArray = result!["packages"]! as! NSArray
                    for i in 0...self.packageNameLabels.count - 1 {
                    let packageID = (userDetailsArray[i] as! NSDictionary).value(forKey: "package_id") as! Int
                    let packageName = (userDetailsArray[i] as! NSDictionary).value(forKey: "package_name") as! String
                    let packageDescription = (userDetailsArray[i] as! NSDictionary).value(forKey: "package_description") as! String
                    let packageSize = (userDetailsArray[i] as! NSDictionary).value(forKey: "package_size") as! String
                    let packageSizeInMB = Double(packageSize)! / 1000000
                        let packagePrice = (userDetailsArray[i] as! NSDictionary).value(forKey: "package_price") as! String
                        self.packageNameLabels[i].text = packageName
                        self.packageDescriptionLabels[i].text = packageDescription
                        self.packageSizeLabels[i].text = "\(String(format: "%.2f", packageSizeInMB))MB"
                        self.packagePriceLabels[i].text = "$\(packagePrice)"
                    }
                }
                else {
                    self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                }
            }
            else {
                self.showAlertWithOkButton(message: "Please try again")
            }
        })
    }
    
    //MARK:- Show Alert With Ok Button
    func showAlertWithOkButton(message:String){
        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
            
        }
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 3
    }
    
    // MARK: - Table view Delegate Methods
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 230
    }
    
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
    @objc func actionPushNotificationInActiveState(_ notification: Notification) {
        
        let message = "Its time to post your post."
        
        let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
        let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
            
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
            self.navigationController?.present(listVC, animated: true, completion: {
                
            })
        })
        let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
            self.dismiss(animated: true, completion: nil)
        })
        alertController.addAction(buttonView)
        alertController.addAction(buttonCancel)
        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func actionPushNotificationBGState(_ notification: Notification) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
        listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
        self.navigationController?.present(listVC, animated: true, completion: {
            
        })
    }
    
}

