//
//  MECheckOutViewController.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 29/4/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//

import UIKit
import Stripe


class MECheckOutViewController: UITableViewController, UITextFieldDelegate {
    var paymentIntentClientSecret: String?
    
    var packageID : Int?
    var packagePrice = ""
    var from = ""
    var maxLength = 0
    
    @IBOutlet weak var cancelButton: UIButton!
    @IBOutlet weak var payButton: UIButton!
    @IBOutlet weak var LabelValue: UILabel!
    @IBOutlet weak var cardNumberTextField: UITextField!
    @IBOutlet weak var validityTextField: UITextField!
    @IBOutlet weak var cvvTextField: UITextField!
    @IBOutlet weak var cardHolderNameTextField: UITextField!
    @IBOutlet weak var btnPay: UIButton!
    @IBOutlet weak var outerView: UIView!
    @IBOutlet weak var innerView: UIView!
    
    lazy var cardTextField: STPPaymentCardTextField = {
        let cardTextField = STPPaymentCardTextField()
        return cardTextField
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setUpNavigationBar()
        initializeTextFields()
        LabelValue.text = "$\(packagePrice)"
        cancelButton.layer.cornerRadius = 8
        cancelButton.clipsToBounds = true
        payButton.layer.cornerRadius = 8
        payButton.clipsToBounds = true
        cardNumberTextField.applyBorderColor()
        validityTextField.applyBorderColor()
        cvvTextField.applyBorderColor()
        cardHolderNameTextField.applyBorderColor()
        innerView.setCornerRadius()
        outerView.setCornerRadius()
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismissKeyboard))
        view.addGestureRecognizer(tap)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
       }
    
    @objc func dismissKeyboard () {
        view.endEditing(true)
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        dismissKeyboard()
        return true
    }
    
    func initializeTextFields () {
        cardNumberTextField.delegate = self
        cardNumberTextField.keyboardType = UIKeyboardType.numberPad
        
        validityTextField.delegate = self
        validityTextField.keyboardType = UIKeyboardType.numbersAndPunctuation
        
        cvvTextField.delegate = self
        cvvTextField.keyboardType = UIKeyboardType.numberPad
        
        cardHolderNameTextField.delegate = self
        cardHolderNameTextField.keyboardType = UIKeyboardType.alphabet
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if textField == cardNumberTextField {
            maxLength = 16
        }
        if textField == validityTextField {
            maxLength = 7
        }
        if textField == cvvTextField {
            maxLength = 3
        }
        if textField == cardHolderNameTextField {
            maxLength = 35
        }
        let currentString: NSString = (textField.text ?? "") as NSString
        let newString: NSString =
        currentString.replacingCharacters(in: range, with: string) as NSString
        return newString.length <= maxLength
    }
    
    func displayAlert(title: String, message: String, restartDemo: Bool = false) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .cancel))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    //MARK:- Set Up NavigationBar
     func setUpNavigationBar() {
         if #available(iOS 13.0, *) {
                    let navBarAppearance = UINavigationBarAppearance()
                    navBarAppearance.configureWithOpaqueBackground()
                    navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                    navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                    navBarAppearance.backgroundColor = kAppThemeYellowTop
                    self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                    self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
                }
         self.navigationController?.navigationBar.isHidden = false
         self.navigationItem.setHidesBackButton(true, animated: true)
        self.navigationItem.title = "Payment"
         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
       
         self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
         let leftButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(leftButtonAction))
         self.navigationItem.leftBarButtonItem = leftButtonButton
}
    
    @objc func leftButtonAction() {
        self.navigationController?.popViewController(animated: true)
     }
    
    
    @IBAction func payButtonPressed(_ sender: UIButton) {
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        //dateFormatter.locale = NSLocale(localeIdentifier: "en_IN") as Locale
        let dateString: String = dateFormatter.string(from: date)
           
        
        DispatchQueue.main.async {
         
            if self.cardNumberTextField.hasText && self.validityTextField.hasText && self.cvvTextField.hasText && self.cardHolderNameTextField.hasText {
            
            let cardParams = STPCardParams()
                cardParams.number = self.cardNumberTextField.text
                cardParams.cvc = self.cvvTextField.text
                cardParams.name = self.cardHolderNameTextField.text
            
                if let validity = self.validityTextField.text {
                    guard let monthInteger = Int(validity.prefix(2)) else { return}
                    guard let yearInteger = Int(validity.suffix(4)) else {
                        
                        return }
            cardParams.expMonth = UInt(monthInteger)
            cardParams.expYear = UInt(yearInteger)
            
            
                    if STPCardValidator.validationState(forCard: cardParams) == .valid {
                self.btnPay.isUserInteractionEnabled = false
                
                STPAPIClient.shared.createToken(withCard: cardParams) { (token: STPToken?, error: Error?) in
                    guard let token = token, error == nil else {
                        self.btnPay.isUserInteractionEnabled = true
                        return
                    }
                    print("Token : \(token)")
                    print("Ready to pay the amount: \(self.packagePrice) for the package ID: \(self.packageID!)")
                    
                   
                    if self.from == "choosePlan" {
                        self.registerPlan(token: token)
                    }
                    else if self.from == "upgradePlan" {

                        ServiceManager.sharedInstance.post(withServiceName: "upgrade_plan", andParameters: ["stripeToken":"\(token)", "planid":self.packageID!,"user_id":"\(MEUserDetails.currentUser.id!)","package_price":"\(self.packagePrice)" ,"date":dateString], withHud: true) { success, result, error in
                            if success == true {
                                print("package price: \(self.packagePrice), Result: \(result!)")
                                let resultDict = result as! NSDictionary
                                let message = resultDict["message"] as! String
                                if message == "Success"{
                                    
                                }
                                if (result as! NSDictionary)["message"] as! String == "Success" {
                                    let alert = UIAlertController(title: "", message: "Payment Successful", preferredStyle: .alert)
                                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                                        print("Payment Successful")
                                            let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                                            UIApplication.shared.keyWindow!.rootViewController = viewController
                                    }))
                                    self.present(alert, animated: true, completion: nil)
                                }
                            } else {
                                print("Please try again.  transaction unsuccessful")
                            }
                        }
                    }
                }
            
            }else{
                self.displayAlert(title: "Invalid credentials", message: "Please enter valid credentials")
            }
                }
        }else{
            self.displayAlert(title: "", message: "Please fill all the fields")
        }
    }
}
    
    @IBAction func cancelButtonPressed(_ sender: UIButton) {
        let alert = UIAlertController(title: "", message: "Transaction Cancelled", preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { (action) in
            self.cardNumberTextField.text = ""
            self.cvvTextField.text = ""
            self.validityTextField.text = ""
            self.cardHolderNameTextField.text = ""
            if self.from == "upgradePlan" {
                self.navigationController?.popViewController(animated: true)
            }
            else if self.from == "choosePlan" {
                self.navigationController?.popViewController(animated: true)
            }
        }))
        present(alert, animated: true, completion: nil)
    }
    
    
    //MARK:- Show Alert With Ok Button
    func showAlertWithOkButton(message:String){
        AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
        }
    }
    
    //MARK: - TableView Delegate Method
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
       @objc func actionPushNotificationInActiveState(_ notification: Notification) {
  
        let message = "Its time to post your post."
             
               let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
               let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
               
                let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                 listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                self.navigationController?.present(listVC, animated: true, completion: {
                   
                })
               })
               let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                   self.dismiss(animated: true, completion: nil)
               })
               alertController.addAction(buttonView)
               alertController.addAction(buttonCancel)
               self.present(alertController, animated: true, completion: nil)
       }
  
          @objc func actionPushNotificationBGState(_ notification: Notification) {
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                           self.navigationController?.present(listVC, animated: true, completion: {
                              
                           })
    }
    
}

extension MECheckOutViewController: STPAuthenticationContext {
    func authenticationPresentingViewController() -> UIViewController {
        return self
    }
}

extension UITextField {
    func applyBorderColor() {
        layer.borderColor = UIColor.systemOrange.cgColor
        layer.borderWidth = 1.0
        layer.cornerRadius = 8
        clipsToBounds = true
    }
}
extension UIView {
    func setCornerRadius() {
        layer.cornerRadius = 8
        clipsToBounds = true
    }
}

extension MECheckOutViewController {
    func registerPlan(token: STPToken) {
        ServiceManager.sharedInstance.post(withServiceName: "register_plan", andParameters:["stripeToken":"\(token)", "planid":self.packageID!,"user_id":"\(MEUserDetails.currentUser.id!)","package_price":"\(self.packagePrice)","date":"2021-07-19 11:25:55"], withHud: true) { success, result, error in
            if success == true {
                print("package price: \(self.packagePrice)")
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let alert = UIAlertController(title: "", message: "Are you Sure You want to Logout", preferredStyle: .alert)
                    alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
                        print("Payment Successful")
                            let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                            UIApplication.shared.keyWindow!.rootViewController = viewController
                    }))
                    
                }
            } else {
                print("Please try again.  transaction unsuccessful")
            }
        }
    }
}
