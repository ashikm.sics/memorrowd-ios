//
//  MEProfileTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 14/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import MobileCoreServices
import Kingfisher
import SVProgressHUD
import ADCountryPicker
import CoreLocation

class MEProfileTVC: UITableViewController,UITextFieldDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate, ADCountryPickerDelegate {
    
         @IBOutlet var imageViewProfilePic              : UIImageView!
         @IBOutlet var labelName                        : UILabel!
         @IBOutlet var labelUserName                    : UILabel!
         @IBOutlet var textFieldName                    : UITextField!
         @IBOutlet var textFieldEmail                   : UITextField!
         @IBOutlet var textFieldLocation                : UITextField!
         @IBOutlet var textFieldPhoneNumber             : UITextField!
         @IBOutlet var btnChange                        : UIButton!
         @IBOutlet var txtFields                        : [UITextField]!
         @IBOutlet weak var labelTotalSpace             : UILabel!
         @IBOutlet weak var labelLeftSpace              : UILabel!
        @IBOutlet weak var countryCodeButton: UIButton!
    
    var locManager = CLLocationManager()
    var currentLocation: CLLocation!
    
    var imagePicker                = UIImagePickerController()
    let picker = ADCountryPicker()
    var dialCode = ""
    var countryName  = ""
    var countryFlag = UIImage()
    var countryCodeArray = [MECountryList]()
    var countryCodePicked = "IN"
    var edit = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.btnChange.setBorderAndCornerRadius(borderColor:kAppThemeYellowTop, borderWidth: 0, cornerRadius: 5)
      self.imageViewProfilePic.setBorderAndCornerRadius(borderColor:.white, borderWidth: 2, cornerRadius: 53)
        //        self.setUpNavigationBar()
        self.getDetails()
        txtFields.forEach { (txtField) in
            if txtField == self.textFieldEmail {
                txtField.isUserInteractionEnabled = false
            }
            else {
                       txtField.isUserInteractionEnabled = true
            }
                         
                       
                      }
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
          
        self.getSpaceDetails()
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
       }
    func determineMyCurrentLocation() {
        
        locManager = CLLocationManager()
        locManager.delegate = self
        locManager.desiredAccuracy = kCLLocationAccuracyBest
        locManager.requestWhenInUseAuthorization()
        locManager.startUpdatingLocation()
    }
    //MARK:- Set Up NavigationBar
//     func setUpNavigationBar() {
//        if #available(iOS 13.0, *) {
//                   let navBarAppearance = UINavigationBarAppearance()
//                   navBarAppearance.configureWithOpaqueBackground()
//                   navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
//                   navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
//                   navBarAppearance.backgroundColor = kAppThemeYellowTop
//                   self.navigationController?.navigationBar.standardAppearance = navBarAppearance
//                   self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
//               }
//         self.navigationController?.navigationBar.isHidden = true
//         self.navigationItem.setHidesBackButton(true, animated: true)
//         self.navigationItem.title = "Profile"
//         self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
//       
//         self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
//     }
        //MARK:- CustomUI
           func customUI() {
             
            picker.delegate = self
                                      /// Optionally, set this to display the country calling codes after the names
            picker.showCallingCodes = true
                /// The nav bar title to show on picker view
            picker.pickerTitle = "Select a Country"
            let countryCode = MEUserDetails.currentUser.country_code ?? ""
           
            if countryCode == ""{
            
                determineMyCurrentLocation()
            }else{
                countryCodeButton.setTitle(MEUserDetails.currentUser.country_code ?? "", for: .normal)
            }
               
             let toolbarDone = UIToolbar.init()
               toolbarDone.sizeToFit()
               let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done,target: self, action: #selector(self.doneButtonClicked))
               barBtnDone.tintColor = kAppThemeYellowTop
               let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
               toolbarDone.items = [flexSpace,barBtnDone]
//               txtFields.forEach { (txtField) in
//                txtField.isUserInteractionEnabled = false
//                   txtField.inputAccessoryView = toolbarDone
//
//               }
            self.textFieldLocation.text = MEUserDetails.currentUser.address!
            self.textFieldName.text = MEUserDetails.currentUser.name!
            self.textFieldPhoneNumber.text = MEUserDetails.currentUser.number!
            self.textFieldEmail.text = MEUserDetails.currentUser.email!
//            if let image = MEUserDetails.currentUser.picture {
//                let profileUrl = URL(string: image)
//                imageViewProfilePic.kf.setImage(with: profileUrl)
//            }
            self.imageViewProfilePic.downloaded(from:  MEUserDetails.currentUser.picture!)
            self.labelName.text = MEUserDetails.currentUser.name!
            self.labelUserName.text = "@\(MEUserDetails.currentUser.name!)"
           }
    func setCurrentCountryCode(){
       
            
            var lat = ""
            var long = ""
            
            if
               CLLocationManager.authorizationStatus() == .authorizedWhenInUse ||
               CLLocationManager.authorizationStatus() ==  .authorizedAlways
            {
                currentLocation = locManager.location
                 lat = "\(currentLocation.coordinate.latitude)"
                 long = "\(currentLocation.coordinate.longitude)"
            }
            
            let geoCoder = CLGeocoder()
            var placeMark: CLPlacemark!
            let coordinations = CLLocationCoordinate2D(latitude: currentLocation.coordinate.latitude,longitude: currentLocation.coordinate.longitude)
             let location = CLLocation(latitude: coordinations.latitude, longitude: coordinations.longitude)
            geoCoder.reverseGeocodeLocation(location, completionHandler: { [self] (placemarks, error) -> Void in
                            
                            if placemarks != nil{
                                
                                placeMark = placemarks?[0]
                                
                                if let city = placeMark.locality,let country = placeMark.country{
                                    if let countryCode = placeMark.isoCountryCode{
                                     picker.getDialCode(countryCode: countryCode, completion: { phoneCode in
                                                            if phoneCode != nil{
                                                                countryCodeButton.setTitle(phoneCode!, for: .normal)
                                                            }
                                       
                                    })}
                                }
                                        
                                    }
                                })
                            }

    
     //MARK:- Button Actions
           @IBAction func buttonChnageAction(_ sender: UIButton) {
            

            if !self.textFieldPhoneNumber.text!.isEmpty && !self.textFieldName.text!.isEmpty && !self.textFieldLocation.text!.isEmpty {
                 SVProgressHUD.show()
                ServiceManager.sharedInstance.parseLinkUsingPostMethod("updateprofile", with: ["number":self.textFieldPhoneNumber.text!,"email":self.textFieldEmail.text!,"address":self.textFieldLocation.text!,"name":self.textFieldName.text!,"user_id":"\(MEUserDetails.currentUser.id!)", "country_code": dialCode], imageData: self.imageViewProfilePic.image!.jpegData(compressionQuality: 0.7)!, isHud: true) { (success, result, error) in
                       SVProgressHUD.dismiss()
                              if success == true {
                                  if (result as! NSDictionary)["message"] as! String == "Success" {
                                      let userDetailsDict = result!["user_details"]! as! NSDictionary
                                       MEUserDetails.currentUser.initWithDict(dict: userDetailsDict)
                                    self.labelName.text = MEUserDetails.currentUser.name!
                                    self.labelUserName.text = "@\(MEUserDetails.currentUser.name!)"
                                    let data = NSKeyedArchiver.archivedData(withRootObject: userDetailsDict)
                                    UserDefaults.standard.set(data, forKey: "userDetails")
                                    self.tableView.reloadData()
                                        let viewController = kStoryboard.instantiateViewController(withIdentifier: "TabBarController") as! TabBarController
                                           UIApplication.shared.keyWindow!.rootViewController = viewController
                                    
                                  }
                                  else {
                                        self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                                  }
                              }
                              else {
                                    self.showAlertWithOkButton(message: "Please try again")
                              }
                          }
                        
                  }
                else {
                   self.showAlertWithOkButton(message: "Please fill the fields")
                  }
                    
           }
    //MARK:- Show Alert With Ok Button
                 func showAlertWithOkButton(message:String){
                     AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                         
                     }
                 }
    
    @IBAction func countryCodePressed(_ sender: UIButton) {
        self.present(picker, animated: true, completion: nil)
    }
    
          @IBAction func buttonSelectImageAction(_ sender: UIButton) {

            if edit == true {
                let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
                        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                            self.openCamera()
                        }))

                        alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                            self.openGallery()
                        }))

                        alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

                        self.present(alert, animated: true, completion: nil)
            }
                       
                               
          }
    //MARK:- IMAGEPICKER DELEGATE METHOD
          
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
      
        if let image = info[UIImagePickerController.InfoKey.editedImage] as? UIImage {
            self.imageViewProfilePic.image = image
        }
        self.imagePicker.dismiss(animated: true, completion: nil)
    }
    
    func openGallery(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            print("Button capture")
            imagePicker.sourceType = .photoLibrary
            imagePicker.delegate = self
            imagePicker.allowsEditing = true
            present(imagePicker, animated: true, completion: nil)
        }else {
            let alertController = UIAlertController(title: nil, message: "Device has no photoLibrary.", preferredStyle: .alert)
            let okAction = UIAlertAction(title: "Ok", style: .default, handler: { (alert: UIAlertAction!) in
            })
            alertController.addAction(okAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    
    func openCamera()
    {
        if UIImagePickerController.isSourceTypeAvailable(UIImagePickerController.SourceType.camera) {
            imagePicker.delegate = self
            imagePicker.sourceType = .camera
            imagePicker.allowsEditing = true
            self.present(imagePicker, animated: true, completion: nil)
        }
        else
        {
            let alert  = UIAlertController(title: "Warning", message: "You don't have camera", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
        
       //MARK:- Image Picker Cancel
          func imagePickerControllerDidCancel(_ picker: UIImagePickerController)
          {
              dismiss(animated: true, completion: nil)
          }
            @IBAction func buttonEditAction(_ sender: UIButton) {
                if edit == true {
                    let alert = UIAlertController(title: "Choose Image", message: nil, preferredStyle: .actionSheet)
                            alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { _ in
                                self.openCamera()
                            }))

                            alert.addAction(UIAlertAction(title: "Gallery", style: .default, handler: { _ in
                                self.openGallery()
                            }))

                            alert.addAction(UIAlertAction.init(title: "Cancel", style: .cancel, handler: nil))

                            self.present(alert, animated: true, completion: nil)
                }
            
            }
    //MARK:- TextField Delgate
              func textFieldDidBeginEditing(_ textField: UITextField) {
                
                    textField.becomeFirstResponder()
                    
              }
           func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
              
               return true
           }
              func textFieldDidEndEditing(_ textField: UITextField) {
               
                  textField.resignFirstResponder()
              }
       
        func textFieldShouldReturn(_ textField: UITextField) -> Bool {
             textField.resignFirstResponder()
            return true
        }
  
     //MARK:- Done button action keyboard
           @objc func doneButtonClicked() {
               self.view.endEditing(true)
           }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
       @objc func actionPushNotificationInActiveState(_ notification: Notification) {
  
        let message = "Its time to post your post."
             
               let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
               let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
               
                let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                 listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                self.navigationController?.present(listVC, animated: true, completion: {
                   
                })
               })
               let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                   self.dismiss(animated: true, completion: nil)
               })
               alertController.addAction(buttonView)
               alertController.addAction(buttonCancel)
               self.present(alertController, animated: true, completion: nil)
               
           
       }
  
          @objc func actionPushNotificationBGState(_ notification: Notification) {
            let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                            listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                           self.navigationController?.present(listVC, animated: true, completion: {
                              
                           })
    }
    func getDetails(){
        ServiceManager.sharedInstance.post(withServiceName: "listprofile", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { (success, result, error) in
            if success == true {
                if (result as! NSDictionary)["status"] as! Bool == true {
                    let userDetailsDict = result!["user_details"]! as! NSDictionary
                     MEUserDetails.currentUser.initWithDict(dict: userDetailsDict)
                    let data = NSKeyedArchiver.archivedData(withRootObject: userDetailsDict)
                      UserDefaults.standard.set(data, forKey: "userDetails")
                     self.customUI()
                }
                else {
                      self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                }
            }
            else {
                  self.showAlertWithOkButton(message: "Please try again")
            }
        }
    }
    
    func countryPicker(_ picker: ADCountryPicker, didSelectCountryWithName name: String, code: String, dialCode: String) {
        self.dialCode = dialCode
        self.countryCodeButton.setTitle("\(dialCode)", for: .normal)
        self.dismiss(animated: true, completion: nil)
    }
    
    func getSpaceDetails(){
        
        ServiceManager.sharedInstance.post(withServiceName: "plan_details", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) { success, result, error in
            if success == true {
                if (result as! NSDictionary)["message"] as! String == "Success" {
                    let planDetailsDict = result!["packages"]! as! NSDictionary
                    let leftSpace = planDetailsDict["left_space"] as! Int
                    let totalSpace = planDetailsDict["total_space"] as! Int
                    print("Space Left after new upload: \(leftSpace)")
                    let leftSpaceInMB = Double(leftSpace) / 1000000
                    let totalSpaceInMB = Double(totalSpace) / 1000000
                    self.labelTotalSpace.text = "\(String(format: "%.2f", totalSpaceInMB))MB"
                    self.labelLeftSpace.text = "\(String(format: "%.2f", leftSpaceInMB))MB"
                }
            } else {
                print("Failed to update server")
            }
        }
        
    }
    // MARK: - Table view data source

  /*  override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
func hideNavigationBar(self:UIViewController){
    self.navigationController?.setNavigationBarHidden(true, animated: true)
    self.navigationController?.navigationBar.isHidden = true
    self.navigationController?.isNavigationBarHidden = true
}
extension MEProfileTVC : CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        self.setCurrentCountryCode()
    }}
