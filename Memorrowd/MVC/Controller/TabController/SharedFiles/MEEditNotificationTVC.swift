//
//  MEEditNotificationTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 11/12/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import AVFoundation
import AVKit
class MEEditNotificationTVC: UITableViewController,UICollectionViewDelegate,UICollectionViewDataSource,UITextFieldDelegate,UITextViewDelegate,UICollectionViewDelegateFlowLayout {
    @IBOutlet var textfieldDate                  : UITextField!
    @IBOutlet var textfieldTime                  : UITextField!
    @IBOutlet var buttonSubmit                  : UIButton!

    @IBOutlet var collectionView                 : UICollectionView!
    @IBOutlet var imgViewSelectedName              : UIImageView!
    @IBOutlet var labelDate                        : UILabel!
    @IBOutlet var labelTime                        : UILabel!
    @IBOutlet var viewPlayVideo                    : UIView!
    var activeTextField                  =  UITextField()

    var playerLayer                      = AVPlayerLayer()
     let playerController                 = AVPlayerViewController()
    var notificationTime = ""
            @IBOutlet var datePicker                : UIDatePicker!
            var arrayOfImages = ["fb_Square","twitter_square","insta_Square","snapchat_Square","linkedin_Square","whatsapp_square","email_square"]
      
           var arrayOfImagesShare = ["fb_round","twitter_round","insta_round","snapchat_round","linkedin_round","whatsapp_round","email_round","more"]
           var arrayOfImagesShareName = ["Facebook","Twitter","Instagram","Snapchat","Linkedin","Whatsapp","Email","More"]
         var arrayOfSelectedApp = [String]()
          var fileDetails = ListOfShare()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if #available(iOS 13.4, *) {
            datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
        } else {
            // Fallback on earlier versions
        }
        
        arrayOfSelectedApp = self.fileDetails.app!.components(separatedBy: ",")
        self.collectionView.reloadData()
        textfieldDate.setBorderAndCornerRadius(borderColor: UIColor.gray, borderWidth: 1.0, cornerRadius: 4.0)
        textfieldTime.setBorderAndCornerRadius(borderColor: UIColor.gray, borderWidth: 1.0, cornerRadius: 4.0)
        buttonSubmit.setBorderAndCornerRadius(borderColor: UIColor.clear, borderWidth: 0.0, cornerRadius: 4.0)
        textfieldDate.addInputViewDatePicker(target: self, selector: #selector(doneButtonPressedForDate))
        textfieldTime.addInputViewTimeDatePicker(target: self, selector: #selector(doneButtonPressedForTime))
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewWillAppear(_ animated: Bool) {
                super.viewWillAppear(animated)
        self.labelDate.text = self.fileDetails.date!
        self.labelTime.text = self.fileDetails.time!
        self.textfieldDate.text = self.fileDetails.date!
        self.textfieldTime.text = self.fileDetails.time!
        if fileDetails.file_type ==  "video" {
                   self.imgViewSelectedName.isHidden = true
                     self.playVideo()
                     
                  }
                  else {
                     self.imgViewSelectedName.isHidden = false
                   self.imgViewSelectedName.downloaded(from:  fileDetails.file!)

                    
                  }
          
           NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
           
           NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
           
       }
    @objc func doneButtonPressedForDate() {
  
            if let  datePicker = textfieldDate.inputView as? UIDatePicker {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "yyyy-MM-dd"
                textfieldDate.text = dateFormatter.string(from: datePicker.date)
            }
        textfieldDate.resignFirstResponder()
     }
    
    @objc func doneButtonPressedForTime() {
            if let  datePicker = textfieldTime.inputView as? UIDatePicker {
                let dateFormatter = DateFormatter()
                dateFormatter.dateFormat = "HH:mm"
                textfieldTime.text = dateFormatter.string(from: datePicker.date)
            }
            textfieldTime.resignFirstResponder()
     }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
            @objc func actionPushNotificationInActiveState(_ notification: Notification) {
       
             let message = "Its time to post your post."
                  
                    let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
                    let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
                       let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                         listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                        self.navigationController?.present(listVC, animated: true, completion: {
                           
                        })
                     
                    })
                    let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                        self.dismiss(animated: true, completion: nil)
                    })
                    alertController.addAction(buttonView)
                    alertController.addAction(buttonCancel)
                    self.present(alertController, animated: true, completion: nil)
                    
                
            }
       @objc func actionPushNotificationBGState(_ notification: Notification) {
                 let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                                 listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                                self.navigationController?.present(listVC, animated: true, completion: {
                                   
                                })
         }
    //MARK:- Play Video
          func playVideo()  {
              guard let path = Bundle.main.path(forResource: "YogaTutorial", ofType:"mp4") else {
                  debugPrint("Dummy Video not found")
                  return
              }
             /* if dictOfVideo.video! != "" {
              let player = AVPlayer(url: URL(string: dictOfVideo.video!.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed)!)!)
              playerController.player = player
              }*/
            let player = AVPlayer(url: URL(string: self.fileDetails.file!)!)
              playerController.player = player
              self.viewPlayVideo.addSubview(playerController.view)
              playerController.view.frame = CGRect(x: 0, y: 0, width: self.viewPlayVideo.frame.width, height: self.viewPlayVideo.frame.height)
              playerController.view.tintColor = kAppThemeYellowTop
              playerController.view.setBorderAndCornerRadius(borderColor: .clear, borderWidth: 0.0, cornerRadius: 5.0)
              playerController.player?.play()
           
          }
    @IBAction func buttonSetNOtificationAction(_ sender: UIButton) {
        
        self.setNotificationDetails()
                
              }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 1
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 397
    }
    
    // MARK: - Collection view data source
      func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
          return arrayOfImages.count
          
      }
      
      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
          let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cellcolletion", for: indexPath)
          let imageView = cell.viewWithTag(1) as! UIImageView
          imageView.image = UIImage(named: self.arrayOfImages[indexPath.row])
               imageView.contentMode = .scaleAspectFill
              if  self.arrayOfSelectedApp.contains(arrayOfImagesShareName[indexPath.row]) {
           
                  cell.layer.borderColor = UIColor.blue.cgColor
                  cell.layer.borderWidth = 1
                  cell.isSelected = true
              }
              else {
                
                  let cell = collectionView.cellForItem(at: indexPath)
                  cell?.layer.borderColor = UIColor.clear.cgColor
                  cell?.layer.borderWidth = 1
                  cell?.isSelected = false
              }
          return cell
         
      }
   
      func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
           return CGSize(width: 45, height: 45)
      }
    
      func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
           let cell = collectionView.cellForItem(at: indexPath)
          if  !self.arrayOfSelectedApp.contains(arrayOfImagesShareName[indexPath.row]) {
          self.arrayOfSelectedApp.append(arrayOfImagesShareName[indexPath.row])
              cell!.layer.borderColor = UIColor.blue.cgColor
              cell!.layer.borderWidth = 1
              cell!.isSelected = true
          }
          else {
              let index = self.arrayOfSelectedApp.firstIndex(of: arrayOfImagesShareName[indexPath.row])
                      self.arrayOfSelectedApp.remove(at: index!)
              let cell = collectionView.cellForItem(at: indexPath)
              cell?.layer.borderColor = UIColor.clear.cgColor
              cell?.layer.borderWidth = 1
              cell?.isSelected = false
          }
    }
    //MARK:- TextField Delgate
//                func textFieldDidBeginEditing(_ textField: UITextField) {
//                   let toolbarDone = UIToolbar.init()
//                     activeTextField                  =  textField
//
//                   toolbarDone.sizeToFit()
//                                let barBtnDone = UIBarButtonItem.init(barButtonSystemItem: UIBarButtonItem.SystemItem.done,target: self, action: #selector(self.doneButtonClicked))
//                                barBtnDone.tintColor = kAppThemeYellowTop
//                                let flexSpace = UIBarButtonItem(barButtonSystemItem: UIBarButtonItem.SystemItem.flexibleSpace, target: nil, action: nil)
//                                toolbarDone.items = [flexSpace,barBtnDone]
//                      textField.inputAccessoryView = toolbarDone
//                    print(textField.tag)
//                   if textField == textfieldDate {
//                    print(textField.tag)
//                       datePicker.datePickerMode = .date
//                    if #available(iOS 13.4, *) {
//                        datePicker.preferredDatePickerStyle = UIDatePickerStyle.wheels
//                    } else {
//                        // Fallback on earlier versions
//                    }
//                       textField.inputView = datePicker
//
//                   }
//                   else if textField == textfieldTime {
//                    print(textField.tag)
//                    datePicker.datePickerMode = .time
//                    if #available(iOS 13.4, *) {
//                        datePicker.preferredDatePickerStyle = UIDatePickerStyle.automatic
//                    } else {
//                        // Fallback on earlier versions
//                    }
//                       textField.inputView = datePicker
//                   }
//                      textField.becomeFirstResponder()
//                }
             func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
                
                 return true
             }
                func textFieldDidEndEditing(_ textField: UITextField) {
                  
                    textField.resignFirstResponder()
                }
         
          func textFieldShouldReturn(_ textField: UITextField) -> Bool {
               textField.resignFirstResponder()
              return true
          }
//    @IBAction func buttonPickerAction(_ sender: UIDatePicker) {
//                   if self.activeTextField == textfieldDate {
//                                let dateFormater = DateFormatter()
//                                 dateFormater.dateFormat = "yyyy-MM-dd"
//                    self.textfieldDate.text = dateFormater.string(from: datePicker.date)
//                   } else {
//                    let dateFormater = DateFormatter()
//                            dateFormater.dateFormat = "HH:mm"
//
//                            self.textfieldTime.text = dateFormater.string(from: datePicker.date)
//                  }
//               }
    //MARK:- Done button action keyboard
             @objc func doneButtonClicked() {
                 self.view.endEditing(true)
             }
         func setNotificationDetails(){
            if !self.textfieldDate.text!.isEmpty && !self.textfieldTime.text!.isEmpty  {
               if self.arrayOfSelectedApp.count > 0 {
                ServiceManager.sharedInstance.post(withServiceName: "socialsharesave", andParameters: ["fileid":self.fileDetails.fileid!,"date":self.textfieldDate.text!,"time":self.textfieldTime.text!,"socialmedia":self.arrayOfSelectedApp.joined(separator: ","),"id":"\(MEUserDetails.currentUser.id!)","notid":self.fileDetails.notid!], withHud: true) { (success, result, error) in
                              if success == true {
                                  if (result as! NSDictionary)["message"] as! String == "Success" {
                                     self.showAlertWithOkButton(message:"Successfully Set")
                                    self.navigationController?.popViewController(animated: true)
                                  }
                                  else {
                                        self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                                  }
                              }
                              else {
                                    self.showAlertWithOkButton(message: "Please try again")
                              }
                          }
               }
               else {
                   self.showAlertWithOkButton(message: "Please Select Any App")
               }
               }
                         else {
                            self.showAlertWithOkButton(message: "Please fill the fields")
                           }
            }
    //MARK:- Show Alert With Ok Button
                   func showAlertWithOkButton(message:String){
                       AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                           
                       }
                   }
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
