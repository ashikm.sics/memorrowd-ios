//
//  MESharedFilesTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 14/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class MESharedFilesTVC: UITableViewController {
    var arrayOfShare = [ListOfShare]()

    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.customUI()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    override func viewWillAppear(_ animated: Bool) {
        
        super.viewWillAppear(true)
         self.getLastestList()
   
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationInActiveState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
        NotificationCenter.default.addObserver(self, selector: #selector(self.actionPushNotificationBGState(_:)), name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
        
    }
    override func viewWillDisappear(_ animated: Bool) {
           super.viewDidDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveState"), object: nil)
        
         NotificationCenter.default.removeObserver(self, name: Notification.Name(rawValue: "PushnotificationInActiveStateBG"), object: nil)
       }
    //MARK:- Set Up NavigationBar
       func setUpNavigationBar() {
           self.navigationController?.navigationBar.isHidden = false
           self.navigationItem.setHidesBackButton(true, animated: true)
               self.navigationItem.title = "Shared Files"
                 self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white,NSAttributedString.Key.font: UIFont(name: "Nunito-Regular", size: 22)!]
         
           self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
       
       }
          //MARK:- CustomUI
             func customUI() {
               
                self.tableView.reloadData()
             }
    //MARK:- Button Actions
     @IBAction func buttonDeleteAction(_ sender: UIButton) {
        ServiceManager.sharedInstance.post(withServiceName: "delete_schedule", andParameters: ["id":MEUserDetails.currentUser.id!,"notid":self.arrayOfShare[sender.tag].notid!], withHud: true) { (success, result, error) in
                        if success == true {
                            if (result as! NSDictionary)["message"] as! String == "Success" {
                               self.showAlertWithOkButton(message:"Deleted Successfully")
                                self.getLastestList()
                            }
                            else {
                                  self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                            }
                        }
                        else {
                              self.showAlertWithOkButton(message: "Please try again")
                        }
                    }
       
        
     }
    
      @IBAction func buttonEditAction(_ sender: UIButton) {
        let listVC = kStoryboard.instantiateViewController(withIdentifier: "MEEditNotificationTVC") as! MEEditNotificationTVC
        listVC.fileDetails = self.arrayOfShare[sender.tag]
        self.navigationController?.pushViewController(listVC, animated: true)
      }
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return  self.arrayOfShare.count
    }
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
             let cell  = tableView.dequeueReusableCell(withIdentifier: "cell") as! sharedVideoTableViewCell
     
        if  self.arrayOfShare[indexPath.row].file_type! == "Image" {
             cell.imageViewVideos.downloaded(from:  self.arrayOfShare[indexPath.row].file!)
            cell.imageViewPlay.isHidden = true
        }
        else {
             cell.imageViewVideos.downloaded(from:  self.arrayOfShare[indexPath.row].file!)
            cell.imageViewPlay.isHidden = false
        }
        cell.labelVideosName .text  = self.arrayOfShare[indexPath.row].title!
        cell.labelVideosTime.text  = self.arrayOfShare[indexPath.row].time!
        cell.labelVideosDate.text  = self.arrayOfShare[indexPath.row].date!

        cell.labelVideosSharedVia.text  =  self.arrayOfShare[indexPath.row].app!
        cell.btnDelete.tag = indexPath.row
        cell.btnEdit.tag = indexPath.row
        return cell
         }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 132
    }
    //MARK:- HANDLING PUSHNOTIFICATION IN ACTIVE STATE
         @objc func actionPushNotificationInActiveState(_ notification: Notification) {
    
          let message = "Its time to post your post."
               
                 let alertController = UIAlertController(title: "Notification", message: message, preferredStyle: UIAlertController.Style.alert)
                 let buttonView = UIAlertAction(title: "Post", style: UIAlertAction.Style.default, handler: { (_) in
                    let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                      listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                     self.navigationController?.present(listVC, animated: true, completion: {
                        
                     })
                  
                 })
                 let buttonCancel = UIAlertAction(title: "Cancel", style: UIAlertAction.Style.destructive, handler: { (_) in
                     self.dismiss(animated: true, completion: nil)
                 })
                 alertController.addAction(buttonView)
                 alertController.addAction(buttonCancel)
                 self.present(alertController, animated: true, completion: nil)
                 
             
         }
    @objc func actionPushNotificationBGState(_ notification: Notification) {
              let listVC = kStoryboard.instantiateViewController(withIdentifier:"MENotificationSetTVC") as! MENotificationSetTVC
                              listVC.fileId  = (notification.userInfo!["aps"] as! NSDictionary)["notificationid"] as! Int
                             self.navigationController?.present(listVC, animated: true, completion: {
                                
                             })
      }
    func getLastestList() {
        
    ServiceManager.sharedInstance.post(withServiceName: "listshare", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"] as [String : Any], withHud: true) { (success, result, error) in
              
              if success == true {
                  if (result as! NSDictionary)["message"] as! String == "Success" {
                      self.arrayOfShare.removeAll()
                      let listDetailsDict = result as! NSDictionary
                    
                      for items in listDetailsDict.value(forKey: "details") as! NSArray {
                          let obj  = ListOfShare().initWithDictionary(userDictionary: items as! NSDictionary)
                          self.arrayOfShare.append(obj)
                      }
                    self.tableView.reloadData()

                  }
                  else {
                        self.showAlertWithOkButton(message: (result as! NSDictionary)["message"] as! String )
                  }
              }
              else {
                    self.showAlertWithOkButton(message: "Please try again")
              }
          }}
    //MARK:- Show Alert With Ok Button
                func showAlertWithOkButton(message:String){
                    AJAlertController.initialization().showAlertWithOkButton(aStrMessage: message) { (index, title) in
                        
                    }
                }
          
    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
