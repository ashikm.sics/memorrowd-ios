//
//  SettingsTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 16/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit
import SVProgressHUD

class SettingsTVC: UITableViewController {
    @IBOutlet var labelemail                      : UILabel!
    @IBOutlet var sliderCollection                : [UISlider]!

    var arrayOfImagesShareName = ["Facebook","Twitter","Instagram","Linkedin","Whatsapp","Snapchat","Email"]
    var arrayOfSelectedName = ["Facebook","Twitter","Instagram","Linkedin","Whatsapp","Snapchat","Email"]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        self.labelemail.text = MEUserDetails.currentUser.email!
        self.updateList()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidLayoutSubviews() {
//        updateList()
//        print(arrayOfSavedApp)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
    }
    
    //MARK:- Set Up NavigationBar
         func setUpNavigationBar() {
            if #available(iOS 13.0, *) {
                       let navBarAppearance = UINavigationBarAppearance()
                       navBarAppearance.configureWithOpaqueBackground()
                       navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                       navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                       navBarAppearance.backgroundColor = kAppThemeYellowTop
                       self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                       self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
                   }
             self.navigationController?.navigationBar.isHidden = false
             self.navigationItem.setHidesBackButton(true, animated: true)
             self.navigationItem.title = "Settings"
             self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
             self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
            
               let sideButtonButton = UIBarButtonItem(image: UIImage(named: "back")!.withRenderingMode(.alwaysOriginal), style: .plain, target: self, action: #selector(backAction))
                     self.navigationItem.leftBarButtonItem = sideButtonButton
         }
         @objc func backAction() {
               self.navigationController?.popViewController(animated: true)
              
         }
    //MARK:- Button Actions
    @IBAction func buttonChangePasswordAction(_ sender: UIButton) {
    let signUpVC = kStoryboard.instantiateViewController(withIdentifier: "MEChangePasswordTVC") as! MEChangePasswordTVC
    self.navigationController?.pushViewController(signUpVC, animated: true)
    }
     @IBAction func buttonChangeSliderAction(_ sender: UISlider) {
        var selectedApp = ""
        selectedApp = self.arrayOfImagesShareName[sender.tag]
        print("slider value: \(sender.value)")
        if sender.value > 0.1{
            sender.value = 1
                if !self.arrayOfSelectedName.contains(selectedApp)  {
                    self.arrayOfSelectedName.append(selectedApp)
                    print("selected Appl: \(selectedApp)")
                }
        }
        
        else {
           if self.arrayOfSelectedName.contains(selectedApp)  {
            let ind = self.arrayOfSelectedName.firstIndex(of: selectedApp)
            print("selected App2: \(ind)")
            self.arrayOfSelectedName.remove(at: ind!)
            }
        }
        self.getAppSaved()
    }
    func getAppSaved(){
        

             print(arrayOfSelectedName)
//        SVProgressHUD.show()
//        Loader.Show()
        print("Selection is: \(self.arrayOfSelectedName)")
        print("userID: \(MEUserDetails.currentUser.id)")
        ServiceManager.sharedInstance.post(withServiceName: "saveapp", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)","socialapps":self.arrayOfSelectedName.joined(separator: ",")], withHud: true) {  (success, result, error) in
//            SVProgressHUD.dismiss()
//            Loader.Stop()
                                if success == true {
                                    if (result as! NSDictionary)["message"] as! String == "Success" {
                                      
                                    }
                                    else {
                                    }
                                }
                                else {
                                }
                          
                          
                    }
                 
                      
    }
    func updateList(){
        print(MEUserDetails.currentUser.id!)
//        Loader.Show()
          ServiceManager.sharedInstance.post(withServiceName: "listapp", andParameters: ["user_id":"\(MEUserDetails.currentUser.id!)"], withHud: true) {  (success, result, error) in
//              SVProgressHUD.dismiss()
//              Loader.Stop()
                                  if success == true {
                                      if (result as! NSDictionary)["message"] as! String == "Success" {
                                         let app =  (result as! NSDictionary)["socialapps"] as! String
                                          arrayOfSavedApp = app.components(separatedBy: ",")
                                          self.arrayOfSelectedName = app.components(separatedBy: ",")
                                        for (ind,item) in self.arrayOfImagesShareName.enumerated() {
                                            print("apps: \(self.arrayOfImagesShareName)")
                                            print("apps turned on:\(arrayOfSavedApp)")
                                            print("index:\(ind)")
                                            print("item: \(item)")
                                            let trimmed = item.trimmingCharacters(in: .whitespaces)
                                            print("trimmed: \(trimmed)")
                                            if arrayOfSavedApp.contains(trimmed) {
                                                print(true)
                                                self.sliderCollection[ind].value = 1
                                                print(arrayOfSavedApp)
                                                
                                            }
                                            else {
                                                print(false)
                                                self.sliderCollection[ind].value = 0
                                                print(arrayOfSavedApp)
                                            }
                                        }
                                      }
                                      else {
                                      }
                                  }
                                  else {
                                  }
                            
                            
                      }
                   
                        
      }
    
   /* // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
