//
//  MESidemenuTVC.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 15/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class MESidemenuTVC: UITableViewController {
    @IBOutlet var imageViewProfilePic              : UIImageView!
    @IBOutlet var labelName                        : UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNavigationBar()
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }
    
    override func viewDidLayoutSubviews() {
        self.customUI()
    }
    //MARK:- Set Up NavigationBar
       func setUpNavigationBar() {
        if #available(iOS 13.0, *) {
                   let navBarAppearance = UINavigationBarAppearance()
                   navBarAppearance.configureWithOpaqueBackground()
                   navBarAppearance.titleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.largeTitleTextAttributes = [.foregroundColor: UIColor.white]
                   navBarAppearance.backgroundColor = kAppThemeYellowTop
                   self.navigationController?.navigationBar.standardAppearance = navBarAppearance
                   self.navigationController?.navigationBar.scrollEdgeAppearance = navBarAppearance
               }
           self.navigationController?.navigationBar.isHidden = true
           self.navigationItem.setHidesBackButton(true, animated: true)
           //self.navigationItem.title = "Add To Cart"
           self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor: UIColor.white]
           self.navigationController?.navigationBar.barTintColor = kAppThemeYellowTop
        
       }
       
       //MARK:- CustomUI
          func customUI() {
           self.tableView.backgroundView =  UIImageView(image: UIImage(named: "login-bg"))
              self.imageViewProfilePic.setBorderAndCornerRadius(borderColor:.white, borderWidth: 2, cornerRadius: 53)
            print("pro pic\(MEUserDetails.currentUser.picture!)")
              print("user ID is \(MEUserDetails.currentUser.id)")
            self.imageViewProfilePic.downloaded(from:  MEUserDetails.currentUser.picture!)
                       self.labelName.text = MEUserDetails.currentUser.name!
              
          }
    
  /*  // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 0
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 0
    }*/

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */
   
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let isPackageExpired = MEUserDetails.currentUser.isPackageExpired
        if indexPath.row == 0 {
            
        }
        else if  indexPath.row == 1 {
                   
        }
        else if  indexPath.row == 2 {
            if isPackageExpired{
                showExpiredAlert()
            }else{
                NotificationCenter.default.post(name: Notification.Name("sideMenu"), object: nil, userInfo: ["ind":2])
                dismiss(animated: true) {
                }
            }
            
         
        }
        else if  indexPath.row == 3 {
             NotificationCenter.default.post(name: Notification.Name("sideMenu"), object: nil, userInfo: ["ind":1])
              dismiss(animated: true) {
            }
        }
        else if  indexPath.row == 4 {
            let viewController = kStoryboard.instantiateViewController(withIdentifier: "MEUpgradePlanTVC") as! MEUpgradePlanTableViewController
            self.navigationController?.pushViewController(viewController, animated: true)
                   
        }
        else if  indexPath.row == 5 {
            if isPackageExpired{
                showExpiredAlert()
            }else{
                NotificationCenter.default.post(name: Notification.Name("sideMenu"), object: nil, userInfo: ["ind":3])
                  dismiss(animated: true) {
                }
            }
     
        }
        else if  indexPath.row == 6 {
             NotificationCenter.default.post(name: Notification.Name("sideMenu"), object: nil, userInfo: ["ind":4])
              dismiss(animated: true) {
            }
        }
        else if  indexPath.row == 7 {
             NotificationCenter.default.post(name: Notification.Name("Settings"), object: nil, userInfo: nil)
              dismiss(animated: true) {
            }
        }
        else if  indexPath.row == 8 {
              dismiss(animated: true) {
             let homeNav = kStoryboard.instantiateViewController(withIdentifier: "navigationController") as! UINavigationController
                UserDefaults.standard.removeObject(forKey: "userDetails")
              let window = UIApplication.shared.delegate?.window!
                window!.rootViewController = homeNav
            }
        }
    }
   
    
    func showExpiredAlert(){
        self.dismiss(animated: true) {
            NotificationCenter.default.post(name: Notification.Name(rawValue: "showExpiredAlert"), object: nil, userInfo: nil)
           // NotificationCenter.default.post(name: Notification.Name("NotificationIdentifier"), object: nil)
        }
       // self.navigationController?.popToRootViewController(animated: true)
      

          
    }
    }
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */


