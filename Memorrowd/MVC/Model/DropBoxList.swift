//
//  DropBoxList.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 24/07/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class DropBoxList: NSObject {

  var name              : String?
  var imagOrVdodata     : Data?
  var image             : UIImage!
  var type              : String?
  var pathLower         : String?
    func initWithDictionary(userDictionary : NSDictionary) -> DropBoxList {
        name      = userDictionary.value(forKey: "name")    != nil ? userDictionary.value(forKey:"name")! as! String          : ""
        type      = userDictionary.value(forKey: "type")    != nil ? userDictionary.value(forKey:"type")! as! String          : ""
        imagOrVdodata      = userDictionary.value(forKey: "imagData")    != nil ? (userDictionary.value(forKey:"imagData")! as! Data)          : Data()
//        image      = userDictionary.value(forKey: "image")    != nil ? (userDictionary.value(forKey:"image")! as! UIImage)          : UIImage(named: "Folder")
        image      = userDictionary.value(forKey: "image")    != nil ? userDictionary.value(forKey:"image")! as? UIImage          : UIImage(named: "Folder")
        pathLower      = userDictionary.value(forKey: "pathLower")    != nil ? userDictionary.value(forKey:"pathLower")! as! String          : ""
        return self
   }
}

