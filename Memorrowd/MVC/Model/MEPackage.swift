//
//  MEPackage.swift
//  Memorrowd
//
//  Created by Eldos Thomas on 7/5/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//

import Foundation


class MEPackage {
    var package_id : Int?
    
    
    required public init?(dictionary: NSDictionary) {

        package_id = dictionary["package_id"] != nil ? dictionary["package_id"] as? Int : 0
            
        }
    
    static func modelsFromDictionaryArray(array:NSArray) -> [MEPackage]
        {
            var models = [MEPackage]()
            for item in array{
                let result = item as! NSDictionary
                let post = MEPackage(dictionary: result)
               
                models.append(post!)
                
            }
            return models
        }
    
}
