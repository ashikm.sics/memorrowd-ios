//
//  listOfFiles.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 24/08/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class listOfFiles: NSObject, NSCoding {
    
    public var fileid            : Int!
    public var path              : String?
    public var title             : String!
    public var category          : String?
    public var descriptionFile   : String?
    public var date              : String?
    public var time              : String?
    
    static var currentList: listOfFiles = {
        let instance = listOfFiles()
        return instance
    }()
    
    init(fileid: Int = 0, path: String = "", title: String = "", category: String = "",descriptionFile: String = "", date: String = "", time: String = "") {
        self.fileid = fileid
        self.path = path
        self.title = title
        self.category = category
        self.descriptionFile = descriptionFile
        self.date = date
        self.time = time
    }
    
       func initWithDictionary(userDictionary : NSDictionary) -> listOfFiles {
           fileid      = userDictionary.value(forKey: "fileid")    != nil ? userDictionary.value(forKey:"fileid")! as! Int          : 0
           path      = userDictionary.value(forKey: "path")    != nil ? userDictionary.value(forKey:"path")! as! String          : ""
           title      = userDictionary.value(forKey: "title")    != nil ? (userDictionary.value(forKey:"title")! as! String)          : ""
           category      = userDictionary.value(forKey: "category")    != nil ? (userDictionary.value(forKey:"category")! as! String)          : ""
           descriptionFile      = userDictionary.value(forKey: "description")    != nil ? userDictionary.value(forKey:"description")! as! String          : ""
        date      = userDictionary.value(forKey: "date")    != nil ? (userDictionary.value(forKey:"date")! as! String)          : ""
        time      = userDictionary.value(forKey: "time")    != nil ? userDictionary.value(forKey:"time")! as! String          : ""
           return self
    }
    
    required convenience init?(coder: NSCoder) {
        guard let fileid = coder.decodeObject(forKey: "fileid") as? Int,
              let path = coder.decodeObject(forKey: "path") as? String,
              let title = coder.decodeObject(forKey: "title") as? String,
              let category = coder.decodeObject(forKey: "category") as? String,
              let descriptionFile = coder.decodeObject(forKey: "description") as? String,
              let date = coder.decodeObject(forKey: "date") as? String,
              let time = coder.decodeObject(forKey: "time") as? String
        else { return nil }
        self.init(fileid: fileid, path: path, title: title, category: category, descriptionFile: descriptionFile, date: date, time: time)
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(fileid, forKey: "fileid")
           coder.encode(path, forKey: "path")
           coder.encode(title, forKey: "title")
        coder.encode(category, forKey: "category")
        coder.encode(descriptionFile, forKey: "description")
        coder.encode(date, forKey: "date")
        coder.encode(time, forKey: "time")
    }
}
/*"fileid": 8,
           "path": "http://web.sicsglobal.com/memorrowds/files/159774396246961.jpg",
           "title": "",
           "category": "",
           "description ": ""*/

