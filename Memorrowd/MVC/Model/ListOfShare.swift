//
//  ListOfShare.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 28/09/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

import UIKit

class ListOfShare: NSObject {
        var file              : String!
        var file_type              : String?
        var app             : String!
        var date          : String?
        var time       : String?
    var fileid :Int?
     var notid :Int?
    var title : String!
          func initWithDictionary(userDictionary : NSDictionary) -> ListOfShare {
               file      = userDictionary["file"]      is NSNull ? "" : userDictionary.value(forKey: "file")    != nil ? userDictionary.value(forKey:"file")! as! String          : ""
              file_type      = userDictionary["file_type"]      is NSNull ? "" : userDictionary.value(forKey: "file_type")    != nil ? userDictionary.value(forKey:"file_type")! as! String          : ""
              app      = userDictionary["app"]      is NSNull ? "" : userDictionary.value(forKey: "app")    != nil ? (userDictionary.value(forKey:"app")! as! String)          : ""
              date      = userDictionary["date"]      is NSNull ? "" : userDictionary.value(forKey: "date")    != nil ? (userDictionary.value(forKey:"date")! as! String)          : ""
              time      = userDictionary["time"]      is NSNull ? "" : userDictionary.value(forKey: "time")    != nil ? userDictionary.value(forKey:"time")! as! String          : ""
              title      = userDictionary["title"]      is NSNull ? "" : userDictionary.value(forKey: "title")    != nil ? userDictionary.value(forKey:"title")! as! String          : ""
              fileid      = userDictionary.value(forKey: "fileid")    != nil ? userDictionary.value(forKey:"fileid")! as! Int          : 0
              notid      = userDictionary.value(forKey: "notid")    != nil ? userDictionary.value(forKey:"notid")! as! Int          : 0

              return self
       }
}
/*"file": "https://web.sicsglobal.com/memorrowds/files/160405298472193.jpg",
"file_type": "Image",
"title": "trst",
"app": "Facebook",
"date": "31 October 2020",
"time": "03:00 PM",
"fileid": 260,
"notid": 28*/
