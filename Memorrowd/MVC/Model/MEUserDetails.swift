//
//  MEUserDetails.swift
//  Memorrowd
//
//  Created by Srishti Innovative on 18/08/20.
//  Copyright © 2020 Srishti Innovative. All rights reserved.
//

    
    import UIKit



    private var sharedUser : MEUserDetails?
    class MEUserDetails: NSObject {
        class var currentUser: MEUserDetails {
            if sharedUser == nil
            {
                sharedUser = MEUserDetails()
            }
            return sharedUser!
        }
    var id             :Int!
    var name           :String!
    var email          :String!
    var number         :String!
    var address          :String!
    var country_code   : String!
    var picture         :String!
    var filesuploaded :Int!
    var isPackageExpired = false
        
    func initWithDict(dict:NSDictionary)  {
        
        //        UserDefaults.standard.set(NSKeyedArchiver.archivedData(withRootObject: dict), forKey: "userDetails")
        //        UserDefaults.standard.synchronize()
        
        id             = dict["id"]      is NSNull ? 0 : dict["id"]        != nil ? dict["id"] as! Int : 0
        name      = dict["name"]   is NSNull ? "" : dict["name"]     != nil ? dict["name"] as! String : ""
        email      = dict["email"]   is NSNull ? "" : dict["email"]     != nil ? dict["email"] as! String : ""
        country_code      = dict["country_code"]   is NSNull ? "" : dict["country_code"]     != nil ? dict["country_code"] as! String : ""
      number       = dict["number"]    is NSNull ? "" : dict["number"]      != nil ? dict["number"] as! String : ""
        
              address       = dict["address"]    is NSNull ? "" : dict["address"]      != nil ? dict["address"] as! String : ""
            picture       = dict["picture"]    is NSNull ? "" : dict["picture"]      != nil ? dict["picture"] as! String : ""
         filesuploaded       = dict["filesuploaded"]    is NSNull ? 0 : dict["filesuploaded"]      != nil ? dict["filesuploaded"] as! Int : 0
      
        
      
    }
    
}
/*{"user_details":{"id":19,"name":"varsha","email":"varshag.srishti@gmail.com","number":"123456"},"status":true,"message":"Successfully
Logged In"}*/
/*{"user_details":{"name":"varsha","email":"varshag.srishti@gmail.com","number":"12345678","password":"varsha123"},"status":true,"message":"Successfully
Registered"}*/
