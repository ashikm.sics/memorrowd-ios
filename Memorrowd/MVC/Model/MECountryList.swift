//
//  MECountryList.swift
//  Memorrowd
//
//  Created by Xavier Joseph on 21/12/21.
//  Copyright © 2021 Srishti Innovative. All rights reserved.
//

import UIKit

class MECountryList: NSObject, NSCoding {
    
    public var c_code            : String?
    public var country           : String?
    public var phone_code        : String?
    
    static var currentList: MECountryList = {
        let instance = MECountryList()
        return instance
    }()
    
    init(c_code: String = "", country: String = "", phone_code: String = "") {
        self.c_code = c_code
        self.country = country
        self.phone_code = phone_code
    }
    
       func initWithDictionary(userDictionary : NSDictionary) -> MECountryList {
           c_code      = userDictionary.value(forKey: "c_code")    != nil ? userDictionary.value(forKey:"c_code")! as! String          : ""
           country      = userDictionary.value(forKey: "country")    != nil ? (userDictionary.value(forKey:"country")! as! String)          : ""
           phone_code      = userDictionary.value(forKey: "phone_code")    != nil ? (userDictionary.value(forKey:"phone_code")! as! String)          : ""
           return self
    }
    
    required convenience init?(coder: NSCoder) {
        guard let c_code = coder.decodeObject(forKey: "c_code") as? String,
              let country = coder.decodeObject(forKey: "country") as? String,
              let phone_code = coder.decodeObject(forKey: "phone_code") as? String
        else { return nil }
        self.init(c_code: c_code, country: country, phone_code: phone_code)
        
    }
    
    func encode(with coder: NSCoder) {
        coder.encode(c_code, forKey: "c_code")
        coder.encode(country, forKey: "country")
        coder.encode(phone_code, forKey: "phone_code")
    }
}
